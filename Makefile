# := speeds things up by avoiding recursive expansion.
openwrt-build := ./openwrt-build/openwrt-build
projects-repo := aquinas-projects
domain        := $(shell jq -r .domain aquinas.json)
files-common  := $(shell jq -r .commonFiles aquinas-build.json)
webhost       := aquinas-www.$(domain)
webadmin      := teacher
gopkgs        := $(shell go list ./go/... | sed "s%^_$(CURDIR)/%./%g")
gopkgsnowasm  := $(filter-out ./go/busycrate ./go/wasm,$(gopkgs))

# C99, because otherwise cpp defines "linux" as "1."
cpp = cpp -traditional-cpp -C -P -nostdinc -std=c99 -Werror -Iwww

hosts= \
	aquinas-git \
	aquinas-www \
	aquinas-target \
	aquinas-user \

# Build with ext4 security labels for use with libcap-bin.
# To generate a patch such as this one, enter the OpenWrt tree and
# (1) run "make kernel_menuconfig" and (2) run "git diff target/linux."
aquinas-git_openwrt_build_opts=-k openwrt-build-materials/definitions/aquinas-git.kernel-config-patch
aquinas-www_openwrt_build_opts=-k openwrt-build-materials/definitions/aquinas-www.kernel-config-patch

emails = \
	www/static/email-register \
	www/static/email-reset \

godeps = \
	github.com/google/uuid \
	github.com/gorilla/schema \
	github.com/gorilla/mux \
	golang.org/x/sys/unix \

sysdeps = \
	dig \
	g++ \
	git \
	go \
	hugo \
	jq \
	latexml \
	git \
	perl \
	ssh \
	wget \

extension_AMD64 = .S
extension_C = .c
extension_Go = .go
extension_Java = .java
extension_Python =

langsuffix = $(if $(findstring none,$(1)),,$(1))

all: list mds programs wasms installs conf hugo # check

.PHONY: deps
deps:
	for i in $(godeps); do go get "$$i" || exit 1; done
	for i in $(sysdeps); do if ! which "$$i" >/dev/null 2>&1; then echo install $(sysdeps); exit 1; fi; done
	if ! echo 'void main(){}' | gcc -static -m64 -x c -o /dev/null -; then echo install static C lib; exit 1; fi

# macOS ls does not support -I.
projects := $(shell ls $(projects-repo) | grep -v "Makefile\|references.bib")

define upper
$(shell echo $(1) | sed 's/\([a-z]\)\(.*\)/\u\1\2/g')
endef

define languages
$(shell jq -r '.languages[] // empty' $(projects-repo)/$(1)/description.json)
endef

define prerequisites
$(shell jq -r 'select(.prerequisites != null) | .prerequisites[] // empty' $(projects-repo)/$(1)/description.json)
endef

define summary
$(shell jq -r '.summary // empty' $(projects-repo)/$(1)/description.json)
endef

# No getent on macOS.
ifneq ($(shell which getent),)
service_ip := $(shell getent ahostsv4 aquinas-target.$(domain) | grep STREAM | cut -d ' ' -f 1)
else
service_ip := 127.0.0.1
endif

define numchecks
$(shell jq -r '.checks | length' $(projects-repo)/$(1)/description.json)
endef

define service_sources
$(shell jq -r 'select(.services != null) | .services[].source // empty' $(projects-repo)/$(1)/description.json)
endef

define service_ports
$(shell jq -r 'select(.services != null) | .services[].port // empty' $(projects-repo)/$(1)/description.json)
endef

define BUILDSERVICESRC_template
ifeq ($(findstring www/$(2),$(cleans)),)
cleans += tex/$(2)

tex/$(2): $(projects-repo)/$(1)/$(2)
	./sh/aquinas-sanitize $$< >$$@
endif
endef

define BUILDRECORD_template
# $(1): project
# $(2): language
cleans += tex/$(1)$(2).tex
cleans += tex/$(1)$(2).checked
cleans += tex/$(1)$(2)-title.tex
cleans += tex/$(1)$(2)-summary.tex
cleans += tex/$(1)$(2)-project.tex
cleans += tex/$(1)$(2)-instructions.tex
cleans += tex/references.bib
cleans += www-big/project/$(1)$(2)/files
cleans += www/content/project/$(1)$(2)/img/
mds    += www/content/project/$(1)$(2).md

# Should also install git-hooks/pre-commit at .git/hooks/pre-commit.
tex/$(1)$(2).checked: ./git-hooks/pre-commit $(projects-repo)/$(1)/description.json
	./git-hooks/pre-commit $(1)
	touch tex/$(1)$(2).checked

tex/$(1)$(2)-title.tex:
	printf '%s' "$(1)" > $$@

tex/$(1)$(2)-summary.tex:
	printf '%s' "$(call summary,$(1))" > $$@

# Project name without language suffix; \jobname in LaTeX has suffix.
tex/$(1)$(2)-project.tex:
	printf '\\newcommand{\\project}{%s\\xspace}\n' "$(1)" > $$@
	printf '\\provideboolean{references}\n' >> $$@; \
	if grep \cite tex/$(1)$(2)-instructions.tex || grep \nocite tex/$(1)$(2)-instructions.tex; then \
		printf '\setboolean{references}{true}\n' >> $$@; \
	else \
		printf '\setboolean{references}{false}\n' >> $$@; \
	fi
	printf '\\provideboolean{graded}\n' >> $$@; \
	if [ "$(call numchecks,$(1))" -ne 0 ]; then \
		printf '\setboolean{graded}{true}\n' >> $$@; \
	else \
		printf '\setboolean{graded}{false}\n' >> $$@; \
	fi
	printf '\\newcommand{\\lang}{%s\\xspace}\n' "$(2)" >> $$@
	printf '\\newcommand{\\prerequisites}{\n' >> $$@
	if [ -n "$(call prerequisites,$(1))" ]; then \
		printf '\\section*{Prerequisites}\n' >> $$@; \
		printf 'Before attempting this project, you should complete the following prerequisites:\n' >> $$@; \
		printf '\\begin{enumerate}\n' >> $$@; \
		for p in $(call prerequisites,$(1)); do \
			if jq -r '.languages[] // empty' $(projects-repo)/$$$$p/description.json | grep $(2); then \
				printf '\item %s\n' "\\href{$$$${p}$(2)}{$$$${p} in $(2)}" >> $$@; \
			elif jq -r '.languages[] // empty' $(projects-repo)/$$$$p/description.json | grep C && [ "$(2)" = AMD64 ]; then \
				printf '\item %s\n' "\\href{$$$${p}C}{$$$${p} in C}" >> $$@; \
			elif jq -r '.languages[] // empty' $(projects-repo)/$$$$p/description.json | grep AMD64; then \
				printf '\item %s\n' "\\href{$$$${p}AMD64}{$$$${p} in AMD64}" >> $$@; \
			else \
				printf '\item %s\n' "\\href{$$$${p}}{$$$${p}}" >> $$@; \
			fi; \
		done; \
		printf '\\end{enumerate}\n' >> $$@; \
	fi
	printf '}\n' >> $$@
	printf '\\newcommand{\\extension}{%s}\n' "$(extension_$(2))" >> $$@
	if [ "$(2)" = Java ]; then \
		printf '\\newcommand{\\projectextension}{%s$(extension_$(2))}\n' "$(call upper,$(1))" >>$$@; \
	else \
		printf '\\newcommand{\\projectextension}{%s$(extension_$(2))}\n' "$(1)" >>$$@; \
	fi; \
	printf '\\newcommand{\\serviceip}{%s\\xspace}\n' "$(service_ip)" >> $$@
	alphabet="abcdefghijklmnopqrstuvwxyz"; \
	n=1; \
	for p in $(call service_ports,$(1)); do \
		l=$$$$(expr substr "$$$${alphabet}" "$$$${n}" 1); \
		printf '\\newcommand{\\serviceport%s}{%s\\xspace}\n' "$$$${l}" "$$$${p}" >> $$@; \
		n="$$$$((n + 1))"; \
	done

ifneq ($(wildcard $(projects-repo)/$(1)/instructions.md),)
tex/$(1)$(2)-instructions.tex: $(projects-repo)/$(1)/instructions.md $(if $(call service_sources,$(1)),$(foreach s,$(call service_sources,$(1)),tex/$(s)))
	if [ -f $(projects-repo)/references.bib ]; then \
		cp $(projects-repo)/references.bib tex/; \
	fi
	pandoc -f markdown_strict -t latex $(projects-repo)/$(1)/instructions.md >$$@
	if [ -d $(projects-repo)/$(1)/img ]; then \
		mkdir -p www/content/project/$(1)$(2)/img/; \
		cp -u $(projects-repo)/$(1)/img/* www/content/project/$(1)$(2)/img/; \
	fi
	if [ -d $(projects-repo)/$(1)/files ]; then \
		mkdir -p www-big/project/$(1)/files/; \
		cp -u $(projects-repo)/$(1)/files/* www-big/project/$(1)$(2)/files/; \
	fi
else
tex/$(1)$(2)-instructions.tex: $(projects-repo)/$(1)/instructions.tex $(if $(call service_sources,$(1)),$(foreach s,$(call service_sources,$(1)),tex/$(s)))
	if [ -f $(projects-repo)/references.bib ]; then \
		cp $(projects-repo)/references.bib tex/; \
	fi
	cp $(projects-repo)/$(1)/instructions.tex $$@
	if [ -d $(projects-repo)/$(1)/img ]; then \
		mkdir -p www/content/project/$(1)$(2)/img/; \
		cp -u $(projects-repo)/$(1)/img/* www/content/project/$(1)$(2)/img/; \
	fi
	if [ -d $(projects-repo)/$(1)/files ]; then \
		mkdir -p www-big/project/$(1)/files/; \
		cp -u $(projects-repo)/$(1)/files/* www-big/project/$(1)/files/; \
	fi
endif

tex/$(1)$(2).tex: tex/$(1)$(2).checked \
                  tex/$(1)$(2)-instructions.tex \
                  tex/$(1)$(2)-title.tex \
                  tex/$(1)$(2)-summary.tex \
                  tex/$(1)$(2)-project.tex
	cp tex/template-record.tex $$@

endef

$(foreach p,$(projects), \
	$(foreach s,$(call service_sources,$(p)), \
		$(eval $(call BUILDSERVICESRC_template,$(p),$(s))) \
	) \
)

$(foreach p,$(projects), \
	$(foreach l,$(call languages,$(p)), \
		$(eval $(call BUILDRECORD_template,$(p),$(call langsuffix,$(l)))) \
	) \
)

define BUILDPROJLIST_template
gencode += go/httpd/project-list.go
cleans  += go/httpd/project-list.go

go/httpd/project-list.go: build/gen-project-list $(foreach p,$(projects),$(projects-repo)/$(p)/description.json)
	./build/gen-project-list >$$@
endef

$(eval $(call BUILDPROJLIST_template))

define BUILDREF_template
cleans += tex/reference-cmd-list.tex
cleans += tex/reference-c-list.tex
cleans += tex/reference-go-list.tex
cleans += tex/reference-java-list.tex
cleans += tex/reference-python-list.tex
cleans += tex/reference-fn-c-list.tex
cleans += tex/reference-fn-go-list.tex
cleans += tex/reference-fn-java-list.tex
cleans += tex/reference-fn-python-list.tex
cleans += tex/references.tex
mds    += www/content/references.md

tex/reference-cmd-list.tex: $(mds)
	grep -h cmddesc $(patsubst %,$(projects-repo)/%/instructions.tex,$(projects)) | sort | uniq >$$@

tex/reference-c-list.tex: $(mds)
	grep -h elemcdesc $(patsubst %,$(projects-repo)/%/instructions.tex,$(projects)) | sort | uniq >$$@

tex/reference-go-list.tex: $(mds)
	grep -h elemgodesc $(patsubst %,$(projects-repo)/%/instructions.tex,$(projects)) | sort | uniq >$$@

tex/reference-java-list.tex: $(mds)
	grep -h elemjavadesc $(patsubst %,$(projects-repo)/%/instructions.tex,$(projects)) | sort | uniq >$$@

tex/reference-python-list.tex: $(mds)
	grep -h elempythondesc $(patsubst %,$(projects-repo)/%/instructions.tex,$(projects)) | sort | uniq >$$@

tex/reference-fn-c-list.tex: $(mds)
	grep -h fncdesc $(patsubst %,$(projects-repo)/%/instructions.tex,$(projects)) | sort | uniq >$$@

tex/reference-fn-go-list.tex: $(mds)
	grep -h fngodesc $(patsubst %,$(projects-repo)/%/instructions.tex,$(projects)) | sort | uniq >$$@

tex/reference-fn-java-list.tex: $(mds)
	grep -h fnjavadesc $(patsubst %,$(projects-repo)/%/instructions.tex,$(projects)) | sort | uniq >$$@

tex/reference-fn-python-list.tex: $(mds)
	grep -h fnpythondesc $(patsubst %,$(projects-repo)/%/instructions.tex,$(projects)) | sort | uniq >$$@

tex/references.tex: tex/reference-cmd-list.tex \
                    tex/reference-c-list.tex \
                    tex/reference-go-list.tex \
                    tex/reference-java-list.tex \
                    tex/reference-python-list.tex \
                    tex/reference-fn-c-list.tex \
                    tex/reference-fn-go-list.tex \
                    tex/reference-fn-java-list.tex \
                    tex/reference-fn-python-list.tex
	cp tex/template-references.tex $$@
endef

$(eval $(call BUILDREF_template))

define BUILDTEXPAGE_template
cleans += $(1)
cleans += $(1).tmp
cleans += $(css)

# NOTE: Latexmlpost cannot handle external files (e.g., <img ...>)
#       when writing to stdout, hence $(1).tmp. We use sed to process relative
#       links, and this allows the links to also work when manually building
#       PDFs from project descriptions (the relative position of the targets
#       change.
$(1): tex/$(patsubst %.md,%.tex,$(notdir $(1)))
	latexml tex/$(patsubst %.md,%.tex,$(notdir $(1))) \
		| sed 's/QUOTEETOUQ/"/g' \
		| sed "s/graphic=\"img/graphic=\"$(basename $(notdir $(1)))\/img/g" \
		| sed "s/href=\"files/href=\"$(basename $(notdir $(1)))\/files/g" \
		| latexmlpost --format=html5 --nodefaultcss --stylesheet=tex/LaTeXML-html5.xsl --sourcedirectory=tex --destination=$(1).tmp -
	if [ "$(1)" = www/content/references.md ]; then \
		cat www/assets/frontmatter \
			| sed "s~SUBTITLE~Helpful references~g" \
			| sed 's/TITLE/Reference list/g' \
			| sed 's/URL/\/references.tmpl/g' >$(1); \
	else \
		cat www/assets/frontmatter \
			| sed "s~SUBTITLE~$$(shell cat tex/$(patsubst %.md,%-summary.tex,$(notdir $(1))))~g" \
		        | sed 's/TITLE/The $$(shell cat tex/$(patsubst %.md,%-title.tex,$(notdir $(1)))) project/g' \
			| sed 's/URL/\/project\/$(patsubst %.md,%,$(notdir $(1)))$(2)\/$(patsubst %.md,%,$(notdir $(1)))$(2).tmpl/g' >$(1); \
	fi
	cat $(1).tmp >>$(1)
	rm $(1).tmp
endef

$(foreach p,$(mds),$(eval $(call BUILDTEXPAGE_template,$(p))))

list: $(list)

mds: $(mds)

define CLEANS_template
cleans += vm-$(1).cfg
cleans += $(1)-openwrt-x86-64-generic-ext4-combined.img
cleans += $(if $(shell grep extra_disks openwrt-build-materials/definitions/$(1).json),$(1)-data-*.img,)
endef

define BUILDVM_template
$(eval $(call CLEANS_template,$(1)))
realcleans += openwrt-$(1)
$(1).diffconfig = $(if $(wildcard openwrt-build-materials/definitions/$(1).diffconfig),openwrt-build-materials/definitions/$(1).diffconfig)

$(1)-openwrt-x86-64-generic-ext4-combined.img: openwrt-build-materials/definitions/$(1).json $(programs)
	$(openwrt-build) -x \
	                 -c openwrt-build-materials/definitions/$(1).json \
	                 -p openwrt-build-materials/definitions/$(1).post \
	                 $$(if $$($(1).diffconfig),-d $$($(1).diffconfig)) \
	                 $(2) $(files-common) openwrt-build-materials/files/$(1)
endef

define BUILDGO_template
programs += build/$(1)
cleans   += build/
cleans   += openwrt-build-materials/files/$(3)/usr/$(4)/$(1)

# NOTE: CGO_ENABLED=0 results in a statically-linked program even when
# using network facilities.
build/$(1): go/$(1)/*.go $(if $(2),go/$(1)/$(2))
	CGO_ENABLED=0 go build -o build/$(1) go/$(1)/*.go

ifneq ($(3),)
installs += $(3)-$(1)

.PHONY: $(3)-$(1)
$(3)-$(1): build/$(1)
	install -D build/$(1) openwrt-build-materials/files/$(3)/usr/$(4)/$(1)
endif
endef

define BUILDGOWASM_template
wasms  += www/static/wasm/$(1).wasm.gz
cleans += www/static/wasm/$(1).wasm.gz

www/static/wasm/$(1).wasm.gz: go/$(1)/*.go
	GOOS=js GOARCH=wasm go build -o www/static/wasm/$(1).wasm go/$(1)/*.go
	gzip -f www/static/wasm/$(1).wasm

endef

$(eval $(call BUILDGOWASM_template,busycrate))

define BUILDC_template
programs += build/$(1)
cleans   += build/
cleans   += openwrt-build-materials/files/$(3)/usr/$(4)/$(1)
build/$(1): c/$(1).c $(if $(2),c/$(2))
	gcc -static -m64 -o build/$(1) $$^

ifneq ($(3),)
installs += $(3)-$(1)

.PHONY: $(3)-$(1)
$(3)-$(1): build/$(1)
	install -D build/$(1) openwrt-build-materials/files/$(3)/usr/$(4)/$(1)
endif
endef

define BUILDSH_template
installs += $(3)-$(1)
cleans   += openwrt-build-materials/files/$(2)/usr/$(3)/$(1)

.PHONY: $(3)-$(1)
$(3)-$(1): sh/$(1)
	install -D sh/$(1) openwrt-build-materials/files/$(2)/usr/$(3)/$(1)
endef

define BUILDCONF_template
conf   += $(1)
cleans += $(foreach h,$(hosts),openwrt-build-materials/files/$(h)/etc/aquinas/$(1))

.PHONY: $(1)
$(1):
	for h in $(hosts); do \
		install -D $$@ openwrt-build-materials/files/$$$$h/etc/aquinas/$$@; \
	done
endef

define BUILDFW_template
conf   += openwrt-build-materials/files/$(1)/etc/config/firewall.post
cleans += openwrt-build-materials/files/$(1)/etc/config/firewall.post

openwrt-build-materials/files/$(1)/etc/config/firewall.post:
	./sh/aquinas-update-firewall $(projects-repo) $(1) >$$@
endef

$(foreach h,$(hosts),$(eval $(call BUILDVM_template,$(h),$($(h)_openwrt_build_opts))))

$(eval $(call BUILDGO_template,gen-project-list,,,))
$(eval $(call BUILDGO_template,queued,,aquinas-git,sbin))
$(eval $(call BUILDGO_template,aquinas-enqueue,,aquinas-git,bin))
$(eval $(call BUILDGO_template,grader,,aquinas-git,sbin))
$(eval $(call BUILDGO_template,httpsh,,aquinas-git,bin))
$(eval $(call BUILDGO_template,aquinas-deploy-key,,aquinas-git,sbin))
$(eval $(call BUILDSH_template,aquinas-initialize-project,aquinas-git,sbin))
$(eval $(call BUILDSH_template,aquinas-initialize-projects,aquinas-git,sbin))
$(eval $(call BUILDSH_template,aquinas-initialize-todo,aquinas-git,sbin))
$(eval $(call BUILDSH_template,aquinas-add-student-slave,aquinas-git,sbin))
$(eval $(call BUILDSH_template,aquinas-quota-student,aquinas-git,sbin))
$(eval $(call BUILDSH_template,aquinas-remove-student-slave,aquinas-git,sbin))
$(eval $(call BUILDSH_template,aquinas-get-ssh-authorized-keys,aquinas-git,sbin))
$(eval $(call BUILDGO_template,aquinas-sanitize,,aquinas-git,bin))
$(eval $(call BUILDSH_template,aquinas-git-post-receive,aquinas-git,bin))
$(eval $(call BUILDGO_template,buildrunsh,,aquinas-user,bin))
$(eval $(call BUILDGO_template,httpd,project-list.go,aquinas-www,sbin))
$(eval $(call BUILDGO_template,aquinas-add-student,,aquinas-www,sbin))
$(eval $(call BUILDGO_template,aquinas-remove-student,,aquinas-www,sbin))
$(eval $(call BUILDGO_template,aquinas-passwd-student,,aquinas-www,sbin))
$(eval $(call BUILDSH_template,aquinas-update-www,aquinas-git,sbin))
$(eval $(call BUILDSH_template,aquinas-update-firewall,aquinas-git,sbin))
$(eval $(call BUILDC_template,chrootdrop,,aquinas-target,sbin))
$(eval $(call BUILDFW_template,aquinas-target))
$(eval $(call BUILDFW_template,aquinas-user))
$(eval $(call BUILDCONF_template,aquinas-functions))
$(eval $(call BUILDCONF_template,aquinas.json))

programs: $(programs)

wasms: $(wasms)

installs: $(installs)

conf: $(conf)

vms: all $(foreach h,$(hosts),$(h)-openwrt-x86-64-generic-ext4-combined.img)

cleans += www/assets/scss/custom.scss

www/assets/scss/custom.scss: tex/LaTeXML.css tex/ltx-listings.css tex/hugo-customization.css tex/LaTeXML-customization.css tex/flyn.org.css
	cat $^ > www/assets/scss/custom.scss

# TODO: Get rid of this sloppiness where we have both f/index.html and f.html.
mds_static = \
	400 \
	403 \
	404 \
	500 \
	account \
	alias \
	alias2 \
	logout \
	password \
	password2 \
	references \
	register \
	register2 \
	register3 \
	remove \
	reset \
	reset2 \
	reset3 \
	results \
	ssh \
	ssh2 \
	wasm \

.PHONY: hugo
hugo: www/assets/scss/custom.scss $(mds)
	( cd www && hugo )
	mv www/public/index.html www/public/landing.tmpl

.PHONY: lint
lint: $(gencode)
	golint -set_exit_status $(gopkgs)

.PHONY: test
test: $(gencode)
	go test $(gopkgsnowasm)

.PHONY: testshort
testshort: $(gencode)
	go test -short $(gopkgsnowasm)

.PHONY: race
race: $(gencode)
	go test -race $(gopkgsnowasm)

.PHONY: raceshort
raceshort: $(gencode)
	go test -short -race $(gopkgsnowasm)

cleans += coverage.out

.PHONY: coverage
coverage: $(gencode)
	go test -covermode=count -coverprofile coverage.out $(gopkgsnowasm) || true
	go tool cover -func=coverage.out

cleans += coverage.html

.PHONY: coveragehtml
coveragehtml: $(gencode)
	go test -covermode=count -coverprofile coverage.out $(gopkgsnowasm) || true
	go tool cover -html=coverage.out

.PHONY: publish_tmpls
publish_tmpls: $(emails) hugo
	rsync --inplace --progress -vv -r -l -H -e ssh $(emails) www/public/ www-big/ $(webadmin)@$(webhost):/www

.PHONY: publish_programs
publish_programs: programs installs conf
	ssh root@aquinas-www.$(domain) /etc/init.d/httpd stop
	ssh root@aquinas-git.$(domain) /etc/init.d/queued stop
	for h in $(hosts); do \
		echo $$h:; \
		rsync --inplace --progress -vv -r -l -H -e ssh openwrt-build-materials/files/$$h/* root@$$h.$(domain):/; \
	done
	ssh root@aquinas-www.$(domain) /etc/init.d/httpd start
	ssh root@aquinas-git.$(domain) /etc/init.d/queued start

.PHONY: publish
publish: $(wasms) publish_tmpls publish_programs

.PHONY: clean
clean:
	rm -rf $(cleans) www/public
	rm -rf $(cleans) www/resources

.PHONY: realclean
realclean: clean
	rm -rf $(realcleans)
