Please refer to README.md for instructions that describe how to build,
configure, deploy, and maintain Aquinas.

The easiest way to contribute to Aquinas is through the project's presence
on on gitlab.com:

	https://gitlab.com/flyn.org/aquinas

The Aquinas project makes use of on the issue ticket system and merge
features provided by GitLab.

You may email requests for support to the primary author of Aquinas,
mike@flyn.org.

Contributions to Aquinas must comply with Aquinas' license terms, as
documented in the file named COPYING.
