#!/bin/sh

set -e

if [ -f aquinas-functions ]; then
	. ./aquinas-functions
else
	. /etc/aquinas/aquinas-functions
fi

usage() {
	error usage: "$(basename "$0")" PROJECTS-DIR aquinas-user\|aquinas-target
}

if [ "$#" != 2 ]; then
	usage
fi

projectsdir="$1"
host="$2"

if [ aquinas-user != "$host" ] && [ aquinas-target != "$host" ]; then
	usage
fi

if [ -z "$logServer" ]; then
	error $conf does not define logServer
fi

if [ ! -d "$projectsdir" ]; then
	error "$projectsdir" does not exist
fi

cat <<EOF
config defaults
	option drop_invalid 1
	option input ACCEPT
	option output ACCEPT
	option forward ACCEPT

# Forbid even outgoing connections.
config zone
	option name lan
	option network lan
	option input DROP
	option output DROP
	option forward DROP

# Allow SSH connections from LAN.
config rule
	option target ACCEPT
	option src lan
	option proto tcp
	option dest_port 22

# Allow outgoing syslog connections to log server.
config rule
	option target ACCEPT
	option dest lan
	option dest_ip $logServer
	option proto tcp
	option dest_port 514
EOF

aquinastarget=$(dig +short "aquinas-target.$domain")
aquinasuser=$(dig +short "aquinas-user.$domain")

for p in $(ls "$projectsdir" | grep -E -v \(Makefile\|references.bib\)); do
	ports=$(jq -r 'select(.services != null ) | .services[].port // empty' "$projectsdir/$p/description.json")
	if [ -n "$ports" ]; then
		if [ aquinas-user = "$host" ]; then
			for port in $ports; do
				cat <<EOF

# Allow outgoing connections to $p service (port $port) on aquinas-target.
config rule
	option target ACCEPT
	option dest lan
	option proto tcp
	option src_ip $aquinasuser
	option dest_ip $aquinastarget
	option dest_port $port
EOF
			done
		else
			for port in $ports; do
				cat <<EOF

# Allow incoming connections to $p service (port $port) from aquinas-user.
config rule
	option target ACCEPT
	option src lan
	option proto tcp
	option src_ip $aquinasuser
	option dest_ip $aquinastarget
	option dest_port $port
EOF
			done
		fi
	fi
done
