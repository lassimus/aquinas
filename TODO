Privacy review
==============

* Register existing user
* Reset password for non-existing vs. existing user
* API leaks

Tests
=====

* Setting alias causes its use
* Teacher can see all true names
* Student can see only aliases, anonymous, and self
* Setting alias with length > 254 rejected

Controls
========

* Set memory and process limits when running student submissions.
* Prevent CPP-based exfil
* Chroot jails around submitted code on aquinas-user
* Constrain teachers as they run arbitrary checks

Random
======

* Move JSON checker to aquinas-projects?
* Build proper OpenJDK package for OpenWrt
  * For now:
    * Download OpenJDK for musl from https://jdk.java.net/15/
    * Copy bin/{java,javac} to openwrt-build/files/aquinas-user/usr/bin/
    * Copy lib/* to openwrt-build/files/aquinas-user/usr/lib/
* Drop aquinas-git's external disk and instead build big base image?
* Use common SSH key routines in aquinas-setup-ssh and other scripts
* Avoid 'y' in aquinas-setup-ssh
* Project update -> update www; LaTeXML on OpenWrt?
* Glossary: github.com/brucemiller/LaTeXML/issues/682
* Use common means of generating passwords across Aquinas scripts and openwrt-build
* openwrt-build: document all options for extra disks (e.g., image=aquinas physical=herald) xen_prefix, gb, fs, args

Infrastructure
==============

* User-unique challenges:
  * bumbler: "randomize" library using binary editing
  * xinetd -> authenticator -> randomized target binary/library

Projects
========

* Prereq for shell: errno, -1, and perror.
* Note checksec
* Ghidra
* Use of printf %n, %4$n (POSIX extension: which parameter to write to)
  * Use of field width to "increase" bytes written
* Attach GOT
* Defeat ASLR
* Defeat RELRO (GOT protection)
* Bug because setenv/getenv are not thread safe
* atoi with bad value
* See secure programming books and do the opposite
* shellcode: fix link to syscall. https://gitlab.com/flyn.org/aquinas/issues/5
* for: describe using down arrow before copying program text and how to use pipe to capture program output
* fsv: block arbitrary writes; see Will Brattain's code
* io: teach about print(..., end='')
* make: worker should depend on this
* buf: io should depend on this
* use of from_hex etc. from canary; make this a prerequisite for canary
* formatted printing
* Literals which should follow variables
* floating point, which follows variables: new temperature formula using \frac
  * Avoid float if possible
  * Do not use ==
  * Float vs. integer division
* A bit on the theoretical limitation of float
* scanf; recursion should depend on this, in addition to function
* pointers
* boolean logic and if/condition
* for depends on condition
* stdin, stdout, stderr
* while
* pipe and redirect
* gdb
* binary I/O and endianness
* stack and x86 calling conventions
* stack and x86_64 calling conventions
* pwntools
* smash (depend on gdb, pwntools, stack, writing binary+endian, and overflow)
* command-line args, which depends on function
* complete description of network project in Go and Python
* Environment variables

Grading
=======

* New technique: reference implementation and random input
* generator; reveal results!
