package safe

import (
	"testing"
)

func TestKey(t *testing.T) {
	d := []byte("ssh-rsa SGVsbG8sIHdvcmxkIQo= user@example.com")
	var k Key

	if _, err := KeyChecked(string(d)); err != nil {
		t.Error("keySafe failed")
	}

	if err := k.UnmarshalText(d); err != nil {
		t.Error(err.Error())
	}

	if string(d) != k.String() {
		t.Error("safeKey modified")
	}
}

func TestKeySpaces(t *testing.T) {
	d := []byte("  ssh-rsa SGVsbG8sIHdvcmxkIQo= user@example.com  ")
	var k Key

	if _, err := KeyChecked(string(d)); err != nil {
		t.Error("keySafe failed")
	}

	if err := k.UnmarshalText(d); err != nil {
		t.Error(err.Error())
	}
}

func TestKeyNewlines(t *testing.T) {
	d := []byte("\n\nssh-rsa SGVsbG8sIHdvcmxkIQo= user@example.com\n\n")
	var k Key

	if _, err := KeyChecked(string(d)); err != nil {
		t.Error("keySafe failed")
	}

	if err := k.UnmarshalText(d); err != nil {
		t.Error(err.Error())
	}
}

func TestKeyTooLong(t *testing.T) {
	var k Key

	d := "ssh-rsa "
	for i := 0; i < 65536; i++ {
		d += "A"
	}
	d += " user@example.com"

	if _, err := KeyChecked(string(d)); err == nil {
		t.Error("keySafe failed")
	}

	if err := k.UnmarshalText([]byte(d)); err == nil {
		t.Error("safeKey accepted excessively long email address")
	}
}
