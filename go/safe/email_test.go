package safe

import (
	"testing"
)

func TestEmail(t *testing.T) {
	d := []byte("user@example.com")
	var e Email

	if _, err := EmailChecked(string(d)); err != nil {
		t.Error("emailSafe failed")
	}

	if err := e.UnmarshalText(d); err != nil {
		t.Error(err.Error())
	}

	if e.String() != string(d) {
		t.Error("safeEmail modified")
	}
}

func TestEmail2(t *testing.T) {
	d := []byte("User <user@example.com>")
	var e Email

	if _, err := EmailChecked(string(d)); err != nil {
		t.Error("emailSafe failed")
	}

	if err := e.UnmarshalText(d); err != nil {
		t.Error(err.Error())
	}

	if e.String() != "user@example.com" {
		t.Error("safeEmail mis-parsed")
	}
}

func TestEmailSpecial(t *testing.T) {
	d := []byte("test")
	var e Email

	if _, err := EmailChecked(string(d)); err != nil {
		t.Error("emailSpecial failed")
	}

	if err := e.UnmarshalText(d); err != nil {
		t.Error(err.Error())
	}

	if e.String() != string(d) {
		t.Error("safeEmail modified")
	}
}

func TestEmailSpecial2(t *testing.T) {
	d := []byte("")
	var e Email

	if _, err := EmailChecked(string(d)); err != nil {
		t.Error("emailSpecial failed")
	}

	if err := e.UnmarshalText(d); err != nil {
		t.Error(err.Error())
	}

	if e.String() != string(d) {
		t.Error("safeEmail modified")
	}
}

func TestEmailIllegalSpaceLocalPart(t *testing.T) {
	d := []byte("User <user @example.com>")
	var e Email

	if _, err := EmailChecked(string(d)); err == nil {
		t.Error("emailSafe failed")
	}

	if err := e.UnmarshalText(d); err == nil {
		t.Error("safeEmail accepted space in local part")
	}
}

func TestEmailIllegalSpaceDomain(t *testing.T) {
	d := []byte("User <user@example .com>")
	var e Email

	if _, err := EmailChecked(string(d)); err == nil {
		t.Error("emailSafe failed")
	}

	if err := e.UnmarshalText(d); err == nil {
		t.Error("safeEmail accepted space in domain")
	}
}

func TestEmailTooLong(t *testing.T) {
	var e Email

	d := "user@"
	for i := 0; i < 254; i++ {
		d += "a"
	}
	d += ".com"

	if _, err := EmailChecked(string(d)); err == nil {
		t.Error("emailSafe failed")
	}

	if err := e.UnmarshalText([]byte(d)); err == nil {
		t.Error("safeEmail accepted excessively long email address")
	}
}
