package safe

import (
	"testing"
)

func TestBooleanStringTrue(t *testing.T) {
	d := []byte("true")
	var b BooleanString

	if _, err := BooleanStringChecked(string(d)); err != nil {
		t.Error("booleanStringChecked failed")
	}

	if err := b.UnmarshalText(d); err != nil {
		t.Error(err.Error())
	}

	if b.String() != string(d) {
		t.Error("booleanString modified")
	}
}

func TestBooleanStringFalse(t *testing.T) {
	d := []byte("false")
	var b BooleanString

	if _, err := BooleanStringChecked(string(d)); err != nil {
		t.Error("booleanStringChecked failed")
	}

	if err := b.UnmarshalText(d); err != nil {
		t.Error(err.Error())
	}

	if b.String() != string(d) {
		t.Error("booleanString modified")
	}
}

func TestBooleanStringUnsafe(t *testing.T) {
	d := []byte("bad")
	var b BooleanString

	if _, err := BooleanStringChecked(string(d)); err == nil {
		t.Error("booleanStringChecked failed")
	}

	if err := b.UnmarshalText(d); err == nil {
		t.Error(err.Error())
	}
}
