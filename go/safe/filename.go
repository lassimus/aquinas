package safe

import (
	"errors"
	"regexp"
)

// Filename is a string that represents a filename.
type Filename struct {
	*string
}

// FilenameChecked checks a string for compliance.
func FilenameChecked(s string) (string, error) {
	if len(s) > 254 {
		return "", errors.New("string too long")
	}

	blacklist := regexp.MustCompile("[^a-zA-Z0-9-]")
	if blacklist.Match([]byte(s)) {
		return "", errors.New("blacklisted rune in string")
	}

	return s, nil
}

// UnmarshalText transforms a []byte into a Filename, checking its format.
func (s *Filename) UnmarshalText(text []byte) (err error) {
	s2, err := FilenameChecked(string(text))
	if err != nil {
		return
	}

	s.string = &s2

	return
}

// String transforms a Filename into a string.
func (s *Filename) String() string {
	return *(s.string)
}
