package safe

import (
	"errors"
	"fmt"
	"io"
)

// BooleanString is a string that must be "true" or "false"
type BooleanString struct{
	*bool
}

func _booleanChecked(s string) (bool, error) {
	var b bool
	var ignored string

	n, err := fmt.Sscanf(s, "%t%s", &b, &ignored)
	if err != nil && err != io.EOF {
		return false, err
	}
	if n != 1 {
		return false, errors.New("extra data after boolean value")
	}

	return b, nil
}

// BooleanStringChecked checks a string for compliance.
func BooleanStringChecked(s string) (string, error) {
	if _, err := _booleanChecked(s); err != nil {
		return "", err
	}

	return s, nil
}

// UnmarshalText transforms a []byte into a BooleanString, checking its format.
func (s *BooleanString) UnmarshalText(text []byte) (err error) {
	b, err := _booleanChecked(string(text))
	if err != nil {
		return
	}

	s.bool = &b

	return
}

// String transforms a BooleanString into a string.
func (s *BooleanString) String() string {
	return fmt.Sprintf("%t", *s.bool)
}
