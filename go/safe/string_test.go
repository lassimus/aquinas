package safe

import (
	"testing"
)

func TestString(t *testing.T) {
	s1 := "abcdefghijklmnopqrstuvwxyz" +
	      "ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
	      "0123456789" +
	      "-@.+/ "
	s2, err := StringChecked(s1)
	if err != nil {
		t.Error(err.Error())
	}
	if s1 != s2 {
		t.Error("safe modified " + s1)
	}
}

func TestStringDangerous(t *testing.T) {
	s1 := ";"
	s2, err := StringChecked(s1)
	if err == nil {
		t.Error("expected error but did not receive one")
	}
	if "" != s2 {
		t.Error("safe did not produce \"\" for dangerous input")
	}
}

func TestStringDangerousBeginning(t *testing.T) {
	s1 := ";a"
	s2, err := StringChecked(s1)
	if err == nil {
		t.Error("expected error but did not receive one")
	}
	if "" != s2 {
		t.Error("safe did not produce \"\" for dangerous input")
	}
}

func TestStringDangerousMiddle(t *testing.T) {
	s1 := "a;a"
	s2, err := StringChecked(s1)
	if err == nil {
		t.Error("expected error but did not receive one")
	}
	if "" != s2 {
		t.Error("safe did not produce \"\" for dangerous input")
	}
}

func TestStringDangerousEnd(t *testing.T) {
	s1 := "a;"
	s2, err := StringChecked(s1)
	if err == nil {
		t.Error("expected error but did not receive one")
	}
	if "" != s2 {
		t.Error("safe did not produce \"\" for dangerous input")
	}
}

func TestStringTooLong(t *testing.T) {
	s1 := ""
	for i := 0; i < 255; i++ {
		s1 += "a"
	}

	s2, err := StringChecked(s1)
	if err == nil {
		t.Error("expected error but did not receive one")
	}
	if "" != s2 {
		t.Error("safe did not produce \"\" for dangerous input")
	}
}
