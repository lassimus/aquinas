package safe

import (
	"errors"
	"regexp"
)

// String is a restricted string.
type String struct {
	*string
}

// StringChecked checks a string for compliance.
func StringChecked(s1 string) (string, error) {
	if len(s1) > 254 {
		return "", errors.New("string too long")
	}

	blacklist := regexp.MustCompile("[^a-zA-Z0-9@.=+-/ ]")
	if blacklist.Match([]byte(s1)) {
		return "", errors.New("blacklisted rune in string")
	}

	return s1, nil
}

// UnmarshalText transforms a []byte into a String, checking its format.
func (s *String) UnmarshalText(text []byte) (err error) {
	s2, err := StringChecked(string(text))
	if err != nil {
		return
	}

	s.string = &s2

	return
}

// String transforms a String into a string.
func (s *String) String() string {
	return *(s.string)
}
