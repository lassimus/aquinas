package safe

import (
	"net/mail"
)

// Email is a string that represents an email address.
type Email struct {
	*mail.Address
	isSpecial bool /* "" is legitimate; cannot use as discriminator. */
	special string
}

// EmailChecked checks a string for compliance.
func EmailChecked(e1 string) (string, error) {
	var e2 Email

	/* Special cases: */
	if e1 == "test" || e1 == "" {
		return e1, nil
	}

	err := e2.UnmarshalText([]byte(e1))
	if err != nil {
		return "", err
	}

	return e1, nil
}

// UnmarshalText transforms a []byte into an Email, checking its format.
func (e *Email) UnmarshalText(text []byte) (err error) {
	/* Special cases: */
	if string(text) == "test" || string(text) == "" {
		e.isSpecial = true
		e.special = string(text)
		return
	}

	e.Address, err = mail.ParseAddress(string(text))
	if err != nil {
		return
	}

	if _, err = StringChecked(e.Address.Address); err != nil {
		return
	}

	return
}

// String transforms an Email into a string.
func (e *Email) String() string {
	/* Special cases: */
	if e.isSpecial {
		return e.special
	}

	return e.Address.Address
}
