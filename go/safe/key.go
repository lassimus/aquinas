package safe

import (
	"encoding/base64"
	"errors"
	"fmt"
	"regexp"
	"strings"
)

// Key is a string that represents an SSH key.
type Key struct {
	*string
}

// KeyChecked checks a string for compliance.
func KeyChecked(k1 string) (string, error) {
	var k2 Key

	err := k2.UnmarshalText([]byte(k1))
	if err != nil {
		return "", err
	}

	return k1, nil
}

// UnmarshalText transforms a []byte into a Key, checking its format.
func (k *Key) UnmarshalText(text []byte) (err error) {
	if len(text) == 0 {
		k = nil
		return nil
	}

	if len(text) > 65536 {
		return errors.New("key too long")
	}

	s := strings.TrimSpace(string(text))
	k.string = &s

	blacklist := regexp.MustCompile("[^a-zA-Z0-9@.=+-/ \r\n]")
	if blacklist.Match([]byte(*k.string)) {
		return errors.New("blacklisted rune in key " + *k.string)
	}

	keys := strings.Split(*k.string, "\n")
	for n, key := range keys {
		field := strings.Fields(key)
		if len(field) != 3 {
			return fmt.Errorf("SSH key %d field count not valid: %s", n, *k.string)
		}

		if _, err := base64.StdEncoding.DecodeString(field[1]); err != nil {
			return fmt.Errorf("SSH key %d base64 not valid: %s", n, *k.string)
		}
	}

	return
}

// String transformsa Key into a string.
func (k *Key) String() string {
	return *(k.string)
}
