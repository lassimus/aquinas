package safe

import (
	"testing"
)

func TestFilename(t *testing.T) {
	s1 := "abcdefghijklmnopqrstuvwxyz" +
	      "ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
	      "0123456789" +
	      "-"
	s2, err := FilenameChecked(s1)
	if err != nil {
		t.Error(err.Error())
	}
	if s1 != s2 {
		t.Error("filename modified " + s1)
	}
}

func TestFilenameDangerous(t *testing.T) {
	s1 := "."
	s2, err := FilenameChecked(s1)
	if err == nil {
		t.Error("expected error but did not receive one")
	}
	if "" != s2 {
		t.Error("filename did not produce \"\" for dangerous input")
	}
}

func TestFilenameDangerousBeginning(t *testing.T) {
	s1 := ".a"
	s2, err := FilenameChecked(s1)
	if err == nil {
		t.Error("expected error but did not receive one")
	}
	if "" != s2 {
		t.Error("filename did not produce \"\" for dangerous input")
	}
}

func TestFilenameDangerousMiddle(t *testing.T) {
	s1 := "a.a"
	s2, err := FilenameChecked(s1)
	if err == nil {
		t.Error("expected error but did not receive one")
	}
	if "" != s2 {
		t.Error("filename did not produce \"\" for dangerous input")
	}
}

func TestFilenameDangerousEnd(t *testing.T) {
	s1 := "a."
	s2, err := FilenameChecked(s1)
	if err == nil {
		t.Error("expected error but did not receive one")
	}
	if "" != s2 {
		t.Error("filename did not produce \"\" for dangerous input")
	}
}

func TestFilenameTooLong(t *testing.T) {
	s1 := ""
	for i := 0; i < 255; i++ {
		s1 += "a"
	}

	s2, err := FilenameChecked(s1)
	if err == nil {
		t.Error("expected error but did not receive one")
	}
	if "" != s2 {
		t.Error("filename did not produce \"\" for dangerous input")
	}
}
