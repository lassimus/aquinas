package db

import (
	"errors"
	"fmt"
	"path"
	"../proj"
)

// ErrAliasExists indicates a requested alias already exists.
var ErrAliasExists error = errors.New("alias exists")

// DB is a database of account, access, and submission information.
type DB interface {
	AddStudent(user, password string) (jobUUID string, err error)
	RemoveStudent(user string) (jobUUID string, err error)
	Authenticate(user, password string) (err error)
	SetPassword(user, password string) (err error)
	Alias(user string) (alias string, err error)
	SetAlias(user, alias string) (err error)
	SSHAuthKeys(user string) (key string, err error)
	SetSSHAuthKeys(user, key string) (jobUUID string, err error)
	KV(user, k string) (v string, err error)
	SetKV(user string, k string, v string) (err error)
	Last(root, user, file string) (last string, err error)
	Attempts(user string) (result map[string]string, err error)
	Permitted(file, user string) error
	Grades(user, student, project string, brief bool) (grades []proj.Grade, err error)
	StudentExists(user string) bool
	IsTeacher(user string) bool
}

// BuildRepo crafts a Git repository URL.
func BuildRepo(db DB, norm, host, root, user, project string) (string, string) {
	gitProvider, _ := db.KV(user, "git-provider")
	gitPath    , _ := db.KV(user, "git-path")
	gitUsername, _ := db.KV(user, "git-username")
	gitToken   , _ := db.KV(user, "git-token")

	switch (gitProvider) {
	case "gitlab":
		return fmt.Sprintf("https://%s:%s@gitlab.com/%s/%s", gitUsername, gitToken, gitPath, project), "gitlab"
	default:
		return fmt.Sprintf("%s@%s:%s", norm, host, path.Join(root, user, project)), "aquinas"
	}
}
