package db

import (
	"bufio"
	"../common"
	"../conf"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"../proj"
	"../run"
	"strings"
	"time"
)

type file struct {
	contents  string
	timestamp time.Time
}

type record struct {
	sshAuthKeys string
	anonID      int
	last        file
	records     file
	kv          map[string]*file
}

// Filesystem is a filesystem-based database of account, access, and submission
// information.
type Filesystem map[string]*record

var nextID int

func newRecord() *record {
	r := new(record)
	r.kv = make(map[string]*file)

	return r
}

// AddStudent creates the given student and assigned him the given password.
func (d Filesystem) AddStudent(student, password string) (jobUUID string, err error) {
	dir  := path.Join("/etc/httpd/accounts", student)
	hash := common.HashCalc(password)

	if _, err := os.Stat(dir); err == nil {
		return "", fmt.Errorf("%s already exists", student)
	}

	if err := os.Mkdir(dir, 0755); err != nil {
		return "", fmt.Errorf("could not create %s: %s", dir, err)
	}

	pwPath := path.Join(dir, "password")
	if err := ioutil.WriteFile(pwPath, []byte(hash + "\n"), 0644); err != nil {
		return "", fmt.Errorf( "could not write to %s: %s", pwPath, err)
	}

	gitHost := "aquinas-git." + conf.Domain()
	stdin := strings.NewReader("add " + student + "\n")
	stdout, _, err := run.SSH(stdin, nil, gitHost)
	if err != nil {
		return "", fmt.Errorf("failed to add student on %s: %s", gitHost, err)
	}

        /* Drop the '\n'. */
	jobUUID = string(stdout[:len(stdout) - 1])

	return jobUUID, err
}

// RemoveStudent removes the given student.
func (d Filesystem) RemoveStudent(student string) (jobUUID string, err error) {
	dir := path.Join("/etc/httpd/accounts", student)

	if _, err := os.Stat(dir); os.IsNotExist(err) {
		return "", fmt.Errorf("%s does not exist", student)
	} else if err != nil {
		return "", fmt.Errorf("error finding record for %s: %s", student, err)
	}

	if err := os.RemoveAll(dir); err != nil {
		return "", fmt.Errorf("could not remove %s: %s", dir, err)
	}

	dir = path.Join("/www", student)
	if err := os.RemoveAll(dir); err != nil {
		return "", fmt.Errorf("could not remove %s: %s", dir, err)
	}

	gitHost := "aquinas-git." + conf.Domain()
	stdin := strings.NewReader("remove " + student + "\n")
	stdout, _, err := run.SSH(stdin, nil, gitHost)
	if err != nil {
		return "", fmt.Errorf("failed to remove student on %s: %s", gitHost, err)
	}

	/* Drop the '\n'. */
	jobUUID = string(stdout[:len(stdout) - 1])

	delete(d, student)

	return
}

func (d Filesystem) passwordHash(user string) (hash string, err error) {
	passwordPath := path.Join("/etc/httpd/accounts", user, "password")

	password, err := ioutil.ReadFile(passwordPath)
	if err != nil {
		return
	}

	hash = string(password[:len(password) - 1]) /* Drop "\n". */
	return
}

// Authenticate checks to see if the given password is correct for the given
// user.
func (d Filesystem) Authenticate(user, password string) (err error) {
	hashedPassword, err := d.passwordHash(user)
	if err != nil {
		err = errors.New("received bad login credentials: " +
		                 "could not read password for " + user)
		return
	}

	if !common.HashSame(hashedPassword, common.HashCalc(password)) {
		err = errors.New("received bad login credentials: bad " +
		                 "password for " + user)
		return
	}

	return
}

// SetPassword sets the given user's password to the given value.
func (d Filesystem) SetPassword(user, password string) (err error) {
	userPath := path.Join("/etc/httpd/accounts", user)
	if _, err = os.Stat(userPath); os.IsNotExist(err) {
		err = os.Mkdir(userPath, 0750)
	}
	if err != nil {
		return
	}

	hash := common.HashCalc(password)

	passwordPath := path.Join(userPath, "password")
	err = ioutil.WriteFile(passwordPath, []byte(hash + "\n"), 0644)
	return
}

// Alias gets the alias for the given user.
func (d Filesystem) Alias(user string) (alias string, err error) {
	return d.KV(user, "alias")
}

// SetAlias sets the given user's password to the given value.
func (d Filesystem) SetAlias(user, alias string) (err error) {
	/* Check to see if alias already in use. */
	dir := "/etc/httpd/accounts"
	files, err := ioutil.ReadDir(dir)
	if err != nil {
		return
	}

	for _, info := range files {
		p := path.Join(dir, info.Name(), "alias")

		if _, err = os.Stat(p); os.IsNotExist(err) {
			continue
		}

		alias2, err := ioutil.ReadFile(p)
		if err != nil {
			return err
		}

		if alias + "\n" == string(alias2) {
			return ErrAliasExists
		}
	}

	return d.SetKV(user, "alias", alias + "\n")
}

// KV gets the value corresponding with the given key for the given user.
func (d Filesystem) KV(user, k string) (v string, err error) {
	p := path.Join("/etc/httpd/accounts", user, k)
	info, err := os.Stat(p)
	if err == nil {
		r, found := d[user]
		if !found {
			d[user] = newRecord()
		} else if _, ok := r.kv[k]; ok && r.kv[k].timestamp.Equal(info.ModTime()) {
			return r.kv[k].contents, nil
		}
	} else if !os.IsNotExist(err) {
		return "", fmt.Errorf(k + " stat failed: %v", err)
	} else {
		return "", nil
	}

	_v, err := ioutil.ReadFile(p)
	if err != nil {
		return "", fmt.Errorf(k + " read failed: %v", err)
	}

	v = string(_v[:len(_v) - 1]) /* Drop "\n". */
	f := new(file)
	f.contents  = v
	f.timestamp = info.ModTime()
	d[user].kv[k] = f

	return
}

// SetKV sets the given user's key to the given value.
func (d Filesystem) SetKV(user, k, v string) (err error) {
	aliasPath := path.Join("/etc/httpd/accounts", user, k)
	return ioutil.WriteFile(aliasPath, []byte(v + "\n"), 0644)
}

// SSHAuthKeys gets the SSH keys for the given user.
func (d Filesystem) SSHAuthKeys(user string) (key string, err error) {
	if r, found := d[user]; !found {
		d[user] = newRecord()
	} else if r.sshAuthKeys != "" {
		return r.sshAuthKeys, nil
	}

	stdin := strings.NewReader("ssh " + user + "\n")
	output, _, err := run.SSH(stdin, nil, "aquinas-git." + conf.Domain())
	if err != nil {
		return "", fmt.Errorf("could not retrieve SSH key from aquinas-git: %v", err)
	}

	key = string(output[:len(output) - 1]) /* Drop "\n". */
	d[user].sshAuthKeys = key

        return
}

// SetSSHAuthKeys sets the SSH keys for the given user to the given value.
func (d Filesystem) SetSSHAuthKeys(user, key string) (jobUUID string, err error) {
	arg := user + ":" + key
	encodedArg := base64.StdEncoding.EncodeToString([]byte(arg))
	stdin := strings.NewReader("key " + encodedArg + "\n")
	u, _, err := run.SSH(stdin, nil, "aquinas-git." + conf.Domain())
	if err != nil {
		return
	}

	jobUUID = string(u[:len(u)-1]) /* Drop '\n'. */

	return
}

// Last returns the last submission record for the given user and file (project).
func (d Filesystem) Last(root, user, filename string) (last string, err error) {
	project := strings.TrimSuffix(filename, path.Ext(filename)) + "-last"

	p := path.Join(root, user, project)
	info, err := os.Stat(p)
	if err == nil {
		r, found := d[user]
		if !found {
			d[user] = newRecord()
			d[user].kv = make(map[string]*file)
		} else if r.last.timestamp.Equal(info.ModTime()) {
			return r.last.contents, nil
		}
	} else if !os.IsNotExist(err) {
		return "", fmt.Errorf("last stat failed: %v", err)
	} else {
		return "", nil
	}

	_last, err := ioutil.ReadFile(p)
	if err != nil {
		return "", fmt.Errorf("last read failed: %v", err)
	}

	last = string(_last[:len(_last) - 1]) /* Drop "\n". */
	d[user].last.contents  = last
	d[user].last.timestamp = info.ModTime()

	return
}

// Attempts returns a mapping of project names to "done" or "failed" for the given user.
func (d Filesystem) Attempts(user string) (result map[string]string, err error) {
	dir    := "/www/" + user + "/"
	suffix := "-records"
	result  = make(map[string]string)

	files, err := ioutil.ReadDir(dir)
	if os.IsNotExist(err) {
		return result, nil
	}
	if err != nil {
		return result, fmt.Errorf("attempts readdir failed: %v", err)
	}

	for _, info := range files {
		var f *os.File
		var ok bool

		if !strings.HasSuffix(info.Name(), suffix) {
			continue
		}

		proj := strings.TrimSuffix(info.Name(), suffix)

		f, err = os.Open(dir + info.Name())
		if os.IsNotExist(err) {
			err = nil
			continue
		}
		if err != nil {
			return
		}
		defer f.Close()

		scanner := bufio.NewScanner(f)
		for scanner.Scan() {
			if strings.Contains(scanner.Text(), "PASS") {
				ok = true
				break
			}
		}

		if ok {
			result[proj] = "done"
		} else {
			result[proj] = "failed"
		}
	}

	return
}

// Permitted returns whether the given user is allowed to access the given file.
func (d Filesystem) Permitted(file, user string) error {
	if file == "" {
		/* Root is always permitted. */
		return nil
	}

	base := strings.TrimSuffix(file, filepath.Ext(file))
	restrict := path.Join("/etc/httpd/restrictions", base)

	files, err := ioutil.ReadDir(restrict)
	if err != nil {
		if os.IsNotExist(err) {
			return nil
		}

		return err
	}

	if user != "" {
		for _, file := range files {
			if file.Name() == user {
				return nil
			}
		}
	}

	msg := "project is restricted"
	return errors.New(msg)
}

// ApplyAlias conditionally renders the alias of the given student, depending
// on which user asks.
func (d Filesystem) ApplyAlias(user, student string) string {
	alias, err := d.Alias(student)
	if err != nil || alias == "" {
		if d.IsTeacher(user) || user == student {
			return student
		}

		if _, ok := d[student]; !ok {
			nextID++
			d[student] = newRecord()
			d[student].anonID = nextID
		} else if d[student].anonID == 0 {
			nextID++
			d[student].anonID = nextID
		}

		return fmt.Sprintf("Anonymous #%d", d[student].anonID)
	}

	if d.IsTeacher(user) || user == student {
		return alias + " (" + student + ")"
	}

	return alias
}

// Grades returns a set of grades, subject to who is asking what.
// Students can retrieve their own full grades.
// Students can retrieve their own brief grades.
// Students can retrieve brief grades (pass only) on everyone (not individuals).
// Teachers can retrieve full grades on anyone.
func (d Filesystem) Grades(user, student, project string, brief bool) (grades []proj.Grade, err error) {
	var students []os.FileInfo
	var includeFailures bool

	if !d.IsTeacher(user) && student != user && !brief {
		return []proj.Grade{}, errors.New("full grade list denied except for self")
	}

	/*
	 * Allowing students to restrict query based on user might violate
	 * privacy even if brief: specifying email address could correlate
	 * alias. Students can request brief results for themselves or everyone.
	 */
	if !d.IsTeacher(user) && student != user && student != "" {
		return []proj.Grade{}, errors.New("targeted brief grade list denied")
	}

	if d.IsTeacher(user) || student == user {
		/* Users can get their own failures, but not others'. */
		includeFailures = true
	}

	if student != "" {
		s, err := os.Stat(path.Join("/www", student))
		if err != nil {
			if os.IsNotExist(err) {
				/* Student dir. not yet created; no grades */
				return []proj.Grade{}, nil
			}

			return []proj.Grade{}, err
		}
		students = []os.FileInfo{s}
	} else {
		students, err = ioutil.ReadDir("/www")
		if err != nil {
			return []proj.Grade{}, err
		}
	}

	for _, info1 := range students {
		student2 := info1.Name()

		if student != student2 && !strings.Contains(student2, "@") {
			/* Test account, and not explicitly requested. */
			continue
		}

		records, err := ioutil.ReadDir(path.Join("/www", student2))
		if err != nil {
			return []proj.Grade{}, err
		}

		for _, info2 := range records {
			var grade2 []proj.Grade

			record := info2.Name()

			if !strings.HasSuffix(record, "-records") {
				continue
			}

			if project != "" && project != strings.TrimSuffix(record, "-records") {
				continue
			}

			f, err := os.Open(path.Join("/www", student2, record))
			if err != nil {
				continue
			}
			defer f.Close()

			dec := json.NewDecoder(f)

			err = dec.Decode(&grade2)
			if err != nil {
				continue
			}

			if brief {
				grade3 := []proj.Grade{}

				for _, g := range grade2 {
					if g.Outcome == "PASS" {
						grade3 = []proj.Grade{g}
						break
					}

					if includeFailures {
						grade3 = []proj.Grade{g}
					}
				}

				grade2 = grade3
			}

			grades = append(grades, grade2...)
		}
	}

	for i := range grades {
		grades[i].Student = d.ApplyAlias(user, grades[i].Student)
	}

	return grades, nil
}

// StudentExists returns true if the given student exists.
func (d Filesystem) StudentExists(student string) bool {
	_, err := os.Stat(path.Join("/etc/httpd/accounts", student))

	return err == nil
}

// IsTeacher returns whether given user is a teacher.
func (d Filesystem) IsTeacher(user string) bool {
	_, err := os.Stat(path.Join("/etc/httpd/accounts", user, "teacher"))
	return !os.IsNotExist(err)
}
