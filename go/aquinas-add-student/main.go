package main

import (
	"../db"
	"fmt"
	"os"
)

func main() {
	var student, password, alias string

	if len(os.Args) != 3 && len(os.Args) != 4 {
		fmt.Fprintf(os.Stderr, "usage: %s USER PASSWORD [ALIAS]\n", os.Args[0])
		os.Exit(1)
	}

	student  = os.Args[1]
	password = os.Args[2]

	if len(os.Args) == 4 {
		alias = os.Args[3]
	}

	db := make(db.Filesystem)

	jobUUID, err := db.AddStudent(student, password)
	if err != nil {
		fmt.Fprintf(os.Stderr, "could not add student: %s\n", err)
		os.Exit(1)
	}

	fmt.Println(jobUUID)

	if alias != "" {
		if err := db.SetAlias(student, alias); err != nil {
			fmt.Fprintf(os.Stderr, "could not set student's alias: %s\n", err)
			os.Exit(1)
		}
	}
}
