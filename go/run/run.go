package run

import (
	"errors"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"syscall"
	"time"
)

// Error records an error comprised of an exit code and stderr message.
type Error struct {
	ExitCode int
	stderr []byte
}

// ErrTimeout is a sentinel error that indicates a grading check timed out.
var ErrTimeout = errors.New("process timed out")

func (e *Error) Error() string {
	return string(e.stderr)
}

// Standard takes reader, hooks it to a command as stdin, and runs the command.
// Returns the command's stdout, stderr, and any error that occurs. If the
// command itself exits with an error code, then Standard will set its err to
// an Error containing this exit code along with the command's stderr.
// (In this case, the stderr in the Error duplicates the returned emsg.
func Standard(reader io.Reader, env []string, arg0 string, args ...string) (output []byte, emsg []byte, err error) {
	cmd := exec.Command(arg0, args...)
	cmd.Env = append(os.Environ(), env...)

	stdin, err := cmd.StdinPipe()
	if err != nil {
		return nil, nil, err
	}

	stdout, err := cmd.StdoutPipe()
	if err != nil {
		return nil, nil, err
	}

	stderr, err := cmd.StderrPipe()
	if err != nil {
		return nil, nil, err
	}

	if nil != reader {
		go func() {
			defer stdin.Close()
			io.Copy(stdin, reader)
		}()
	}

	if err = cmd.Start(); err != nil {
		return nil, nil, err
	}

	output, err = ioutil.ReadAll(stdout)
	if err != nil {
		return nil, nil, err
	}

	if len(output) == 0 {
		output = nil
	}

	emsg, err = ioutil.ReadAll(stderr)
	if err != nil {
		return nil, nil, err
	}

	if len(emsg) == 0 {
		emsg = nil
	}

	err = cmd.Wait()

	if exitErr, ok := err.(*exec.ExitError); ok {
		if status, ok := exitErr.Sys().(syscall.WaitStatus); ok {
			err = &Error{ExitCode: status.ExitStatus(), stderr: emsg}
		} else {
			err = errors.New("Failed to extract exit code")
		}
	}

	return
}

// Background runs a program in the background, but it is otherwise the same
// as Standard.
func Background(reader io.Reader, env []string, arg0 string, args ...string) error {
	cmd := exec.Command(arg0, args...)
	cmd.Env = append(os.Environ(), env...)

	stdin, err := cmd.StdinPipe()
	if err != nil {
		return err
	}

	if nil != reader {
		go func() {
			defer stdin.Close()
			io.Copy(stdin, reader)
		}()
	}

	if err = cmd.Start(); err != nil {
		return err
	}

	return nil
}

type childResult struct {
	output []byte
	emsg   []byte
	err      error
}

// Timed runs a program while imposing a time limit, but it is otherwise the
// same as Standard.
func Timed(duration time.Duration, reader io.Reader, env []string, arg0 string, args ...string) (output []byte, emsg []byte, err error) {
	cmd := exec.Command(arg0, args...)
	cmd.Env = append(os.Environ(), env...)

	stdin, err := cmd.StdinPipe()
	if err != nil {
		return nil, nil, err
	}

	stdout, err := cmd.StdoutPipe()
	if err != nil {
		return nil, nil, err
	}

	stderr, err := cmd.StderrPipe()
	if err != nil {
		return nil, nil, err
	}

	if nil != reader {
		go func() {
			defer stdin.Close()
			io.Copy(stdin, reader)
		}()
	}

	if err = cmd.Start(); err != nil {
		return nil, nil, err
	}

	done := make(chan childResult, 1)
	go func() {
		// Avoid race with Goroutine-local result.
		var res childResult

		res.output, res.err = ioutil.ReadAll(stdout)
		if res.err != nil {
			done <- res
		}

		if len(res.output) == 0 {
			res.output = nil
		}

		res.emsg, res.err = ioutil.ReadAll(stderr)
		if res.err != nil {
			done <- res
		}

		if len(res.emsg) == 0 {
			res.emsg = nil
		}

		res.err = cmd.Wait()
		done <- res
	}()

	select {
	case res := <-done:
		if exitErr, ok := res.err.(*exec.ExitError); ok {
			if status, ok := exitErr.Sys().(syscall.WaitStatus); ok {
				err = &Error{ExitCode: status.ExitStatus(), stderr: res.emsg}
			} else {
				err = errors.New("Failed to extract exit code")
			}
		}

		output = res.output
		emsg   = res.emsg

	case <-time.After(duration):
		if err = cmd.Process.Kill(); err == nil {
			err = ErrTimeout
		}
	}

	return
}

// Remote wraps Standard, adding an invocation of "ssh -T HOST" to the command line.
func Remote(reader io.Reader, env []string, host string, args ...string) (output []byte, emsg []byte, err error) {
	prefix := []string{ "-T", host }
	args = append(prefix, args...)
	return Standard(reader, env, "ssh", args...)
}

// SSH wraps Standard, simply running "ssh -T HOST".
func SSH(reader io.Reader, env []string, host string) (output []byte, emsg []byte, err error) {
	return Standard(reader, env, "ssh", "-T", host)
}
