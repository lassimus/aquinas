package run

import (
	"../conf"
	"strings"
	"testing"
	"time"
)

func TestStandardStdout(t *testing.T) {
	output, _, err := Standard(nil, nil, "echo", "foo")
	if err != nil {
		t.Error(err.Error())
	}
	if string(output) != "foo\n" {
		t.Error("unexpected value in output: " + string(output))
	}
}

func TestStandardStdin(t *testing.T) {
	stdin := strings.NewReader("foo\n")
	output, _, err := Standard(stdin, nil, "cat")
	if err != nil {
		t.Error(err.Error())
	}
	if string(output) != "foo\n" {
		t.Error("unexpected value in output: " + string(output))
	}
}

func TestStandardStderr(t *testing.T) {
	_, _, err := Standard(nil, nil, "ls", "/file-does-not-exist")
	if err == nil {
		t.Error("expected error but did not receive one")
	}
	if "ls: cannot access '/file-does-not-exist': No such file or directory\n" != err.Error() {
		t.Error("unexpected value in stderr: " + err.Error())
	}
}

func TestStandardStderrExitZero(t *testing.T) {
	_, emsg, err := Standard(nil, nil, "sh", "-c", "echo foo >&2")
	if err != nil {
		t.Error(err.Error())
	}
	if string(emsg) != "foo\n" {
		t.Error("unexpected value in stderr: " + string(emsg))
	}
}

func TestStandardExitCode(t *testing.T) {
	_, _, err := Standard(nil, nil, "false")
	if err == nil {
		t.Error("expected error but did not receive one")
	}
	if stderrErr, ok := err.(*Error); ok {
		if stderrErr.ExitCode != 1 {
			t.Error("received wrong exit code")
		}
	} else {
		t.Error(err.Error())
	}
}

func TestStandardBadCommand(t *testing.T) {
	_, _, err := Standard(nil, nil, "command-does-not-exist")
	if err == nil {
		t.Error("non-existing command did not produce error")
	}
}

func TestTimedStdout(t *testing.T) {
	output, _, err := Timed(1 * time.Second, nil, nil, "echo", "foo")
	if err != nil {
		t.Error(err.Error())
	}
	if string(output) != "foo\n" {
		t.Error("unexpected value in output: " + string(output))
	}
}

func TestTimedStdin(t *testing.T) {
	stdin := strings.NewReader("foo\n")
	output, _, err := Timed(1 * time.Second, stdin, nil, "cat")
	if err != nil {
		t.Error(err.Error())
	}
	if string(output) != "foo\n" {
		t.Error("unexpected value in output: " + string(output))
	}
}

func TestTimedStderr(t *testing.T) {
	_, _, err := Timed(1 * time.Second, nil, nil, "ls", "/file-does-not-exist")
	if err == nil {
		t.Error("expected error but did not receive one")
	}
	if "ls: cannot access '/file-does-not-exist': No such file or directory\n" != err.Error() {
		t.Error("unexpected value in stderr: " + err.Error())
	}
}

func TestTimedStderrExitZero(t *testing.T) {
	_, emsg, err := Timed(1 * time.Second, nil, nil, "sh", "-c", "echo foo >&2")
	if err != nil {
		t.Error(err.Error())
	}
	if string(emsg) != "foo\n" {
		t.Error("unexpected value in stderr: " + string(emsg))
	}
}

func TestTimedExitCode(t *testing.T) {
	_, _, err := Timed(1 * time.Second, nil, nil, "false")
	if err == nil {
		t.Error("expected error but did not receive one")
	}
	if stderrErr, ok := err.(*Error); ok {
		if stderrErr.ExitCode != 1 {
			t.Error("received wrong exit code")
		}
	} else {
		t.Error(err.Error())
	}
}

func TestTimedBadCommand(t *testing.T) {
	_, _, err := Timed(1 * time.Second, nil, nil, "command-does-not-exist")
	if err == nil {
		t.Error("non-existing command did not produce error")
	}
}

func TestTimedOut(t *testing.T) {
	_, _, err := Timed(1 * time.Second, nil, nil, "sleep", "2")
	switch err {
	case ErrTimeout:
		return
	case nil:
		t.Error("process did not return error; should have timed out")
	default:
		t.Error("should have timed out, but received:" + err.Error())
	}
}

func TestRemoteStdout(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping test in short mode.")
	}

	output, _, err := Remote(nil, nil, "root@aquinas-git." + conf.Domain(), "echo", "foo")
	if err != nil {
		t.Error(err.Error())
	}
	if string(output) != "foo\n" {
		t.Error("unexpected value in stderr: " + string(output))
	}
}

func TestRemoteStderr(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping test in short mode.")
	}

	_, _, err := Remote(nil, nil, "root@aquinas-git." + conf.Domain(), "ls", "/file-does-not-exist")
	if err == nil {
		t.Error("expected error but did not receive one")
	}
	if "ls: /file-does-not-exist: No such file or directory\n" != err.Error() {
		t.Error("unexpected value in stderr: " + err.Error())
	}
}

func TestRemoteFail(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping test in short mode.")
	}

	_, _, err := Remote(nil, nil, "root@aquinas-git." + conf.Domain(), "false")
	if err == nil {
		t.Error("expected error but did not receive one")
	}
	if stderrErr, ok := err.(*Error); ok {
		if stderrErr.ExitCode != 1 {
			t.Error("received wrong exit code")
		}
	} else {
		t.Error(err.Error())
	}
}

func TestSSHStdout(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping test in short mode.")
	}

	stdin := strings.NewReader("echo foo\n")
	output, _, err := SSH(stdin, nil, "root@aquinas-git." + conf.Domain())
	if err != nil {
		t.Error(err.Error())
	}
	if string(output) != "foo\n" {
		t.Error("unexpected value in stderr: " + string(output))
	}
}

func TestSSHStderr(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping test in short mode.")
	}

	stdin := strings.NewReader("ls /file-does-not-exist")
	_, _, err := Remote(stdin, nil, "root@aquinas-git." + conf.Domain())
	if err == nil {
		t.Error("expected error but did not receive one")
	}
	if "ls: /file-does-not-exist: No such file or directory\n" != err.Error() {
		t.Error("unexpected value in stderr: " + err.Error())
	}
}

func TestSSHFail(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping test in short mode.")
	}

	stdin := strings.NewReader("false")
	_, _, err := SSH(stdin, nil, "root@aquinas-git." + conf.Domain())
	if err == nil {
		t.Error("expected error but did not receive one")
	}
	if stderrErr, ok := err.(*Error); ok {
		if stderrErr.ExitCode != 1 {
			t.Error("received wrong exit code")
		}
	} else {
		t.Error(err.Error())
	}
}
