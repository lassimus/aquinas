package main

import (
	"encoding/json"
	"fmt"
	"../proj"
	"strings"
	"syscall/js"
	"../wasm"
)

func renderBook(this js.Value, args []js.Value) interface{} {
	data := args[0].String()

	document := wasm.GlobalGet("document")
	ul := wasm.GetElementByID(document, "main-content-ul")
	ul.Set("innerHTML", "")

	input := wasm.GetElementByID(document, "main-content-search")
	search := input.Get("value").String()

	grades := []proj.Grade{}
	json.Unmarshal([]byte(data), &grades)

	for _, g := range grades {
		if !strings.Contains(g.Student, search) &&
		   !strings.Contains(g.Project, search) &&
		   !strings.Contains(g.Outcome, search) &&
		   !strings.Contains(g.Timestamp, search) {
			continue
		}

		li := wasm.CreateElement(document, "li")
		record := fmt.Sprintf("%s: %s %s %s", g.Student, g.Project, g.Outcome, g.Timestamp)
		wasm.AppendChild(li, wasm.CreateTextNode(document, record))
		wasm.AppendChild(ul, li)
	}

	return nil
}

func book(file, student, ignored string) {
	title := "Grade book"
	descr := "List of student grades"
	document, content := wasm.DisplayStart()

	wasm.DisplaySetTitleDescr(document, title, descr)

	grades, err := wasm.GetGradesBrief()
	if err != nil {
		wasm.DisplayLocalErr(document, content, err.Error())
		return
	}

	data, err := json.Marshal(grades)
	if err != nil {
		wasm.DisplayLocalErr(document, content, err.Error())
		return
	}

	label := wasm.CreateElement(document, "label")
	label.Set("innerHTML", "Search:")
	wasm.AppendChild(content, label)

	input := wasm.CreateElement(document, "input")
	input.Set("type", "text")
	input.Set("placeholder", "String")
	input.Set("id", "main-content-search")
	wasm.AddEventListener(input, "input", js.FuncOf(
		func(this js.Value, args []js.Value) interface{} {
			js.FuncOf(renderBook).Invoke(string(data))
			return nil
		}))
	wasm.AppendChild(content, input)

	ul := wasm.CreateElement(document, "ul")
	ul.Set("id", "main-content-ul")
	wasm.AppendChild(content, ul)

	js.FuncOf(renderBook).Invoke(string(data))
}
