package main

import (
	"syscall/js"
	"../wasm"
)

func project(file, student, ignored string) {
	document, content := wasm.DisplayStart()

	inst, err := wasm.GetProjectsNamed(file)
	if err != nil {
		wasm.DisplayLocalErr(document, content, err.Error())
		return
	}

	wasm.DisplaySetTitleDescr(document, inst.Title, inst.Summary)
	content.Set("innerHTML", inst.Instructions)

	grades, err := wasm.GetGradesStudentProject(student, file)
	if err != nil {
		wasm.DisplayLocalErr(document, content, err.Error())
		return
	}

	table := wasm.GetElementByID(document, "record-table")

	for i, g := range grades {
		tr := wasm.CreateElement(document, "tr")
		if i == len(grades) - 1 {
			wasm.SetAttribute(tr, "class", "booktabs-bottom")
		}

		td := wasm.CreateElement(document, "td")
		wasm.SetAttribute(td, "class", "booktabs")
		wasm.AppendChild(td, wasm.CreateTextNode(document, g.Commit))
		wasm.AppendChild(tr, td)

		td = wasm.CreateElement(document, "td")
		wasm.SetAttribute(td, "class", "booktabs")
		wasm.AppendChild(td, wasm.CreateTextNode(document, g.Timestamp))
		wasm.AppendChild(tr, td)

		td = wasm.CreateElement(document, "td")
		wasm.SetAttribute(td, "class", "booktabs")
		wasm.AppendChild(td, wasm.CreateTextNode(document, g.Outcome))
		wasm.AppendChild(tr, td)

		wasm.AppendChild(table, tr)
	}

	parent := wasm.ParentElement(table)

	js.Global().Set("gradenow", js.FuncOf(func (this js.Value, args[]js.Value) interface{} {
		wasm.GetGradesNow(file)

		return nil
	}))

	button := wasm.CreateElement(document, "input")
	wasm.SetAttribute(button, "type", "button")
	wasm.SetAttribute(button, "onclick", "gradenow()")
	wasm.SetAttribute(button, "value", "Regrade")
	wasm.AppendChild(parent, button)
}
