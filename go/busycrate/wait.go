package main

import (
	"net/url"
	"syscall/js"
	"../wasm"
)

func wait(document, content js.Value, student, id, fragment string) {
	if err := wasm.GetWait(id); err != nil {
		wasm.DisplayLocalErr(document, content, err.Error())
		return
	}

	content.Set("innerHTML", fragment)
}

func waitRegister(file, student, params string) {
	title := "Register"
	descr := "Register for a new Aquinas account"
	document, content := wasm.DisplayStart()

	wasm.DisplaySetTitleDescr(document, title, descr)

	content.Set("innerHTML", "Creating your account. " +
	            "Establishing your Git repositories can take some time. " +
	            "Please wait ...")

	values, err := url.ParseQuery(params)
	if err != nil {
		wasm.DisplayLocalErr(document, content, err.Error())
		return
	}

	wait(document, content, student, values["id"][0], `Account established. Please <a href="/login">click here</a>`)
}
