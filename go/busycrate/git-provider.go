package main

import (
	"fmt"
	"syscall/js"
	"../wasm"
)

func toggle(x, y, z js.Value, set bool) {
	if set {
		wasm.RemoveAttribute(x, "disabled")
		wasm.RemoveAttribute(y, "disabled")
		wasm.RemoveAttribute(z, "disabled")
	} else {
		wasm.SetAttribute(x, "disabled", "")
		wasm.SetAttribute(y, "disabled", "")
		wasm.SetAttribute(z, "disabled", "")
	}
}

func gitProvider(file, student, ignored string) {
	title := "Change Git provider"
	descr := "Change the Git provider associated with your account"
	document, content := wasm.DisplayStart()

	wasm.DisplaySetTitleDescr(document, title, descr)

	gitProvider, gitPath, gitUsername, gitToken, err := wasm.GetGitProvider(student)
	if err != nil {
		wasm.DisplayLocalErr(document, content, err.Error())
		return
	}

	p    := wasm.CreateElement(document, "p")
	text := `You can elect to associate a third-party Git provider with
	         your Aquinas account. In this case, Aquinas will grade project
	         submissions you push to that provider upon request.`
	p.Set("innerHTML", text)
	wasm.AppendChild(content, p)

	p     = wasm.CreateElement(document, "p")
	text  = `Please ensure the Git repositories containing your work are
	         available only to you, your Git provider, and Aquinas. We ask
	         that you not share your work with others, according to the
	         Aquinas user agreement.`
	p.Set("innerHTML", text)
	wasm.AppendChild(content, p)

	form := wasm.CreateElement(document, "form")
	wasm.SetAttribute(form, "action", "/git-provider2")
	wasm.SetAttribute(form, "method", "post")

	wasm.AppendChild(form, wasm.CreateTextNode(document, "Git provider: "))

	provider := wasm.CreateElement(document, "select")
	wasm.SetAttribute(provider, "name", "git-provider")
	wasm.SetAttribute(provider, "onchange", "updateForm()")

	opt := wasm.CreateElement(document, "option")
	wasm.SetAttribute(opt, "value", "aquinas")
	if gitProvider == "aquinas" {
		wasm.SetAttribute(opt, "selected", "")
	}
	label := wasm.CreateTextNode(document, "Aquinas")
        wasm.AppendChild(opt, label)
        wasm.AppendChild(provider, opt)

	opt = wasm.CreateElement(document, "option")
	wasm.SetAttribute(opt, "value", "gitlab")
	if gitProvider == "gitlab" {
		wasm.SetAttribute(opt, "selected", "")
	}
	label = wasm.CreateTextNode(document, "GitLab")
        wasm.AppendChild(opt, label)
        wasm.AppendChild(provider, opt)

	wasm.AppendChild(form, provider)
	wasm.AppendChild(form, wasm.CreateElement(document, "br"))

	wasm.AppendChild(form, wasm.CreateTextNode(document, "https://"))

	username := wasm.CreateElement(document, "input")
        wasm.SetAttribute(username, "type", "text")
        wasm.SetAttribute(username, "name", "git-username")
        wasm.SetAttribute(username, "size", "10")
        wasm.SetAttribute(username, "placeholder", "Username")
        wasm.SetAttribute(username, "value", gitUsername)
        wasm.AppendChild(form, username)

	wasm.AppendChild(form, wasm.CreateTextNode(document, ":"))

	token := wasm.CreateElement(document, "input")
        wasm.SetAttribute(token, "type", "text")
        wasm.SetAttribute(token, "name", "git-token")
        wasm.SetAttribute(token, "size", "20")
        wasm.SetAttribute(token, "placeholder", "Token")
        wasm.SetAttribute(token, "value", gitToken)
        wasm.AppendChild(form, token)

	wasm.AppendChild(form, wasm.CreateTextNode(document, "@gitlab.com/"))

	path := wasm.CreateElement(document, "input")
        wasm.SetAttribute(path, "type", "text")
        wasm.SetAttribute(path, "name", "git-path")
        wasm.SetAttribute(path, "size", "10")
        wasm.SetAttribute(path, "placeholder", "Path")
        wasm.SetAttribute(path, "value", gitPath)
        wasm.AppendChild(form, path)

	wasm.AppendChild(form, wasm.CreateTextNode(document, "/PROJECT"))
	wasm.AppendChild(form, wasm.CreateElement(document, "br"))

	input := wasm.CreateElement(document, "input")
        wasm.SetAttribute(input, "type", "submit")
        wasm.SetAttribute(input, "value", "Submit")
        wasm.AppendChild(form, input)

	switch gitProvider {
	case "gitlab":
		toggle(username, token, path, true)
	default:
		toggle(username, token, path, false)
	}

	js.Global().Set("updateForm", js.FuncOf(func (this js.Value, args []js.Value) interface{} {
		switch provider.Get("value").String() {
		case "gitlab":
			toggle(username, token, path, true)
		default:
			toggle(username, token, path, false)
		}

		return nil
	}))

	wasm.AppendChild(content, form)

	location := wasm.GlobalGet("location")
	origin   := location.Get("origin")

	p     = wasm.CreateElement(document, "p")
	text  = fmt.Sprintf(`Name your projects on GitLab to match the Aquinas
	                     projects they solve. You can request that Aquinas
	                     grade PROJECT by issuing an HTTP GET request for
	                     %s/api/grades-now?project=PROJECT.`,
	                     origin)
	p.Set("innerHTML", text)
	wasm.AppendChild(content, p)
}
