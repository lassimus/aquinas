package main

import (
	"net/url"
	"../wasm"
)

func alias2(file, student, params string) {
	title := "Change alias"
	descr := "Change the public alias others will know you by"
	document, content := wasm.DisplayStart()

	wasm.DisplaySetTitleDescr(document, title, descr)

	values, err := url.ParseQuery(params)
	if err != nil {
		wasm.DisplayLocalErr(document, content, err.Error())
		return
	}

	if _, ok := values["alias"]; !ok {
		wasm.DisplayLocalErr(document, content, "missing alias value")
		return
	}

	if err := wasm.PostAlias(student, values["alias"][0]); err != nil {
		wasm.DisplayLocalErr(document, content, err.Error())
	}

	p    := wasm.CreateElement(document, "p")
	text := `Alias changed. Please <a href="/">click</a> to continue.`
	p.Set("innerHTML", text)
	wasm.AppendChild(content, p)
}
