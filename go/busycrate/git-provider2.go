package main

import (
	"net/url"
	"../wasm"
)

func gitProvider2(file, student, params string) {
	var provider, path, username, token []string

	title := "Change Git provider"
	descr := "Change the Git provider associated with your account"
	document, content := wasm.DisplayStart()

	wasm.DisplaySetTitleDescr(document, title, descr)

	values, err := url.ParseQuery(params)
	if err != nil {
		wasm.DisplayLocalErr(document, content, err.Error())
		return
	}

	provider, ok := values["git-provider"]
	if !ok {
		wasm.DisplayLocalErr(document, content, "missing git-provider value")
		return
	}

	switch provider[0] {
	case "gitlab":
		var ok bool

		path, ok = values["git-path"]
		if !ok {
			wasm.DisplayLocalErr(document, content, "missing git-path value")
			return
		}

		username, ok = values["git-username"]
		if !ok {
			wasm.DisplayLocalErr(document, content, "missing git-username value")
			return
		}

		token, ok = values["git-token"]
		if !ok {
			wasm.DisplayLocalErr(document, content, "missing git-token value")
			return
		}
	default:
		path = []string{""}
		username = []string{""}
		token = []string{""}
	}

	if err := wasm.PostGitProvider(student,
	                               provider[0],
	                               path[0],
	                               username[0],
	                               token[0]); err != nil {
		wasm.DisplayLocalErr(document, content, err.Error())
	}

	p    := wasm.CreateElement(document, "p")
	text := `Git provider changed. Please <a href="/">click</a> to continue.`
	p.Set("innerHTML", text)
	wasm.AppendChild(content, p)
}
