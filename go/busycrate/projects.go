package main

import (
	"fmt"
	"../kahn"
	"path"
	"../proj"
	"strings"
	"syscall/js"
	"../wasm"
)

var projectList map[string]proj.Project

func passFailMap(grades []proj.Grade) map[string]bool {
	m := map[string]bool{}

	for _, g := range grades {
		if g.Outcome == "PASS" {
			m[g.Project] = true
		} else {
			m[g.Project] = false
		}
	}

	return m
}

func tagsMap() map[string]bool {
	tags := map[string]bool{}

	for _, v := range projectList {
		for tag := range v.TagsF {
			tags[tag] = true
		}
	}

	return tags
}

func addDropDown(document js.Value, parent js.Value) {
	dd := wasm.CreateElement(document, "select")
	wasm.SetAttribute(dd, "id", "mode")
	wasm.SetAttribute(dd, "onchange", "toggleView()")

	wasm.AppendChild(parent, dd)

	opt := wasm.CreateElement(document, "option")
	wasm.SetAttribute(opt, "value", "list")
	label := wasm.CreateTextNode(document, "list")
	wasm.AppendChild(opt, label)
	wasm.AppendChild(dd, opt)

	opt = wasm.CreateElement(document, "option")
	wasm.SetAttribute(opt, "value", "graph")
	label = wasm.CreateTextNode(document, "Graph")
	wasm.AppendChild(opt, label)
	wasm.AppendChild(dd, opt)
}

func addRadio(document js.Value, parent js.Value, category string) {
	radio := wasm.CreateElement(document, "input")

	wasm.SetAttribute(radio, "type", "radio")
	wasm.SetAttribute(radio, "name", "language")
	wasm.SetAttribute(radio, "onclick", fmt.Sprintf("update('%s');", category))
	wasm.AppendChild(parent, radio)

	label := wasm.CreateTextNode(document, category)
	wasm.AppendChild(parent, label)
}

func addList(document, content js.Value, parent js.Value, passed map[string]bool) {
	nodes := make(kahn.Node)

	for _, v := range projectList {
		/*
		 * TODO: I would like to clean this up. We need the name,
		 * unadorned by language here. Httpd and below need the name
		 * with language. We have projects and projectListItems, and it
		 * would be nice to unify these. Note that projects can have
		 * sensitive portions such as the checks.
		 */

		prerequisites := map[string]bool{}

		for _, v2 := range v.AbstractPrerequisitesF {
			prerequisites[v2] = true
		}

		/*
		 * Use of map prevents duplicates, as projects can have multiple
		 * languages.
		 */
		nodes[v.AbstractNameF] = prerequisites
	}

	sorted, err := kahn.Kahn(nodes)
	if err != nil {
		wasm.DisplayLocalErr(document, content, err.Error())
		return
	}

	for _, k := range sorted {
	/* TODO: We need the name with language here (see above). */
		for k2, v2 := range projectList {
			if k != v2.AbstractNameF {
				continue
			}

			li := wasm.CreateElement(document, "li")
			class1 := "project " + v2.LanguageF
			class2 := "link "    + v2.LanguageF
			if !v2.GradedF {
				class1 += " ungraded"
			} else if pass, ok := passed[k2]; ok {
				if pass {
					class1 += " done"
				} else {
					class1 += " failed"
				}
			}

			for t := range v2.TagsF {
				class1 += " " + t
				class2 += " " + t
			}

			wasm.SetAttribute(li, "class", class1)
			record := fmt.Sprintf(`<a href="%s" class="%s">%s</a>: %s`, path.Join("project", k2), class2, v2.NameF, v2.SummaryF)
			li.Set("innerHTML", record)
			wasm.AppendChild(parent, li)
		}
	}
}

func updateList(document js.Value, parent js.Value, category string) {
	projects := wasm.GetElementsByClassName(document, "project")
	for i := 0; i < projects.Length(); i++ {
		project := projects.Index(i)
		class := wasm.GetAttribute(project, "class")

		if category == "All" ||
		   strings.Contains(class, "none") ||
		   strings.Contains(class, category) {
			wasm.SetAttribute(project, "style", "color: black;")
		} else {
			wasm.SetAttribute(project, "style", "color: lightgray;")
		}
	}

	projects = wasm.GetElementsByClassName(document, "link")
	for i := 0; i < projects.Length(); i++ {
		project := projects.Index(i)
		class := wasm.GetAttribute(project, "class")

		if category == "All" ||
		   strings.Contains(class, "none") ||
		   strings.Contains(class, category) {
			wasm.SetAttribute(project, "style", "pointerEvents: auto; cursor: auto; color: #28c;")
		} else {
			wasm.SetAttribute(project, "style", "pointerEvents: none; cursor: default; color: lightgray;")
		}
	}

	return
}

func addGraph(document js.Value, parent js.Value, passed map[string]bool) {
	updateGraph(document, parent, "C", passed)

	return
}

func updateGraph(document js.Value, parent js.Value, category string, passed map[string]bool) {
	ids := map[string]string{}
	links := map[string]string{}
	nextID := 0

	/* Gather directly-relevant nodes. */
	g := "graph TD;"
	for k, v := range projectList {
		if _, ok := v.TagsF[category]; !ok &&
		   category != "All" &&
		   v.LanguageF != category &&
		   v.LanguageF != "none" &&
		   v.LanguageF != "AMD64" {
			continue
		}

		ids[v.NameF] = fmt.Sprintf("id%d", nextID)
		links[v.NameF] = k
		nextID++
	}

	/* Build dependencies of directly-relevant nodes and gather them. */
	for _, p := range projectList {
		if _, ok := ids[p.NameF]; !ok {
			continue
		}

		if len(p.AbstractPrerequisitesF) > 0 {
			for _, prereq := range p.AbstractPrerequisitesF {
				var id string
				var ok bool

				if id, ok = ids[prereq + " in " + category]; ok {
					goto mark
				}

				/* E.g., "syscall in AMD64" -> "shellcode in C". */
				if id, ok = ids[prereq + " in AMD64"]; ok {
					goto mark
				}

				/* E.g., "hello in C" -> "syscall in AMD64". */
				if id, ok = ids[prereq + " in C"]; ok {
					goto mark
				}

				for k, p2 := range projectList {
					//if proj.name == prereq + " in C" {
					if p2.NameF == prereq + " in " + category {
						id = fmt.Sprintf("id%d", nextID)
						ids[p2.NameF] = id
						links[p2.NameF] = k
						nextID++
						goto mark
					}
				}

				if id, ok = ids[prereq]; ok {
					goto mark;
				}

				id = fmt.Sprintf("id%d", nextID)
				ids[prereq] = id
				links[prereq] = "#"
				nextID++

mark:
				g += fmt.Sprintf(`%s-->%s;`, id, ids[p.NameF])
			}
		}
	}

	for k, v := range ids {
		if passed[links[k]] {
			g += fmt.Sprintf(`%s["✓ %s"]; click %s "%s" "%s";`, v, k, v, path.Join("project", links[k]), k)
			g += fmt.Sprintf(`style %s color:gray,stroke:gray;`, v)
		} else {
			g += fmt.Sprintf(`%s["%s"]; click %s "%s" "%s";`, v, k, v, path.Join("project", links[k]), k)
			g += fmt.Sprintf(`style %s color:black,stroke:black;`, v)
		}
	}

	js.Global().Get("mermaid").Call("render", "graph", g, js.FuncOf(func (this js.Value, args []js.Value) interface{} {
		parent.Set("innerHTML", args[0].String())
		return nil
	}))
}

func clear(node js.Value) {
	node.Set("textContent", "")
}

func projects(file, student, ignored string) {
	var err error

	graphOn := false
	listOn  := true
	grades  := []proj.Grade{}

	title := "Project list"
	descr := "The range of projects to complete"
	document, content := wasm.DisplayStart()

	wasm.DisplaySetTitleDescr(document, title, descr)

	projectList, err = wasm.GetProjects()
	if err != nil {
		wasm.DisplayLocalErr(document, content, err.Error())
		return
	}

	if student != "" {
		grades, err = wasm.GetGradesStudentBrief(student)
		if err != nil {
			wasm.DisplayLocalErr(document, content, err.Error())
			return
		}
	}

	passed := passFailMap(grades)

	addDropDown(document, content)

	addRadio(document, content, "All")
	addRadio(document, content, "AMD64")
	addRadio(document, content, "Bourne")
	addRadio(document, content, "C")
	addRadio(document, content, "Go")
	addRadio(document, content, "Java")
	addRadio(document, content, "Python")

	for tag := range tagsMap() {
		addRadio(document, content, tag)
	}

	divGraph := wasm.CreateElement(document, "div")
	wasm.SetAttribute(divGraph, "class", "mermaid")
	wasm.AppendChild(content, divGraph)
	/* Do not call addGraph until selected (see toggleView()). */

	ulProjects := wasm.CreateElement(document, "ul")
	wasm.SetAttribute(ulProjects, "class", "checklist")
	addList(document, content, ulProjects, passed)

	wasm.AppendChild(content, ulProjects)

	js.Global().Set("toggleView", js.FuncOf(func (this js.Value, args []js.Value) interface{} {
		if graphOn {
			clear(divGraph)
		} else {
			addGraph(document, divGraph, passed)
		}

		if listOn {
			clear(ulProjects)
		} else {
			addList(document, content, ulProjects, passed)
		}

		graphOn = !graphOn
		listOn  = !listOn

		return nil
	}))

	js.Global().Set("update", js.FuncOf(func (this js.Value, args []js.Value) interface{} {
		if listOn {
			category := args[0].String()

			updateList(document, ulProjects, category)
		}

		if graphOn {
			category := args[0].String()

			clear(divGraph)
			updateGraph(document, divGraph, category, passed)
		}

		return nil
	}))
}
