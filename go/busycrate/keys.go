package main

import (
	"../wasm"
)

func keys(file, student, ignored string) {
	title := "Set SSH key(s)"
	descr := "Set the public SSH key(s) associated with your Aquinas account"
	document, content := wasm.DisplayStart()

	wasm.DisplaySetTitleDescr(document, title, descr)

	keys, err := wasm.GetKeys(student)
	if err != nil {
		wasm.DisplayLocalErr(document, content, err.Error())
		return
	}

	p    := wasm.CreateElement(document, "p")
	text := `Before you can complete your first project, you must provide
	         Aquinas with your public SSH key. The keys you provide here
	         must be encoded in the format used by OpenSSH. If you use a
	         different SSH client, then it might be necessary to
	         transform its keys into the OpenSSH format before
	         submitting the result. You will need to look elsewhere for
	         instructions on how to perform such a transformation.`
	p.Set("innerHTML", text)
	wasm.AppendChild(content, p)

	p     = wasm.CreateElement(document, "p")
	text  = `If you do not yet have at least one SSH keypair, then you
	         can generate one by running <code>ssh-keygen</code>. We expect that you
	         will accept the default storage location when prompted.`
	p.Set("innerHTML", text)
	wasm.AppendChild(content, p)

	p     = wasm.CreateElement(document, "p")
	text  = `Once you have a key, you must enter it in the form below.
	         Print your key using <code>cat ~/.ssh/id_rsa.pub</code>, and copy and
	         paste it below.`
	p.Set("innerHTML", text)
	wasm.AppendChild(content, p)

	form := wasm.CreateElement(document, "form")
	wasm.SetAttribute(form, "action", "/keys2")
	wasm.SetAttribute(form, "method", "post")
	wasm.AppendChild(content, form)

	textarea := wasm.CreateElement(document, "textarea")
	wasm.SetAttribute(textarea, "rows", "6")
	wasm.SetAttribute(textarea, "cols", "80")
	wasm.SetAttribute(textarea, "name", "key")
	wasm.SetAttribute(textarea, "placeholder", "SSH key")
	wasm.AppendChild(textarea, wasm.CreateTextNode(document, keys))
	wasm.AppendChild(form, textarea)

	br := wasm.CreateElement(document, "br")
	wasm.AppendChild(form, br)

	input := wasm.CreateElement(document, "input")
	wasm.SetAttribute(input, "type", "submit")
	wasm.SetAttribute(input, "value", "Submit")
	wasm.AppendChild(form, input)
}
