package main

import (
	"net/url"
	"../wasm"
)

func password2(file, student, params string) {
	title := "Change password"
	descr := "Change the password associated with your Aquinas account"
	document, content := wasm.DisplayStart()

	wasm.DisplaySetTitleDescr(document, title, descr)

	values, err := url.ParseQuery(params)
	if err != nil {
		wasm.DisplayLocalErr(document, content, err.Error())
		return
	}

	if values.Get("password") != values.Get("password2") {
		wasm.DisplayLocalErr(document, content, "Passwords do not match")
		return
	}

	err = wasm.PostPassword(student, values.Get("password"))
	if err != nil {
		wasm.DisplayLocalErr(document, content, err.Error())
		return
	}

	p := wasm.CreateElement(document, "p")
	text := `Password changed. Please <a href="/">click</a> to continue.`
	p.Set("innerHTML", text)
	wasm.AppendChild(content, p)
}
