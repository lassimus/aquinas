package main

import (
	"strings"
	"../wasm"
)

func keys2(file, student, params string) {
	title := "Set SSH Key(s)"
	descr := "Set the public SSH key(s) associated with your Aquinas account"
	document, content := wasm.DisplayStart()

	wasm.DisplaySetTitleDescr(document, title, descr)

	/*
	 * Avoid HTTP Get and url.ParseQuery to avoid replacing '+' with ' '.
	 * Body of key (after first '=') might contain '=', but we restore these
	 * with the Join below.
	 */
	s := strings.Split(params, "=")
	if len(s) < 2 {
		wasm.DisplayLocalErr(document, content, "bad query: " + params)
		return
	}

	if err := wasm.PostKeys(student, strings.Join(s[1:], "=")); err != nil {
		wasm.DisplayLocalErr(document, content, err.Error())
		return
	}

	p    := wasm.CreateElement(document, "p")
	text := `Key installed. Please <a href="/">click</a> to continue.`
	p.Set("innerHTML", text)
	wasm.AppendChild(content, p)
}
