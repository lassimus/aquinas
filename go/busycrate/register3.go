package main

import (
	"net/url"
	"../wasm"
)

func register3(file, student, params string) {
	title := "Register"
	descr := "Register a new Aquinas account"
	document, content := wasm.DisplayStart()

	wasm.DisplaySetTitleDescr(document, title, descr)

	values, err := url.ParseQuery(params)
	if err != nil {
		wasm.DisplayLocalErr(document, content, err.Error())
		return
	}

	student = values["student"][0]
	values.Del("student")

	id, err := wasm.PostRegister(student, values)
	if err != nil {
		wasm.DisplayLocalErr(document, content, err.Error())
		return
	}

	content.Set("innerHTML", "Creating your account. " +
	            "Establishing your Git repositories can take some time. " +
	            "Please wait ...")

	if err := wasm.GetWait(id); err != nil {
                wasm.DisplayLocalErr(document, content, err.Error())
                return
        }

	fragment := `Account established. Please click <a href="/login">here</a>.`
	content.Set("innerHTML", fragment)
}
