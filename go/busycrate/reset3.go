package main

import (
	"net/url"
	"../wasm"
)

func reset3(file, student, params string) {
	title := "Reset password"
	descr := "Reset the password associated with your Aquinas account"
	document, content := wasm.DisplayStart()

	wasm.DisplaySetTitleDescr(document, title, descr)

	values, err := url.ParseQuery(params)
	if err != nil {
		wasm.DisplayLocalErr(document, content, err.Error())
		return
	}

	student = values["student"][0]
	values.Del("student")

	if err := wasm.PostResetPassword(student, values); err != nil {
		wasm.DisplayLocalErr(document, content, err.Error())
		return
	}

	window := wasm.GlobalGet("window")
	location := window.Get("location")
	location.Call("replace", "/login")

	p    := wasm.CreateElement(document, "p")
	text := `Please <a href="/login">click here</a>.`
	p.Set("innerHTML", text)
	wasm.AppendChild(content, p)
}
