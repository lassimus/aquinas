package main

import (
	"fmt"
	"net/url"
	"../wasm"
)

func reset2(file, student, params string) {
	title := "Reset password"
	descr := "Reset the password associated with your Aquinas account"
	document, content := wasm.DisplayStart()

	wasm.DisplaySetTitleDescr(document, title, descr)

	values, err := url.ParseQuery(params)
	if err != nil {
		wasm.DisplayLocalErr(document, content, err.Error())
		return
	}

	student = values["student"][0]
	values.Del("student")

	if err := wasm.GetResetPassword(student, params); err != nil {
		wasm.DisplayLocalErr(document, content, err.Error())
		return
	}

	p    := wasm.CreateElement(document, "p")
	text := fmt.Sprintf("We have received your request, %s.", student)
	p.Set("innerHTML", text)
	wasm.AppendChild(content, p)

	p     = wasm.CreateElement(document, "p")
	text  = `Please watch for an email containing your password and
	         instructions that describe how to complete
	         your password reset.`
	p.Set("innerHTML", text)
	wasm.AppendChild(content, p)
}
