package main

import (
	"os"
)

func run() {
	/* Block, allowing callbacks to execute. */
	<-make(chan struct{}, 0)
}

func main() {
	switch os.Args[0] {
		case "alias":
			alias(os.Args[0], os.Args[1], os.Args[2])
		case "alias2":
			alias2(os.Args[0], os.Args[1], os.Args[2])
		case "book":
			book(os.Args[0], os.Args[1], os.Args[2])
		case "course":
			course(os.Args[0], os.Args[1], os.Args[2])
		case "git-provider":
			gitProvider(os.Args[0], os.Args[1], os.Args[2])
		case "git-provider2":
			gitProvider2(os.Args[0], os.Args[1], os.Args[2])
		case "keys":
			keys(os.Args[0], os.Args[1], os.Args[2])
		case "keys2":
			keys2(os.Args[0], os.Args[1], os.Args[2])
		case "password2":
			password2(os.Args[0], os.Args[1], os.Args[2])
		case "projects":
			projects(os.Args[0], os.Args[1], os.Args[2])
		case "rankings":
			rankings(os.Args[0], os.Args[1], os.Args[2])
		case "register2":
			register2(os.Args[0], os.Args[1], os.Args[2])
		case "register3":
			register3(os.Args[0], os.Args[1], os.Args[2])
		case "remove2":
			remove2(os.Args[0], os.Args[1], os.Args[2])
		case "reset2":
			reset2(os.Args[0], os.Args[1], os.Args[2])
		case "reset3":
			reset3(os.Args[0], os.Args[1], os.Args[2])
		case "waitRegister":
			waitRegister(os.Args[0], os.Args[1], os.Args[2])
		default:
			project(os.Args[0], os.Args[1], os.Args[2])
	}

	run()
}
