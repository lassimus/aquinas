package main

import (
	"fmt"
	"sort"
	"../wasm"
)

func rankings(file, student, ignored string) {
	title := "Rankings"
	descr := "Student rankings"
	document, content := wasm.DisplayStart()

	wasm.DisplaySetTitleDescr(document, title, descr)

	grades, err := wasm.GetGradesBrief()
	if err != nil {
		wasm.DisplayLocalErr(document, content, err.Error())
		return
	}

	sums := make(map[string]int)
	for _, g := range grades {
		if g.Outcome != "PASS" {
			continue
		}
		if _, ok := sums[g.Student]; ok {
			sums[g.Student]++
		} else {
			sums[g.Student] = 1
		}
	}

	rankings := make(map[int][]string)
	for k, v := range sums {
		if _, ok := rankings[v]; ok {
			rankings[v] = append(rankings[v], k)
		} else {
			rankings[v] = []string{k}
		}
	}

	keys := []int{}
	for k := range rankings {
		keys = append(keys, k)
	}
	sort.Sort(sort.Reverse(sort.IntSlice(keys)))

	gradeList := wasm.CreateElement(document, "ol")

	for _, k := range keys {
		plural := ""
		if k != 1 {
			plural = "s"
		}
		entry := wasm.CreateElement(document, "li")
		e := fmt.Sprintf("%s with %d solution%s.", renderList(rankings[k]), k, plural)
		wasm.AppendChild(entry, wasm.CreateTextNode(document, e))
		wasm.AppendChild(gradeList, entry)
	}

	wasm.AppendChild(content, gradeList)
}
