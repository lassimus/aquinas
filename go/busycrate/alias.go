package main

import (
	"../wasm"
)

func alias(file, student, ignored string) {
	title := "Change alias"
	descr := "Change the public alias others will know you by"
	document, content := wasm.DisplayStart()

	wasm.DisplaySetTitleDescr(document, title, descr)

	alias, err := wasm.GetAlias(student)
	if err != nil {
		wasm.DisplayLocalErr(document, content, err.Error())
		return
	}

	p    := wasm.CreateElement(document, "p")
	text := `Aquinas uses your alias when reporting statistics such as
	         rankings to other students. This avoids sharing your email
	         address, and it puts you in control of whether these reports
	         correlate your performance with your real-world identity.
	         Teachers are able to identify students by their email
	         addresses, but Aquinas does not share this information
	         publicly.`
	p.Set("innerHTML", text)
	wasm.AppendChild(content, p)

	p     = wasm.CreateElement(document, "p")
	text  = `Please use discretion when selecting your alias,
	         and be respectful of the sentiments of your fellow students.`
	p.Set("innerHTML", text)
	wasm.AppendChild(content, p)

	form := wasm.CreateElement(document, "form")
	wasm.SetAttribute(form, "action", "/alias2")
	wasm.SetAttribute(form, "method", "post")
	wasm.AppendChild(content, form)

	wasm.AppendChild(form, wasm.CreateTextNode(document, "Alias: "))

	input := wasm.CreateElement(document, "input")
	wasm.SetAttribute(input, "type", "text")
	wasm.SetAttribute(input, "name", "alias")
	wasm.SetAttribute(input, "placeholder", "Alias")
	wasm.SetAttribute(input, "value", alias)
	wasm.AppendChild(form, input)

	input  = wasm.CreateElement(document, "input")
	wasm.SetAttribute(input, "type", "submit")
	wasm.SetAttribute(input, "value", "Submit")
	wasm.AppendChild(form, input)
}
