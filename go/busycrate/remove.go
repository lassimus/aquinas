package main

import (
	"errors"
	"net/http"
	"net/url"
	"time"
	"../wasm"
)

func remove2(file, student, params string) {
	title := "Remove account"
	descr := "PERMANENTLY remove your Aquinas account"
	document, content := wasm.DisplayStart()

	wasm.DisplaySetTitleDescr(document, title, descr)

	values, err := url.ParseQuery(params)
	if err != nil {
		wasm.DisplayLocalErr(document, content, err.Error())
		return
	}

	if values.Get("student") != student {
		wasm.DisplayLocalErr(document, content, "failed student name check")
		return
	}

	if err := wasm.DeleteStudentsNamed(student); err != nil {
		wasm.DisplayLocalErr(document, content, err.Error())
		return
	}

	errChan := make(chan error, 1)
	timeout := 200 * time.Millisecond

	req, err := http.NewRequest("GET", "projects", nil)
	if err != nil {
		wasm.DisplayLocalErr(document, content, err.Error())
		return
	}

	req.Header.Set("Authorization", "Basic logout")
	client := &http.Client{
		Timeout: timeout + 5 * time.Second,
	}

	go func() {
		resp, err := client.Do(req)
		if err != nil {
			errChan <- err
			return
		}
		defer resp.Body.Close()

		if resp.StatusCode != http.StatusOK {
			errChan <- errors.New("Server returned non-200 status")
		}

		errChan <- nil
	}()

	select {
	case err := <-errChan:
		if err != nil {
			wasm.DisplayLocalErr(document, content, err.Error())
			return
		}
	case <-time.After(timeout):
		break
	}

	window := wasm.GlobalGet("window")
	location := window.Get("location")
	location.Call("replace", "/landing")
}
