package main

import (
	"fmt"
	"../wasm"
)

func course(file, student, ignored string) {
	title := "Challenge"
	descr := "Current Aquinas challenge"
	document, content := wasm.DisplayStart()

	wasm.DisplaySetTitleDescr(document, title, descr)

	/* TODO: allow selecting different courses; this is Pintos exercises. */
	course, err := wasm.GetCourse("OS")
	if err != nil {
		wasm.DisplayLocalErr(document, content, err.Error())
		return
	}

	grades, err := wasm.GetGradesBrief()
	if err != nil {
		wasm.DisplayLocalErr(document, content, err.Error())
		return
	}

	students := map[string][]string{}

	for _, g := range grades {
		if g.Outcome != "PASS" {
			continue
		}
		if _, ok := course[g.Project]; ok {
			if _, ok := students[g.Student]; ok {
				students[g.Student] = append(students[g.Student], g.Project)
			} else {
				students[g.Student] = []string{g.Project}
			}
		}
	}

	gradeList := wasm.CreateElement(document, "ol")

	for k, v := range students {
		entry := wasm.CreateElement(document, "li")
		e := fmt.Sprintf("%s solved %s.", k, renderList(v))
		if len(v) == len(course) {
			e += " Exceptional work!"
		} else if len(v) >= len(course) - 1 {
			e += " Nice work!"
		}
		wasm.AppendChild(entry, wasm.CreateTextNode(document, e))
		wasm.AppendChild(gradeList, entry)
	}

	wasm.AppendChild(content, gradeList)
}
