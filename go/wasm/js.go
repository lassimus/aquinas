package wasm

import (
	"syscall/js"
)

// GlobalGet returns an object from JavaScript's global namespace.
func GlobalGet(name string) js.Value {
	return js.Global().Get(name)
}

// GetElementsByClassName returns a DOM element by name.
func GetElementsByClassName(v js.Value, class string) js.Value {
	return v.Call("getElementsByClassName", class)
}

// GetElementByID returns a DOM element by ID.
func GetElementByID(v js.Value, id string) js.Value {
	return v.Call("getElementById", id)
}

// CreateElement returns a newly-created DOM element.
func CreateElement(v js.Value, typ string) js.Value {
	return v.Call("createElement", typ)
}

// CreateTextNode returns a newly-created DOM text node.
func CreateTextNode(v js.Value, text string) js.Value {
	return v.Call("createTextNode", text)
}

// AppendChild appends one DOM element as a child of another.
func AppendChild(v1, v2 js.Value) {
	v1.Call("appendChild", v2)
}

// SetAttribute sets the named attribute of a DOM element.
func SetAttribute(v js.Value, key, value string) {
	v.Call("setAttribute", key, value)
}

// GetAttribute gets the named attribute of a DOM element.
func GetAttribute(v js.Value, key string) string {
	return v.Call("getAttribute", key).String()
}

// RemoveAttribute removes the named attribute from a DOM element.
func RemoveAttribute(v js.Value, key string) string {
	return v.Call("removeAttribute", key).String()
}

// ParentElement returns a DOM element's parent.
func ParentElement(v js.Value) js.Value {
	return v.Get("parentElement")
}

// AddEventListener add a listener to a DOM element.
func AddEventListener(v js.Value, event string, f js.Func) {
	v.Call("addEventListener", event, f)
}

func displayErr(document, element js.Value, msg string) {
	p := document.Call("createElement", "p")
        p.Set("innerHTML", msg)
	AppendChild(element, p)
}

// DisplayLocalErr displays a local error.
func DisplayLocalErr(document, element js.Value, msg string) {
	displayErr(document, element, "Local error: " + msg)
}

// DisplayRemoteErr displays a remote error.
func DisplayRemoteErr(document, element js.Value, msg string) {
	displayErr(document, element, msg)
}

// DisplayStart returns the DOM's document and main-content elements.
func DisplayStart() (document, content js.Value) {
	document = GlobalGet("document")
	content = GetElementsByClassName(document, "main-content").Index(0)

	return
}

// DisplaySetTitleDescr sets the DOM's title and description.
func DisplaySetTitleDescr(document js.Value, title, descr string) {
	document.Set("title", title + " | Aquinas")

	t := GetElementsByClassName(document, "page-title").Index(0)
	t.Set("innerHTML", title)

	d := GetElementsByClassName(document, "page-subtitle").Index(0)
	d.Set("innerHTML", descr)
}
