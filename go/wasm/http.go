package wasm

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"../proj"
)

func httpDelete(url string) (resp *http.Response, err error) {
	var req *http.Request
	client := &http.Client{}

	req, err = http.NewRequest("DELETE", url, nil)
	if err != nil {
		return
	}

	resp, err = client.Do(req)
	if err != nil {
		return
	}

	return
}

func getAndUnmarshal(url string, output interface{}) error {
	resp, err := http.Get(url)
        if err != nil {
                return err
        }
        defer resp.Body.Close()

        data, err := ioutil.ReadAll(resp.Body)
        if err != nil {
                return err
        }

        if resp.StatusCode != http.StatusOK {
                return errors.New("bad status code")
        }

        err = json.Unmarshal(data, output)
        if err != nil {
                return err
        }

	return nil
}

func getStudent(student, k string) (v string, err error) {
	record := map[string]string{}

	url := fmt.Sprintf("/api/students/%s", student)
	if err := getAndUnmarshal(url, &record); err != nil {
		return "", err
	}

	return record[k], nil
}

// GetAlias returns the student's alias.
func GetAlias(student string) (alias string, err error) {
	return getStudent(student, "alias")
}

// GetKeys returns the student's SSH keys.
func GetKeys(student string) (keys string, err error) {
	return getStudent(student, "keys")
}

// GetGitProvider returns information that describes the student's Git provider.
func GetGitProvider(student string) (gitProvider, gitPath, gitUsername, gitToken string, err error) {
	record := map[string]string{}

	url := fmt.Sprintf("/api/students/%s", student)
	if err := getAndUnmarshal(url, &record); err != nil {
		return "", "", "", "", err
	}

	return record["git-provider"], record["git-path"], record["git-username"], record["git-token"], nil
}

// GetGradesBrief returns a summary of all grades.
func GetGradesBrief() (grades []proj.Grade, err error) {
	err = getAndUnmarshal("/api/grades?brief=true", &grades)
	return
}

// GetGradesStudentBrief returns a summary of the student's grades.
func GetGradesStudentBrief(student string) (grades []proj.Grade, err error) {
	url := fmt.Sprintf("/api/grades?student=%s&brief=true", student)
	err = getAndUnmarshal(url, &grades)
	return
}

// GetGradesStudentProject returns a student's grades for the given project.
func GetGradesStudentProject(student, project string) (grades []proj.Grade, err error) {
	url := fmt.Sprintf("/api/grades?student=%s&project=%s", student, project)
	err = getAndUnmarshal(url, &grades)
	return
}

// GetGradesNow triggers a grading sequence and returns the project's grades
// when it finishes.
func GetGradesNow(project string) (grades []proj.Grade, err error) {
	url := fmt.Sprintf("/api/grades-now?project=%s", project)
	err = getAndUnmarshal(url, &grades)
	return
}

// GetProjects returns the list of projects.
func GetProjects() (projectList map[string]proj.Project, err error) {
	url := "/api/projects"
	err = getAndUnmarshal(url, &projectList)
	return
}

// GetCourse returns the list of projects in the given course.
func GetCourse(course string) (projectList map[string]proj.Project, err error) {
	url := fmt.Sprintf("/api/projects?tag=%s", course)
	err = getAndUnmarshal(url, &projectList)
	return
}

// GetProjectsNamed returns the instructions associated with the given project.
func GetProjectsNamed(project string) (inst proj.Instructions, err error) {
	url := fmt.Sprintf("/api/projects/%s", project)
	err = getAndUnmarshal(url, &inst)
	return
}

func getRegisterReset(op, student, params string) (err error) {
	ok := false
	url := fmt.Sprintf("/api/" + op + "/%s?%s", student, params)
	err = getAndUnmarshal(url, &ok)
	if err == nil && !ok {
		err = errors.New("received unexpected response")
	}

	return err
}

// GetRegister returns the information needed to register.
func GetRegister(student, params string) error {
	return getRegisterReset("register", student, params)
}

// GetResetPassword returns the information needed to reset the student's
// password.
func GetResetPassword(student, params string) error {
	return getRegisterReset("reset-password", student, params)
}

// GetWait blocks until the job with the given ID completes.
func GetWait(id string) (err error) {
	ok := false
	url := fmt.Sprintf("/api/wait?id=%s", id)
	err = getAndUnmarshal(url, &ok)
	if err == nil && !ok {
		err = errors.New("received unexpected response")
	}

	return err
}

func postAndUnmarshal(url string, values url.Values, output interface{}) error {
	resp, err := http.PostForm(url, values)
        if err != nil {
                return err
        }
        defer resp.Body.Close()

        data, err := ioutil.ReadAll(resp.Body)
        if err != nil {
                return err
        }

        if resp.StatusCode != http.StatusOK {
                return errors.New("bad status code")
        }

        if err := json.Unmarshal(data, output); err != nil {
                return err
        }

	return nil
}

func postStudent(student, k, v string) (err error) {
	values := url.Values{k: []string{v}}

	ok := false
	url := fmt.Sprintf("/api/students/%s", student)
	err = postAndUnmarshal(url, values, &ok)
	if err == nil && !ok {
		err = errors.New("received unexpected response")
	}

	return err
}

// PostAlias sets the student's alias.
func PostAlias(student, alias string) (err error) {
	return postStudent(student, "alias", alias)
}

// PostKeys sets the student's SSH keys.
func PostKeys(student, keys string) (err error) {
	return postStudent(student, "keys", keys)
}

// PostPassword sets the student's password.
func PostPassword(student, password string) (err error) {
	return postStudent(student, "password", password)
}

// PostGitProvider sets the information that describes the student's Git provider.
func PostGitProvider(student, gitProvider, gitPath, gitUsername, gitToken string) (err error) {
	values := url.Values{
		"git-provider": []string{gitProvider},
	}

	if gitPath != "" {
		values["git-path"] = []string{gitPath}
	}

	if gitUsername != "" {
		values["git-username"] = []string{gitUsername}
	}

	if gitToken != "" {
		values["git-token"] = []string{gitToken}
	}

	ok := false
	url := fmt.Sprintf("/api/students/%s", student)
	err = postAndUnmarshal(url, values, &ok)
	if err == nil && !ok {
		err = errors.New("received unexpected response")
	}

	return err
}

// PostRegister completes a student registration.
func PostRegister(student string, values url.Values) (id string, err error) {
	url := fmt.Sprintf("/api/register/%s", student)
	err = postAndUnmarshal(url, values, &id)
	return id, err
}

// PostResetPassword completes a student password reset.
func PostResetPassword(student string, values url.Values) (err error) {
	ok := false
	url := fmt.Sprintf("/api/reset-password/%s", student)
	err = postAndUnmarshal(url, values, &ok)
	if err == nil && !ok {
		err = errors.New("received unexpected response")
	}
	return err
}

// DeleteStudentsNamed deletes the given student.
func DeleteStudentsNamed(student string) error {
	resp, err := httpDelete(fmt.Sprintf("/api/students/%s", student))
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	if resp.StatusCode != http.StatusOK {
                return errors.New("bad status code")
	}

	ok := false
	if err := json.Unmarshal(data, &ok); err != nil {
		return err
	}

	if !ok {
		return errors.New("received unexpected response")
	}

	return nil
}
