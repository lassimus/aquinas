package proj

type projectNone PrivateProject

func newProjectNone(ap AbstractProject) (p *projectNone) {
	p = new(projectNone)

	p.NameF = ap.Name
	p.AbstractNameF = ap.Name
	p.LanguageF = "None"
	p.TagsF = ap.Tags
	p.AbstractPrerequisitesF = ap.Prerequisites
	p.SummaryF = ap.Summary

	if len(ap.Checks) != 0 {
		p.GradedF = true
	}

	p.ForbiddenF = ap.Forbidden
	p.AltmainF = ap.Altmain
	p.ChecksF = ap.Checks
	p.ServicesF = ap.Services
	p.FilesF = ap.Files
	p.ServiceFilesF = ap.ServiceFiles

	return
}

func (proj projectNone) Name() string { return proj.NameF }
func (proj projectNone) AbstractName() string { return proj.NameF }
func (proj projectNone) Summary() string { return proj.SummaryF }
func (proj projectNone) AbstractPrerequisites() []string { return proj.AbstractPrerequisitesF }
func (proj projectNone) Forbidden() string { return proj.ForbiddenF }
func (proj projectNone) Altmain() string { return "" }
func (proj projectNone) Checks() Checks { return proj.ChecksF }
func (proj projectNone) Services() []service { return proj.ServicesF }
func (proj projectNone) Files() []string { return proj.FilesF }
func (proj projectNone) Lang() string { return "" }
func (proj projectNone) Build() (err error) { return }
func (proj projectNone) ApplyRuntimeAltmain(cmd []string) (cmd2 []string, err error) { return cmd, err }
