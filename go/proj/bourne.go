package proj

import (
	"errors"
	"os"
)

type projectBourne PrivateProject

func newProjectBourne(ap AbstractProject) (p *projectBourne) {
	p = new(projectBourne)

	p.NameF = ap.Name + "Bourne"
	p.AbstractNameF = ap.Name
	p.LanguageF = "Bourne"
	p.TagsF = ap.Tags
	p.AbstractPrerequisitesF = ap.Prerequisites
	p.SummaryF = ap.Summary

	if len(ap.Checks) != 0 {
		p.GradedF = true
	}

	p.ForbiddenF = ap.Forbidden
	p.AltmainF = ap.Altmain
	p.ChecksF = ap.Checks
	p.ServicesF = ap.Services
	p.FilesF = ap.Files
	p.ServiceFilesF = ap.ServiceFiles

	return
}

func (proj projectBourne) Name() string { return proj.NameF }
func (proj projectBourne) AbstractName() string { return proj.AbstractNameF }
func (proj projectBourne) Summary() string { return proj.SummaryF }
func (proj projectBourne) AbstractPrerequisites() []string { return proj.AbstractPrerequisitesF }
func (proj projectBourne) Forbidden() string { return proj.ForbiddenF }
func (proj projectBourne) Checks() Checks { return proj.ChecksF }
func (proj projectBourne) Services() []service { return proj.ServicesF }
func (proj projectBourne) Files() []string { return proj.FilesF }
func (proj projectBourne) Lang() string { return "Bourne" }
func (proj projectBourne) Build() (err error) { return }

func (proj projectBourne) Altmain() string {
	if _, ok := proj.AltmainF[proj.Lang()]; ok {
		return "main2.sh"
	}

	return ""
}

func (proj projectBourne) ApplyRuntimeAltmain(cmd []string) (cmd2 []string, err error) {
	cmd2 = cmd
	if altmain := proj.Altmain(); altmain != "" && cmd[0] == "./" + proj.Name() {
		cmd[0] = "./" + altmain

		if _, err = os.Stat(proj.Name() + "Bourne.sh"); os.IsNotExist(err) {
			if err = os.Rename(proj.Name(), proj.Name() + "Bourne.sh"); err != nil {
				return cmd2, errors.New("could not rename " +
				                         proj.Name() +
				                       " to functionsBourne")
			}
		}
	}

	return
}
