package proj

import (
	"errors"
	"os"
	"../run"
)

type projectC PrivateProject

func newProjectC(ap AbstractProject) (p *projectC) {
	p = new(projectC)

	p.NameF = ap.Name + "C"
	p.AbstractNameF = ap.Name
	p.LanguageF = "C"
	p.TagsF = ap.Tags
	p.AbstractPrerequisitesF = ap.Prerequisites
	p.SummaryF = ap.Summary

	if len(ap.Checks) != 0 {
		p.GradedF = true
	}

	p.ForbiddenF = ap.Forbidden
	p.AltmainF = ap.Altmain
	p.ChecksF = ap.Checks
	p.ServicesF = ap.Services
	p.FilesF = ap.Files
	p.ServiceFilesF = ap.ServiceFiles

	return
}

func (proj projectC) Name() string { return proj.NameF }
func (proj projectC) AbstractName() string { return proj.AbstractNameF }
func (proj projectC) Summary() string { return proj.SummaryF }
func (proj projectC) AbstractPrerequisites() []string { return proj.AbstractPrerequisitesF }
func (proj projectC) Forbidden() string { return proj.ForbiddenF }
func (proj projectC) Checks() Checks { return proj.ChecksF }
func (proj projectC) Services() []service { return proj.ServicesF }
func (proj projectC) Files() []string { return proj.FilesF }
func (proj projectC) Lang() string { return "C" }

func (proj projectC) Altmain() string {
	if _, ok := proj.AltmainF[proj.Lang()]; ok {
		return "main2" + LangToExt(proj.Lang())
	}

	return ""
}

func (proj projectC) Build() (err error) {
	if _, err = os.Stat("Makefile"); !os.IsNotExist(err) {
		if _, _, err = run.Standard(nil, nil, "make"); err != nil {
			os.Stderr.WriteString(err.Error() + "\n")
			err = errors.New(err.Error())
			return
		}
	} else {
		srcFile := proj.AbstractNameF + LangToExt(proj.Lang())
		exeFile := proj.AbstractNameF
		args := []string{"-o", exeFile}

		if altmain := proj.Altmain(); altmain != "" {
			args = append(args, "-Wl,-emain2", altmain)
		}

		args = append(args, srcFile)

		if _, _, err = run.Standard(nil, nil, "gcc", args...); err != nil {
			os.Stderr.WriteString(err.Error() + "\n")
			err = errors.New(err.Error())
			return
		}
	}

	return
}

func (proj projectC) ApplyRuntimeAltmain(cmd []string) (cmd2 []string, err error) { return cmd, err }
