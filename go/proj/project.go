package proj

import (
	"encoding/json"
	"errors"
	"io"
)

type service struct {
	Source        string `json:"source"`
	CompilerFlags string `json:"compiler_flags"`
	Port          int    `json:"port"`
	PublishBinary bool   `json:"publish_binary"`
}

// Checks contains a series of checks performed on a project submission while grading.
type Checks []Check

// AbstractProject represents an abstract project, that is a project that can
// be solved using one of a set of languages.
type AbstractProject struct {
	Name           string          `json:"name"`
	Summary        string          `json:"summary"`
	Languages      []string        `json:"languages"`
	Tags           map[string]bool `json:"tags"`
	Prerequisites  []string        `json:"prerequisites"`
	Forbidden      string          `json:"forbidden"`
	Altmain        map[string]bool `json:"altmain"`
	Checks         Checks          `json:"checks"`
	Services       []service       `json:"services"`
	Files          []string        `json:"files"`
	ServiceFiles   []string        `json:"service_files"`
}

// A Project defines the student-visible aspects of a project.
// Fields would be better unexported, but must be for use with json.
type Project struct {
	NameF                    string          `json:"name"`
	AbstractNameF            string          `json:"abstract_name"`
	LanguageF                string          `json:"language"`
	TagsF                    map[string]bool `json:"tags"`
	AbstractPrerequisitesF []string          `json:"prerequisites"`
	SummaryF                 string          `json:"summary"`
	GradedF                  bool            `json:"graded"`
}

// A PrivateProject further defines the student-invisible aspects of a project.
// Fields would be better unexported, but must be for use with json.
type PrivateProject struct {
	Project
	ForbiddenF      string          `json:"forbidden"`
	AltmainF        map[string]bool `json:"altmain"`
	ChecksF         Checks          `json:"checks"`
	ServicesF       []service       `json:"services"`
	FilesF          []string        `json:"files"`
	ServiceFilesF   []string        `json:"service_files"`
}

// Instructions contains the title, summary, and instructions for a project.
type Instructions struct {
	Title string        `json:"title"`
	Summary string      `json:"summary"`
	Instructions string `json:"instructions"`
}

// A Grade captures the grade assigned to a project submission.
type Grade struct {
	Project   string `json:"project"`
	Student   string `json:"student"`
	Commit    string `json:"commit"`
	Timestamp string `json:"timestamp"`
	Outcome   string `json:"outcome"`
}

// Check describes a grading check.
type Check struct {
	Kind       string          `json:"kind"`
	Parameters json.RawMessage `json:"parameters"`
}

// Result records the result of executing a grading check.
type Result struct {
	Command   string `json:"command"`
	ErrMsg    string `json:"errMsg"`
	Stdout  []byte   `json:"stdout"`
	Stderr  []byte   `json:"stderr"`
	ExitCode  int    `json:"exitCode"`
}

// Results combine a series of grading check results with the corresponding Git
// commit hash.
type Results struct {
	Results []Result `json:"results"`
	Hash      string `json:"hash"`
}

// DecodeResults reads a series of grading results from reader and stores them
// in the given structure.
func DecodeResults(reader io.Reader) (r Results, err error) {
	dec := json.NewDecoder(reader)
	err = dec.Decode(&r)
	return
}

// InstantiateProject creates a PrivateProject from a AbstractProject and language selector.
func InstantiateProject(language string, abs AbstractProject) (ProjectInterface, error) {
	switch language {
	case "AMD64":
		return newProjectAMD64(abs), nil
	case "Bourne":
		return newProjectBourne(abs), nil
	case "C":
		return newProjectC(abs), nil
	case "Go":
		return newProjectGo(abs), nil
	case "Java":
		return newProjectJava(abs), nil
	case "Python":
		return newProjectPython(abs), nil
	case "":
		return newProjectNone(abs), nil
	}

	return nil, errors.New("invalid language: " + language)
}

// DecodeAndInstantiateProject decodes a project into the appropriate type and
// returns the result.
func DecodeAndInstantiateProject(reader io.Reader, lang string) (ProjectInterface, error) {
	var p AbstractProject
	var err error

	dec := json.NewDecoder(reader)

	if err = dec.Decode(&p); err != nil {
		return nil, err
	}

	return InstantiateProject(lang, p)
}

// ProjectInterface is the interface implmented by all projects,
// regardless of language.
type ProjectInterface interface {
	Name() string
	AbstractName() string
	Summary() string
	AbstractPrerequisites() []string
	Forbidden() string
	Checks() Checks
	Services() []service
	Files() []string
	Lang() string
	Altmain() string
	Build() error
	ApplyRuntimeAltmain(cmd []string) (cmd2 []string, err error)
}

// LangToExt returns the filename extension typically associated with a
// programming language.
func LangToExt(lang string) string {
	switch lang {
	case "AMD64":
		return ".S"
	case "C":
		return ".c"
	case "Go":
		return ".go"
	case "Java":
		return ".java"
	default:
		return ""
	}
}

// DecodeLanguageThenProject decodes a project's language before decoding
// the project into the appropriate type and returning the result.
func DecodeLanguageThenProject(reader io.Reader) (ProjectInterface, error) {
	var l string
	var p AbstractProject
	var err error

	dec := json.NewDecoder(reader)

	if err = dec.Decode(&l); err != nil {
		return nil, err
	}

	if err = dec.Decode(&p); err != nil {
		return nil, err
	}

	return InstantiateProject(l, p)
}
