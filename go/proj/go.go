package proj

import (
	"errors"
	"os"
	"../run"
)

type projectGo PrivateProject

func newProjectGo(ap AbstractProject) (p *projectGo) {
	p = new(projectGo)

	p.NameF = ap.Name + "Go"
	p.AbstractNameF = ap.Name
	p.LanguageF = "Go"
	p.TagsF = ap.Tags
	p.AbstractPrerequisitesF = ap.Prerequisites
	p.SummaryF = ap.Summary

	if len(ap.Checks) != 0 {
		p.GradedF = true
	}

	p.ForbiddenF = ap.Forbidden
	p.AltmainF = ap.Altmain
	p.ChecksF = ap.Checks
	p.ServicesF = ap.Services
	p.FilesF = ap.Files
	p.ServiceFilesF = ap.ServiceFiles

	return
}

func (proj projectGo) Name() string { return proj.NameF }
func (proj projectGo) AbstractName() string { return proj.AbstractNameF }
func (proj projectGo) Summary() string { return proj.SummaryF }
func (proj projectGo) AbstractPrerequisites() []string { return proj.AbstractPrerequisitesF }
func (proj projectGo) Forbidden() string { return proj.ForbiddenF }
func (proj projectGo) Checks() Checks { return proj.ChecksF }
func (proj projectGo) Services() []service { return proj.ServicesF }
func (proj projectGo) Files() []string { return proj.FilesF }
func (proj projectGo) Lang() string { return "Go" }

func (proj projectGo) Altmain() string {
	if _, ok := proj.AltmainF[proj.Lang()]; ok {
		return "main2" + LangToExt(proj.Lang())
	}

	return ""
}

func (proj projectGo) Build() (err error) {
	srcFile := proj.AbstractNameF + LangToExt(proj.Lang())
	args := []string{"build", srcFile}

	if altmain := proj.Altmain(); altmain != "" {
		args = append(args, altmain)
	}

	if _, _, err = run.Standard(nil, []string{"CGO_ENABLED=0"}, "go", args...); err != nil {
		os.Stderr.WriteString(err.Error() + "\n")
		err = errors.New(err.Error())
		return
	}

	return
}

func (proj projectGo) ApplyRuntimeAltmain(cmd []string) (cmd2 []string, err error) { return cmd, err }
