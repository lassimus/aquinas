package proj

import (
	"errors"
	"os"
	"../run"
)

type projectAMD64 PrivateProject

func newProjectAMD64(ap AbstractProject) (p *projectAMD64) {
	p = new(projectAMD64)

	p.NameF = ap.Name + "AMD64"
	p.AbstractNameF = ap.Name
	p.LanguageF = "AMD64"
	p.TagsF = ap.Tags
	p.AbstractPrerequisitesF = ap.Prerequisites
	p.SummaryF = ap.Summary

	if len(ap.Checks) != 0 {
		p.GradedF = true
	}

	p.ForbiddenF = ap.Forbidden
	p.AltmainF = ap.Altmain
	p.ChecksF = ap.Checks
	p.ServicesF = ap.Services
	p.FilesF = ap.Files
	p.ServiceFilesF = ap.ServiceFiles

	return
}

func (proj projectAMD64) Name() string { return proj.NameF }
func (proj projectAMD64) AbstractName() string { return proj.AbstractNameF }
func (proj projectAMD64) Summary() string { return proj.SummaryF }
func (proj projectAMD64) AbstractPrerequisites() []string { return proj.AbstractPrerequisitesF }
func (proj projectAMD64) Forbidden() string { return proj.ForbiddenF }
func (proj projectAMD64) Checks() Checks { return proj.ChecksF }
func (proj projectAMD64) Services() []service { return proj.ServicesF }
func (proj projectAMD64) Files() []string { return proj.FilesF }
func (proj projectAMD64) Lang() string { return "AMD64" }

func (proj projectAMD64) Altmain() string {
	if _, ok := proj.AltmainF[proj.Lang()]; ok {
		return "main2" + LangToExt(proj.Lang())
	}

	return ""
}

func (proj projectAMD64) Build() (err error) {
	srcFile := proj.AbstractNameF + LangToExt(proj.Lang())
	exeFile := proj.AbstractNameF
	args := []string{"-s", "-nostartfiles", "-nostdlib", "-o", exeFile}

	args = append(args, srcFile)

	if _, _, err = run.Standard(nil, nil, "gcc", args...); err != nil {
		os.Stderr.WriteString(err.Error() + "\n")
		err = errors.New(err.Error())
		return
	}

	return
}

func (proj projectAMD64) ApplyRuntimeAltmain(cmd []string) (cmd2 []string, err error) { return cmd, err }
