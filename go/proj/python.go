package proj

import (
	"errors"
	"os"
)

type projectPython PrivateProject

func newProjectPython(ap AbstractProject) (p *projectPython) {
	p = new(projectPython)

	p.NameF = ap.Name + "Python"
	p.AbstractNameF = ap.Name
	p.LanguageF = "Python"
	p.TagsF = ap.Tags
	p.AbstractPrerequisitesF = ap.Prerequisites
	p.SummaryF = ap.Summary

	if len(ap.Checks) != 0 {
		p.GradedF = true
	}

	p.ForbiddenF = ap.Forbidden
	p.AltmainF = ap.Altmain
	p.ChecksF = ap.Checks
	p.ServicesF = ap.Services
	p.FilesF = ap.Files
	p.ServiceFilesF = ap.ServiceFiles

	return
}

func (proj projectPython) Name() string { return proj.NameF }
func (proj projectPython) AbstractName() string { return proj.AbstractNameF }
func (proj projectPython) Summary() string { return proj.SummaryF }
func (proj projectPython) AbstractPrerequisites() []string { return proj.AbstractPrerequisitesF }
func (proj projectPython) Forbidden() string { return proj.ForbiddenF }
func (proj projectPython) Checks() Checks { return proj.ChecksF }
func (proj projectPython) Services() []service { return proj.ServicesF }
func (proj projectPython) Files() []string { return proj.FilesF }
func (proj projectPython) Lang() string { return "Python" }
func (proj projectPython) Build() (err error) { return }

func (proj projectPython) Altmain() string {
	if _, ok := proj.AltmainF[proj.Lang()]; ok {
		return "main2.py"
	}

	return ""
}

func (proj projectPython) ApplyRuntimeAltmain(cmd []string) (cmd2 []string, err error) {
	cmd2 = cmd
	if altmain := proj.Altmain(); altmain != "" && cmd[0] == "./" + proj.AbstractName() {
		cmd[0] = "./" + altmain

		if _, err = os.Stat(proj.Name() + ".py"); os.IsNotExist(err) {
			if err = os.Rename(proj.AbstractName(), proj.Name() + ".py"); err != nil {
				return cmd2, errors.New("could not rename " +
				                         proj.AbstractName() + " to " +
				                         proj.Name() + ".py")
			}
		}
	}

	return
}
