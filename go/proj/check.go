package proj

import (
	"bytes"
	"errors"
	"fmt"
	"encoding/json"
	"os"
	"regexp"
	"../run"
	"strings"
	"time"
)

const maxSubmissionRuntime = 15

func runCheck(p ProjectInterface, c Check) (Result, error) {
	var errMsg string
	var checkExitCode int

	r := regexp.MustCompile(`[^\s"']+|"([^"]*)"|'([^']*)`)
	cmd := r.FindAllString(c.Command(), -1)

	/* Drop quotes. */
	for i, f := range cmd {
		if len(f) > 0 && (f[0] == '"' || f[0] == '\'') {
			cmd[i] = f[1:len(f) - 1]
		}
	}

	cmd, err := p.ApplyRuntimeAltmain(cmd)
	if err != nil {
		os.Stderr.WriteString("system error: buildrun: " + err.Error())
		return Result{}, err
	}

	stdout, stderr, err := run.Timed(maxSubmissionRuntime * time.Second,
	                                 bytes.NewReader(c.stdin()),
	                                 nil,
	                                 cmd[0],
	                                 cmd[1:]...)
	if err != nil {
		if stderrErr, ok := err.(*run.Error); ok {
			checkExitCode = stderrErr.ExitCode
			err = nil
		} else {
			errMsg = err.Error()
		}
	}

	return Result{
		Command:  c.Command(),
		ErrMsg:   errMsg,
		Stdout:   stdout,
		Stderr:   stderr,
		ExitCode: checkExitCode,
	}, nil
}

// Command upacks the command associated with a grade check.
func (c Check) Command() string {
	switch c.Kind {
	case "basic":
		p := new(checkParameters)
		json.Unmarshal(c.Parameters, p)
		return p.Command
	case "background":
		p := new(checkParametersBackground)
		json.Unmarshal(c.Parameters, p)
		return p.Command
	case "regexp":
		p := new(checkParametersRegexp)
		json.Unmarshal(c.Parameters, p)
		return p.Command
	}

	return ""
}

func (c Check) stdin() []byte {
	switch c.Kind {
	case "basic":
		p := new(checkParameters)
		json.Unmarshal(c.Parameters, p)
		return p.Stdin
	case "background":
		p := new(checkParametersBackground)
		json.Unmarshal(c.Parameters, p)
		return p.Stdin
	case "regexp":
		p := new(checkParametersRegexp)
		json.Unmarshal(c.Parameters, p)
		return p.Stdin
	}

	return []byte{}
}

// Run executes a grading check.
func (c Check) Run(p ProjectInterface) (Result, error) {
	switch (c.Kind) {
	case "background":
		return runCheckBackground(p, c)
	default:
		return runCheck(p, c)
	}
}

type checkParameters struct {
	Command   string `json:"command"`
	Stdin   []byte   `json:"stdin"`
	Stdout  []byte   `json:"stdout"`
	Stderr  []byte   `json:"stderr"`
	ExitCode  int    `json:"exitCode"`
	Hint      string `json:"hint"`
}

// Grade checks the result of the execution of a check
func (p checkParameters) Grade(i int, result Result) (err error) {
	if !bytes.Equal(result.Stdout, p.Stdout) {
		s1  := truncate(result.Stdout)
		s2  := truncate(p.Stdout)
		msg := fmt.Sprintf("stdout check %d failed: got %s wanted %s",
				    i, s1, s2)
		err  = errors.New(msg)
		return
	}

	if !bytes.Equal(result.Stderr, p.Stderr) {
		s1  := truncate(result.Stderr)
		s2  := truncate(p.Stdout)
		msg := fmt.Sprintf("stderr check %d failed: got %s wanted %s",
				   i, s1, s2)
		err  = errors.New(msg)
		return
	}

	if result.ExitCode != p.ExitCode {
		e1  := result.ExitCode
		e2  := p.ExitCode
		msg := fmt.Sprintf("exit code check %d failed: got %d wanted %d",
				    i, e1, e2)
		err  = errors.New(msg)
		return
	}

	return
}

func (p checkParameters) hint() string {
	return p.Hint
}

// Grade checks a series of grade check results.
func (c Checks) Grade(results Results) (string, error) {
	if len(results.Results) != len(c) {
		err := fmt.Errorf("results count is %d, not %d",
		                   len(results.Results),
		                   len(c))
		return "system error", err
	}

	for i, check := range c {
		var p interface{
			Grade(int, Result) error
			hint() string
		}

		switch check.Kind {
		case "basic":
			p = new(checkParameters)
		case "background":
			p = new(checkParametersBackground)
		case "regexp":
			p = new(checkParametersRegexp)
		default:
			err := errors.New("unknown check type " + check.Kind)
			return "system error", err
		}

		if err := json.Unmarshal(check.Parameters, p); err != nil {
			return "system error", err
		}

		err := p.Grade(i, results.Results[i])
		if err != nil {
			return p.hint(), err
		}
	}

	return "", nil
}

func truncate(b []byte) string {
	if len(b) > 64 {
		return strings.ReplaceAll(string(b[:64]), "\n", "[nl]")
	}

	return strings.ReplaceAll(string(b), "\n", "[nl]")
}
