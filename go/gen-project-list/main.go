package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path"
	"../proj"
)

const (
	description = "description.json"
	projects    = "aquinas-projects"
)

func main() {
	projs := make(map[string]proj.AbstractProject)

	files, err := ioutil.ReadDir(projects)
	if err != nil {
		log.Fatal(err)
	}

	for _, file := range files {
		if file.Name()[0] == '.' ||
		   file.Name() == "references.bib" ||
		   file.Name() == "Makefile" {
			continue
		}

		f, err := os.Open(path.Join(projects, file.Name(), description))
		if err != nil {
			log.Fatal(err)
		}

		dec := json.NewDecoder(f)

		project := proj.AbstractProject{}
		if err := dec.Decode(&project); err != nil {
			log.Fatal(err)
		}

		projs[project.Name] = project
	}

	fmt.Println("package main")
	fmt.Println("")
	fmt.Println("/* Shared with client; cannot contain confidential data. */")
	fmt.Println("")
	fmt.Println(`import "../proj"`)
	fmt.Println("")
	fmt.Println("var projectList = map[string]proj.Project{")
	for k, v := range projs {
		for _, lang := range v.Languages {
			var name2, link string
			if lang == "none" {
				name2 = k
				link = k
			} else {
				name2 = k + " in " + lang
				link = k + lang
			}
			fmt.Println("	\"" + link + "\": proj.Project{")
			fmt.Printf("		NameF: \"%s\",\n", name2)
			fmt.Printf("		AbstractNameF: \"%s\",\n", k)
			fmt.Printf("		LanguageF: \"%s\",\n", lang)
			fmt.Printf("		TagsF: map[string]bool{\n")
			for k, v := range v.Tags {
				fmt.Printf("			\"%s\": %t,\n", k, v)
			}
			fmt.Printf("		},\n")
			fmt.Printf("		AbstractPrerequisitesF: []string{\n")
			for _, prereq := range v.Prerequisites {
				fmt.Printf("			\"%s\",\n", prereq)
			}
			fmt.Printf("		},\n")
			fmt.Printf("		SummaryF: \"%s\",\n", v.Summary)
			fmt.Printf("		GradedF: %t,\n", len(v.Checks) != 0)
			fmt.Println("	},")
		}
	}
	fmt.Println("}")
}
