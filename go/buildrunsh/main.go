package main

/*
 * This program builds and runs a project solution. This program is
 * meant to be installed as a shell on the user VM, as it reads its
 * input from stdin. The Aquinas Git server uses SSH to establish an
 * authenticated and encrypted connection to provide such input and trigger
 * a build and run cycle.
 *
 * NOTE: This is to run in the context of the user who submitted the solution.
 * Therefore, this cannot be trusted to grade the submission; that must be
 * left to the program on the other side of the SSH connection because it
 * runs in another context. This program merely provides the submission's output
 * to the other program by writing it to stdout.
 */

import (
	"bufio"
	"../common"
	"encoding/json"
	"fmt"
	"../limits"
	"log/syslog"
	"os"
	"os/user"
	"path"
	"../proj"
	"regexp"
)

const (
	maxFiles = 32
	maxThreads = 32
	maxMsgQueue = 16
)

var logger  *syslog.Writer

func main() {
	var err error
	var exitCode int

	logger, err = syslog.New(syslog.LOG_NOTICE, os.Args[0])
	if err != nil {
		fmt.Fprintf(os.Stderr, "could not initialize logging: %s\n", err)
		os.Exit(1)
	}

	logger.Debug("building and running")

	l := limits.Limits{
		MaxFiles: maxFiles,
		MaxThreads: maxThreads,
		MaxMsgQueue: maxMsgQueue,
	}
	if err := limits.Set(l); err != nil {
		common.Fail(logger.Err, fmt.Errorf("could not set limits: %s", err))
	}

	p, err := proj.DecodeLanguageThenProject(os.Stdin)
	if err != nil {
		common.Fail(logger.Err, fmt.Errorf("could not decode project: %s", err))
	}

	logger.Debug("project is " + p.Name())

	student, err := user.Current()
	if err != nil {
		common.Fail(logger.Err, fmt.Errorf("could not determine current user: %s", err))
	}

	logger.Debug("student is " + student.Username)

	results, err := buildCheck(student, p)
	if err != nil {
		exitCode = 1
		fmt.Fprintf(os.Stderr, "%s\n", err)
		logger.Err(err.Error())
	}

	/* NOTE: Grader fills in results.Hash, so it is blank here. */

	b, err := json.Marshal(results)
	if err != nil {
		common.Fail(logger.Err, fmt.Errorf("could not encode results: %s", err))
	}

	os.Stdout.Write(b)

	if err := cleanUp(student, p); err != nil {
		common.Fail(logger.Err, fmt.Errorf("could not clean up: %s", err))
	}

	logger.Debug("completed build/run of " + p.Name() + " from " + student.Username)

	os.Exit(exitCode)
}

/* Ensure solution avoids keywords forbidden by project. */
func checkForbidden(p proj.ProjectInterface) error {
	srcFile := p.AbstractName() + proj.LangToExt(p.Lang())

	f, err := os.Open(srcFile)
	if err != nil {
		return fmt.Errorf("could not open %s: %s", srcFile, err)
	}

	blacklist, err := regexp.Compile(p.Forbidden())
	if err != nil {
		return fmt.Errorf("could not compile %s: %s", p.Forbidden(), err)
	}

	loc := blacklist.FindReaderIndex(bufio.NewReader(f))
	if loc != nil {
		buf := make([]byte, loc[1] - 1)

		f.ReadAt(buf, int64(loc[0]))

		return fmt.Errorf("found forbidden keyword %s in %s", string(buf), srcFile)
	}

	return nil
}

func runChecks(p proj.ProjectInterface) (results proj.Results, err error) {
	for _, check := range p.Checks() {
		var result proj.Result

		logger.Debug("running " + check.Command())

		result, err = check.Run(p)
		if err != nil {
			return results, err
		}

		results.Results = append(results.Results, result)
	}

	return results, err
}

func buildCheck(student *user.User, p proj.ProjectInterface) (results proj.Results, err error) {
	dir := path.Join(student.HomeDir, p.Name())
	if err = os.Chdir(dir); err != nil {
		return results, fmt.Errorf("could not enter %s: %s", dir, err)
	}

	if p.Forbidden() != "" {
		if err = checkForbidden(p); err != nil {
			return
		}
	}

	if err = p.Build(); err != nil {
		return results, fmt.Errorf("failed building %s: %s", p.Name(), err)
	}

	return runChecks(p)
}

func cleanUp(student *user.User, p proj.ProjectInterface) (err error) {
	dir := path.Join(student.HomeDir, p.Name())
	if _, err = os.Stat(dir); os.IsNotExist(err) {
		return nil
	}

	if err = os.RemoveAll(dir); err != nil {
		return fmt.Errorf("could not remove %s: %s", dir, err)
	}

	return
}
