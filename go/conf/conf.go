package conf

import (
	"encoding/json"
	"os"
	"regexp"
)

type configuration struct {
	Domain string          `json:"domain"`
	Root string            `json:"root"`
	EmailRegex string      `json:"emailRegex"`
	EmailRelay string      `json:"emailRelay"`
	EmailSender string     `json:"emailSender"`
	EmailSenderName string `json:"emailSenderName"`
}

// SockPath is the path to the socket used by queued.
const SockPath = "/tmp/queue-socket"

var conf *configuration

func init() {
	if conf == nil {
		f, err := os.Open("aquinas.json")
		if err != nil {
			f, err = os.Open("../../aquinas.json")
		}
		if err != nil {
			if f, err = os.Open("/etc/aquinas/aquinas.json"); err != nil {
				panic(err)
			}
		}

		defer f.Close()

		conf   = new(configuration)
		dec   := json.NewDecoder(f)
		if err = dec.Decode(conf); err != nil {
			panic(err)
		}
	}

	/* Ensure regex is well-formed. */
	regexp.MustCompile(conf.EmailRegex)
}

// Domain returns the configured domain.
func Domain() string {
	return conf.Domain
}

// Root returns the configured filesystem root.
func Root() string {
	return conf.Root
}

// EmailRegex returns the regular expression that specifies permitted email
// domains.
func EmailRegex() string {
	return conf.EmailRegex
}

// EmailRelay returns the host that serves as Aquinas' email relay.
func EmailRelay() string {
	return conf.EmailRelay
}

// EmailSender returns the email address that serves as the from address for
// Aquinas emails.
func EmailSender() string {
	return conf.EmailSender
}

// EmailSenderName returns the name that serves as the from name for
// Aquinas emails.
func EmailSenderName() string {
	return conf.EmailSenderName
}
