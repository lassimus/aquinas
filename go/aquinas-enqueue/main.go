package main

import (
	"fmt"
	"../queue"
	"os"
)

func main() {
	exitcode := 0

	stdout, stderr, err := queue.Enqueue(os.Args[1:]...)
	if err != nil {
		fmt.Fprintf(os.Stderr, "error enqueueing job: " + err.Error())
		exitcode = 1
	}

	fmt.Fprintf(os.Stdout, "%s", stdout)
	fmt.Fprintf(os.Stderr, "%s", stderr)

	os.Exit(exitcode)
}
