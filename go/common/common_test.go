package common

import (
	"../conf"
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"../run"
	"strings"
	"testing"
)

func TestHash(t *testing.T) {
	/* NOTE: Go hash must match sh hash (i.e., sha256sum). */

	s := "Hello, world!"

	stdin := strings.NewReader(s)
	output, _, err := run.Standard(stdin, nil, "sha256sum")
	if err != nil {
		t.Error(err.Error())
	}

	h1 := strings.Split(string(output), " ")[0]
	h2 := HashCalc(s)

	if !HashSame(h1, h2) {
		t.Error(strings.ToUpper(h1) + " != " + strings.ToUpper(h2))
	}
}

func TestHashBad(t *testing.T) {
	s1 := "Hello, world!"
	s2 := "Goodbye, world!"

	stdin := strings.NewReader(s1)
	output, _, err := run.Standard(stdin, nil, "sha256sum")
	if err != nil {
		t.Error(err.Error())
	}

	h1 := strings.Split(string(output), " ")[0]
	h2 := HashCalc(s2)

	if HashSame(h1, h2) {
		t.Error(strings.ToUpper(h1) + " == " + strings.ToUpper(h2))
	}
}

func TestNormalizeUsername(t *testing.T) {
	if NormalizeUsername(conf.EmailSender()) != "mike_at_flyn_dot_org" {
		t.Error("failed to normalize " + conf.EmailSender())
	}
}

func TestNormalizeUsernameNotLong(t *testing.T) {
	email     := "01234567890123456789012345678901"
	if NormalizeUsername(email) != email {
		t.Error("failed to normalize " + email)
	}
}

func TestNormalizeUsernameLong(t *testing.T) {
	email     := "012345678901234567890123456789012"
	sum       := md5.Sum([]byte(email))
	hashEmail := hex.EncodeToString(sum[:])
	hashEmail  = "u" + hashEmail[1:]
	if NormalizeUsername(email) != hashEmail {
		t.Error("failed to normalize " + email)
	}
}

func TestNormalizeUsernameCompatible(t *testing.T) {
	cmdline := ". ../../aquinas-functions; calcnorm " + conf.EmailSender()
	username, _, err := run.Standard(nil, nil, "bash", "-c", cmdline)
	if err != nil {
		t.Error("failed to run calcnorm " + conf.EmailSender() + ": " + err.Error())
	}

	if NormalizeUsername(conf.EmailSender()) != string(username)[:len(username) - 1] {
		t.Error("normalize: " + NormalizeUsername(conf.EmailSender()) +
                        " != " + string(username))
	}
}

func TestAllowedEmailDomain(t *testing.T) {
	if !AllowedEmailDomain(conf.EmailSender()) {
		t.Error("did not permit " + conf.EmailSender())
	}
}

func TestAllowedEmailDomainDeny(t *testing.T) {
	email := "user@example.invalid" /* RFC 2606. */
	if AllowedEmailDomain(email) {
		t.Error("permitted " + email)
	}
}

func TestNonce(t *testing.T) {
	n1 := Nonce()
	if len(n1) != 16 {
		t.Error(fmt.Sprintf("len(nonce) is %d", len(n1)))
	}

	n2 := Nonce()
	if n1 == n2 {
		t.Error(fmt.Sprintf("two nonces match: %s %s", n1, n2))
	}
}

func TestPassword(t *testing.T) {
	p1, err := Password()
	if err != nil {
		t.Error(err.Error())
	}
	if len(p1) != passwordLen {
		t.Error(fmt.Sprintf("len(password) is %d", len(p1)))
	}

	p2, err := Password()
	if err != nil {
		t.Error(err.Error())
	}
	if p1 == p2 {
		t.Error(fmt.Sprintf("two passwords match: %s %s", p1, p2))
	}
}

func TestGitHost(t *testing.T) {
	if s := GitHost("aquinas-www." + conf.Domain()); s != "aquinas-git." + conf.Domain() {
		t.Error("GitHost(\"aquinas-www...\") != \"aquinas-git...\"")
	}

	if s := GitHost("aquinas-user." + conf.Domain()); s != "aquinas-user." + conf.Domain() {
		t.Error("GitHost(\"aquinas-user...\") != \"aquinas-user...\"")
	}
}

func TestHasNil(t *testing.T) {
	value1 := "1"
	value2 := "2"
	valueE := ""

	v := new(struct{
		foo *string
		bar *string
	})
	v.foo = &value1
	v.bar = &value2
	if HasNil(v) {
		t.Error("HasNil: false positive on struct")
	}

	v = new(struct{
		foo *string
		bar *string
	})
	v.foo = &value1
	v.bar = nil
	if !HasNil(v) {
		t.Error("HasNil: false negative on struct")
	}

	/* "" does not count as nil. */
	v = new(struct{
		foo *string
		bar *string
	})
	v.foo = &value1
	v.bar = &valueE
	if HasNil(v) {
		t.Error("HasNil: false positive on struct (\"\")")
	}
}

func TestUpdateRepo(t *testing.T) {
	project := "aquinas-projects"

	dir, err := ioutil.TempDir("", "aquinas")
	if err != nil {
		t.Error("TestUpdateRepo: error creating temp. dir")
	}

	err = UpdateRepo("https://www.flyn.org/git/" + project, "master", dir, project)
	if err != nil {
		t.Error("TestUpdateRepo: error updating")
	}

	p := path.Join(dir, project, "references.bib")
	if _, err := os.Stat(p); err != nil {
		t.Error("TestUpdateRepo: stat of " + p + " failed")
	}

	err = os.RemoveAll(dir)
	if err != nil {
		t.Error("TestUpdateRepo: remove " + p + " failed")
	}
}

func TestUpdateRepoBad(t *testing.T) {
	project := "aquinas-projects"

	dir, err := ioutil.TempDir("", "aquinas")
	if err != nil {
		t.Error("TestUpdateRepoBad: error creating temp. dir")
	}

	err = UpdateRepo("https://example.com/git/" + project, "master", dir, project)
	if err == nil {
		t.Error("TestUpdateRepoBad: update unexpectedly succeeded")
	}

	if runErr, ok := err.(*run.Error); ok {
		if !strings.Contains(runErr.Error(), "not found") {
			t.Error("TestUpdateRepoBad: unexpected error: " + runErr.Error())
		}
	} else {
		t.Error("TestUpdateRepoBad: received wrong error")
	}
}
