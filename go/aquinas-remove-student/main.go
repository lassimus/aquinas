package main

import (
	"../db"
	"fmt"
	"os"
)

func main() {
	var student string

	if len(os.Args) != 2 {
		fmt.Fprintf(os.Stderr, "usage: %s USER\n", os.Args[0])
		os.Exit(1)
	}

	student = os.Args[1]

	db := make(db.Filesystem)

	jobUUID, err := db.RemoveStudent(student)
	if err != nil {
		fmt.Fprintf(os.Stderr, "could not remove student: %s\n", err)
		os.Exit(1)
	}

	fmt.Println(jobUUID)
}
