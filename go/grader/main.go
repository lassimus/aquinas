package main

/*
 * This program implements much of the hook executed when a student checks in a
 * project submission. The calling script must run this program as teacher using
 * sudo.
 *
 * (1) Use git to clone or pull the latest project descriptions.
 * (2) ssh to user@aq-test to invoke buildrunsh, provide project description,
 *     and receive results. Grade results and submit a record for use on WWW.
 *     Because this runs user-submitted code, it must run in that user's
 *     context.
 */

import (
	"bytes"
	"../common"
	"../conf"
	"fmt"
	"io"
	"log"
	"log/syslog"
	"os"
	"os/user"
	"path"
	"../proj"
	"regexp"
	"../run"
	"strconv"
	"strings"
)

const workDir    = "workdir"
const students   = "students"
const projects   = "projects"
const records    = "records"
const userHost   = "aquinas-user"
const remoteHome = "/home"

var logger *syslog.Writer

func main() {
	var err error
	var pass bool
	var ran bool
	var submissionRepo string
	var p proj.ProjectInterface
	var realUser, effectiveUser *user.User

	logger, err = syslog.New(syslog.LOG_NOTICE, os.Args[0])
	if err != nil {
		fmt.Fprintf(os.Stderr, "could not initialize logging: %s\n", err)
		os.Exit(1)
	}

	logger.Debug("grading")

	if len(os.Args) != 3 && len(os.Args) != 4 {
		common.Fail(logger.Err, fmt.Errorf("usage: %s STUDENT PROJNAME [REPO]", os.Args[0]))
	}

	student  := os.Args[1]
	projName := os.Args[2]

	logger.Debug("gradng " + projName + " for " + student)

	studentUser, err := user.Lookup(common.NormalizeUsername(student))
	if err != nil {
		common.Fail(logger.Err, fmt.Errorf("could not lookup %s: %s", student, err))
	}

	if len(os.Args) == 4 {
		submissionRepo = os.Args[3]
	} else {
		submissionRepo = path.Join(studentUser.HomeDir, projName)
	}

	if realUser, effectiveUser, err = getUser(); err != nil {
		common.Fail(logger.Err, fmt.Errorf("could not get running user: %s", err))
	}

	logger.Debug(fmt.Sprintf("executing user: %s; effective user: %s",
	                          realUser.Username,
	                          effectiveUser.Username))

	projBase, projLang := splitBaseLang(projName)

	recordDir  := path.Join(effectiveUser.HomeDir, workDir, records, projBase)
	if _, err = os.Stat(recordDir); os.IsNotExist(err) {
		if err = os.Mkdir(recordDir, 0700); err != nil {
			common.Fail(logger.Err, fmt.Errorf("could not create %s: %s", recordDir, err))
		}
	}

	logger.Debug(recordDir + " (now) exists")

	lastPath := path.Join(recordDir, student + projLang + ".last")
	lastFile, err := os.OpenFile(lastPath, os.O_CREATE | os.O_TRUNC | os.O_WRONLY, 0640)
	if err != nil {
		common.Fail(logger.Err, fmt.Errorf("could not open %s: %s", lastPath, err))
	}
	defer lastFile.Close()

	logger.Debug(lastPath + " (now) exists")

	repoProjects := path.Join(effectiveUser.HomeDir, projects)
	workdir := path.Join(effectiveUser.HomeDir, workDir)
	if err := common.UpdateRepo(repoProjects, "master", workdir, projects); err != nil {
                lastFile.WriteString("system error: grader: could not update projects repository\n")
		common.Fail(logger.Err, fmt.Errorf("could not update %s: %s", repoProjects, err))
	}

	logger.Debug("updated repository " + repoProjects + " at " + workdir)

	descPath := path.Join(workdir, projects, projBase, "description.json")
	projJSON, err := os.Open(descPath)
	if err != nil {
                lastFile.WriteString("system error: grader: could not open project description\n")
		common.Fail(logger.Err, fmt.Errorf("could not open %s: %s", descPath, err))
	}
	defer projJSON.Close()

	logger.Debug("opened " + descPath)

	p, err = proj.DecodeAndInstantiateProject(projJSON, projLang)
	if err != nil {
		common.Fail(logger.Err, fmt.Errorf("could not decode project: %s", err))
	}

	logger.Debug("decoded " + descPath)

	if _, err = projJSON.Seek(0, 0); err != nil {
		common.Fail(logger.Err, fmt.Errorf("could not seek: %s", err))
	}

	if err := deploySubmission(student, submissionRepo, p, effectiveUser);
	          err != nil {
                lastFile.WriteString("system error: grader: could not deploy submission\n")
		common.Fail(logger.Err, fmt.Errorf("could not deploy %s (%s): %s", projName, submissionRepo, err))
	}

	results, err := buildRun(student, p, projJSON)
	if err != nil {
		lastFile.WriteString(err.Error())
		logger.Err(fmt.Sprintf("build/run failed: %s", err))
		ran = false
	} else {
		ran = true
		for _, r := range results.Results {
			if r.ErrMsg != "" {
				lastFile.WriteString(r.ErrMsg)
				ran = false
			}
		}
	}

	repoRecords := path.Join(effectiveUser.HomeDir, records)
	if err := common.UpdateRepo(repoRecords, "master", workdir, records); err != nil {
		common.Fail(logger.Err, fmt.Errorf("could update %s: %s", repoRecords, err))
	}

	logger.Debug("updated repository " + repoRecords + " at " + workdir)

	if ran {
		hint, err := p.Checks().Grade(results)
		if err == nil {
			lastFile.WriteString("No error\n")
			pass = true
		} else {
			if hint != "" {
				lastFile.WriteString(hint + "\n")
			} else {
				lastFile.WriteString("No error\n")
			}
			logger.Debug(err.Error())
		}
	}

	msg, err := record(p, student, workdir, results, pass)
	if err != nil {
                lastFile.WriteString("system error: grader: could not record results\n")
		common.Fail(logger.Err, fmt.Errorf("could not record results: %s", err))
	}

	if _, _, err = run.Standard(nil, nil, "aquinas-update-www", student, p.AbstractName(), p.Lang()); err != nil {
                lastFile.WriteString("system error: grader: could not update website\n")
		common.Fail(logger.Err, fmt.Errorf("could not update website: %s", err))
	}

	logger.Debug(msg)
}

func getUser() (realUser, effectiveUser *user.User, err error) {
	if realUser, err = user.Current(); err != nil {
		return
	}

	effectiveUser, err = user.LookupId(strconv.Itoa(os.Geteuid()))

	return
}

func splitBaseLang(projName string) (projBase, projLang string) {
	amd64     := "AMD64"
	amd64Exp  := regexp.MustCompile(`.*` + amd64)
	bourne    := "Bourne"
	bourneExp := regexp.MustCompile(`.*` + bourne)
	c         := "C"
	cExp      := regexp.MustCompile(`.*` + c)
	golang    := "Go"
	goExp     := regexp.MustCompile(`.*` + golang)
	java      := "Java"
	javaExp   := regexp.MustCompile(`.*` + java)
	python    := "Python"
	pythonExp := regexp.MustCompile(`.*` + python)

	switch {
	case amd64Exp.MatchString(projName):
		projLang = amd64
		projBase = strings.TrimSuffix(projName, projLang)
	case bourneExp.MatchString(projName):
		projLang = bourne
		projBase = strings.TrimSuffix(projName, projLang)
	case cExp.MatchString(projName):
		projLang = c
		projBase = strings.TrimSuffix(projName, projLang)
	case goExp.MatchString(projName):
		projLang = golang
		projBase = strings.TrimSuffix(projName, projLang)
	case javaExp.MatchString(projName):
		projLang = java
		projBase = strings.TrimSuffix(projName, projLang)
	case pythonExp.MatchString(projName):
		projLang = python
		projBase = strings.TrimSuffix(projName, projLang)
	default:
		projLang = ""
		projBase = projName
	}

	return
}

func record(p proj.ProjectInterface, student, workdir string, results proj.Results, pass bool) (msg string, err error) {
	recordDir  := path.Join(workdir, records, p.AbstractName())
	recordPath := path.Join(recordDir, student + p.Lang() + ".record")
	recordFile, err := os.OpenFile(recordPath,
				       os.O_APPEND | os.O_CREATE | os.O_WRONLY,
				       0640)
	if err != nil {
		return
	}

	defer recordFile.Close()

	projGit := path.Join(workdir, "students", student, p.Name(), ".git")
	hash, _, err := run.Standard(nil, nil, "git", "--git-dir", projGit,
	                            "log", "-1", "--pretty=%H")
	if err != nil {
		return
	}
	results.Hash = string(hash)

	record := log.New(recordFile, "", log.LstdFlags)
	if err != nil {
		return
	}

	msg = student + " " + p.Name() + " " +
	       strings.TrimSuffix(results.Hash, "\n") + " "
	if pass {
		msg += "PASS"
	} else {
		msg += "FAIL"
	}

	record.Println(msg)

	if _, _, err = run.Standard(nil, nil, "git", "-C", recordDir, "add", recordPath); err != nil {
		return
	}

	gitMsg := "Append record: " + msg
	if _, _, err = run.Standard(nil, nil, "git", "-C", recordDir, "commit", "-m", gitMsg); err != nil {
		return
	}

	if _, _, err = run.Standard(nil, nil, "git", "-C", recordDir, "push"); err != nil {
		return
	}

	return
}

func deploySubmission(student, repo string, p proj.ProjectInterface, effectiveUser *user.User) (err error) {
	workdir := path.Join(effectiveUser.HomeDir, workDir)
	if _, err = os.Stat(workdir); os.IsNotExist(err) {
		if err = os.Mkdir(workdir, 0700); err != nil {
			return
		}
	}

	logger.Debug(workdir + " (now) exists")

	workdir = path.Join(workdir, students)
	if _, err = os.Stat(workdir); os.IsNotExist(err) {
		if err = os.Mkdir(workdir, 0700); err != nil {
			return
		}
	}

	logger.Debug(workdir + " (now) exists")

	workdir = path.Join(workdir, student)
	if _, err = os.Stat(workdir); os.IsNotExist(err) {
		if err = os.Mkdir(workdir, 0700); err != nil {
			return
		}
	}

	logger.Debug(workdir + " (now) exists")

	/* Remove existing repo to avoid problems related to force pushes. */
	if err = os.RemoveAll(path.Join(workdir, p.Name())); err != nil {
		return
	}

	if err = common.UpdateRepo(repo, "master", workdir, p.Name()); err != nil {
		return
	}

	logger.Debug("updated repository " + repo + " at " + workdir)

	srcPath := path.Join(workdir, p.Name())
	remoteHome := path.Join(remoteHome, student)
	if _, _, err = run.Standard(nil, nil, "scp", "-r", srcPath,
	                      "root@" + userHost + "." + conf.Domain() + ":" +
	                       remoteHome); err != nil {
		return
	}

	logger.Debug("copied " + srcPath + " to " + remoteHome)

	if altmain := p.Altmain(); altmain != "" {
		driverPath := path.Join(effectiveUser.HomeDir, workDir,
		                       "projects", p.AbstractName(), altmain)

		if _, err = os.Stat(driverPath); os.IsNotExist(err) {
			return
		}

		targetPath := path.Join(remoteHome, p.Name())

		if _, _, err = run.Standard(nil, nil, "scp", "-r", driverPath,
				      "root@" + userHost + "." + conf.Domain() + ":" +
				       targetPath); err != nil {
			return
		}
	}

	for _, f := range p.Files() {
		filePath := path.Join(effectiveUser.HomeDir, workDir,
		                     "projects", p.AbstractName(), f)

		if _, err = os.Stat(filePath); os.IsNotExist(err) {
			return
		}

		targetPath := path.Join(remoteHome, p.Name())

		if _, _, err = run.Standard(nil, nil, "scp", "-r", filePath,
				      "root@" + userHost + "." + conf.Domain() + ":" +
				       targetPath); err != nil {
			return
		}
	}

	if _, _, err = run.Remote(nil, nil, "root@" + userHost + "." + conf.Domain(),
	                         "chown", "-R",
	                          common.NormalizeUsername(student) + ":" +
	                          common.NormalizeUsername(student),
	                          remoteHome); err != nil {
		return
	}

	return
}

func buildRun(student string, p proj.ProjectInterface, projJSON io.Reader) (results proj.Results, err error) {
	langJSON := "\"" + p.Lang() + "\""
	buildRunShInput := io.MultiReader(strings.NewReader(langJSON), projJSON)

	userHost := common.NormalizeUsername(student) + "@aquinas-user." + conf.Domain()
	stdout, _, err := run.SSH(buildRunShInput, nil, userHost)
	if err != nil {
		return results, fmt.Errorf("could not SSH run buildrunsh: %s", err)
	}

	logger.Debug("built and ran")

	results, err = proj.DecodeResults(bytes.NewReader(stdout))
	if err != nil {
		return results, fmt.Errorf("could not decode results: %s", err)
	}

	logger.Debug("decoded results")

	return
}
