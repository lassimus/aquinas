package kahn

import (
	"fmt"
)

// A Node maps a project name to the set of projects it depends on.
type Node map[string]map[string]bool

// Kahn implements Kahn's algorithm (1962).
func Kahn(nodes Node) (results []string, err error) {
	/* Set of nodes with no incoming edge. */
	var s []string

	/* Gather start nodes with no incoming edges. */
	for k, v := range nodes {
		if len(v) == 0 {
			s = append(s, k)
		}
	}

	for len(s) != 0 {
		/* Add a node from s to results, record as k, and remove from s. */
		results = append(results, s[0])
		k := s[0]
		s = s[1:]

		for k2, v2 := range(nodes) {
			if _, ok := v2[k]; ok {
				delete(v2, k)
				if len(v2) == 0 {
					s = append(s, k2)
				}
			}
		}
	}

	for k, v := range(nodes) {
		if len(v) != 0 {
			err = fmt.Errorf("graph has cycle: %s's prereq not empty (%v)", k, v)
		}
	}

	return
}
