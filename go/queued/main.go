package main

/* This program provides a job queue which allows users to submit jobs to be
 * run as other users. Queue (1) applies application controls as to who
 * can run what as whom and (2) serializes job requests.
 *
 * Clients request jobs by communicating with the queue daemon over a Unix-
 * domain socket. The queue daemon identifies the remote user by reading
 * the UID and GID associates with the socket connection.
 *
 * Queue immediately run jobs whose command starts with "-s" (synchronous).
 */

import (
	"../conf"
	"encoding/json"
	"fmt"
	"log/syslog"
	"net"
	"net/http"
	"os"
	"os/user"
	"../proj"
	"../run"
	"strings"
	"syscall"
	"time"
	"github.com/google/uuid"
)

type jobInterface interface {
	setAs(as string)
	queue() (stdout, stderr []byte, err error)
	run() (stdout, stderr []byte, err error)
}

type perm struct {
	as string
	by string
}

var permissions = map[string][]perm{
	/* Grader can be run as teacher by anyone. */
	"/usr/sbin/grader": []perm{{as: "teacher", by: ""}},

	/* Initialize projects can be run as root by teacher. */
	"/usr/sbin/aquinas-initialize-projects": []perm{{as: "root", by: "teacher"}},

	/* These can be run as root by http. */
	"/usr/sbin/aquinas-add-student-slave":       []perm{{as: "root", by: "http"}},
	"/usr/sbin/aquinas-get-ssh-authorized-keys": []perm{{as: "root", by: "http"}},
	"/usr/sbin/aquinas-deploy-key":              []perm{{as: "root", by: "http"}},
	"/usr/sbin/aquinas-remove-student-slave":    []perm{{as: "root", by: "http"}},
}

func main() {
	/*
	 * Produce. Forever:
	 * 1. Accept a UNIX socket connection.
	 * 2. Read job from socket.
	 * 3. Authenticate and check permissions.
	 * 4. Create a job.
	 * 5. Either immediately execute or write job to channel for
	 *    asynchronous execution by consumer.
	 */
	go produce()

	/*
	 * Consume. Forever:
	 * 1. Pull a job from channel.
	 * 2. Run job.
	 * 3. Notify httpd when job complete if requested.
	 */
	go consume()

	<-done
}

// An asynchronous job. The producer ships these to the consumer for execution.
type job struct {
	args []string    /* Command to run. */
	as     string    /* User to run as (already authorized) */
	id     uuid.UUID /* UUID of running job. */
	notify bool      /* Notify httpd of result? */
}

func (j *job) setAs(as string) { j.as = as }

func (j *job) queue() (stdout, stderr []byte, err error) {
	id := uuid.New()

	stdout = []byte("")
	stderr = []byte(id.String() + "\n")

	jobs <- &job{args: j.args, as: j.as, id: id, notify: j.notify}

	return
}

func (j *job) run() (stdout, stderr []byte, err error) {
	args := append([]string{"-u", j.as}, j.args...)

	stdout, stderr, err = run.Standard(nil, nil, "sudo", args...)
	if err2, ok := err.(*run.Error); ok && err2.ExitCode != 0 {
		err = fmt.Errorf("%s", stderr)
	}

	return
}

// An synchronous job. The producer directly executes these.
type jobSync struct {
	args []string    /* Command to run. */
	as     string    /* User to run as (already authorized) */
}

func (j *jobSync) setAs(as string) { j.as = as }

func (j *jobSync) queue() (stdout, stderr []byte, err error) {
	args := append([]string{"-u", j.as}, j.args...)

	stdout, stderr, err = run.Standard(nil, nil, "sudo", args...)
	if err2, ok := err.(*run.Error); ok && err2.ExitCode != 0 {
		err = fmt.Errorf("%s", stderr)
	}

	return
}

func (j *jobSync) run() (stdout, stderr []byte, err error) {
	return j.queue()
}

var done = make(chan bool)
var jobs = make(chan *job, 1024)

func getCreds(c*net.UnixConn) (ucred *syscall.Ucred, err error) {
	f, err := c.File()
	if err != nil {
		return nil, err
	}
	defer f.Close()

	return syscall.GetsockoptUcred(int(f.Fd()),
	                               syscall.SOL_SOCKET,
	                               syscall.SO_PEERCRED)
}

func checkPerm(program, by string) (as string, ok bool) {
	perms, ok := permissions[program]
	if !ok {
		return "", false
	}

	for _, p := range(perms) {
		if p.by == "" || p.by == by {
			return p.as, true
		}
	}

	return "", false
}

func produce() {
	logger, err := syslog.New(syslog.LOG_NOTICE, os.Args[0])
	if err != nil {
		/*
		 * System might be booting, and syslog might not have started.
		 * Try again after a few seconds.
		 */
		time.Sleep(10 * time.Second)

		logger, err = syslog.New(syslog.LOG_NOTICE, os.Args[0])
		if err != nil {
			panic(err)
		}
	}

	os.Remove(conf.SockPath)

	l, err := net.ListenUnix("unix", &net.UnixAddr{conf.SockPath, "unix"})
	if err != nil {
		panic(err)
	}

	err = os.Chmod(conf.SockPath, 0666)
	if err != nil {
		panic(err)
	}

	defer l.Close()
	defer os.Remove(conf.SockPath)

	for {
		var buf [1024 * 10]byte
		var args    []string
		var j         jobInterface
		var stdout  []byte
		var stderr  []byte

		c, err := l.AcceptUnix()
		if err != nil {
			logger.Err("failed to accept Unix socket connection")
			continue
		}

		n, err := c.Read(buf[:])
		if err != nil {
			logger.Err("failed to read from Unix socket connection")
			continue
		}

		ucred, err := getCreds(c)
		if err != nil {
			logger.Err("failed to identify remote user")
			continue
		}

		u, err := user.LookupId(fmt.Sprintf("%d", ucred.Uid))
		if err != nil {
			logger.Err(fmt.Sprintf("request from invalid user: %d",
			                         ucred.Uid))
			continue
		}

		str := string(buf[:n - 1]) /* Drop "\n". */
		args = strings.Split(str, "\x00")

		if args[0] == "-s" {
			args = args[1:]
			j = &jobSync{args: args}
		} else if args[0] == "-n" {
			args = args[1:]
			j = &job{args: args, notify: true}
		} else {
			j = &job{args: args}
		}

		as, ok := checkPerm(args[0], u.Username)
		if (ok) {
			msg := fmt.Sprintf("%s run %s as %s: ok\n", u.Username, args[0], as)
			logger.Notice(msg)

			j.setAs(as)

			stdout, stderr, err = j.queue()
			if err != nil {
				logger.Notice(err.Error())
			}
		} else {
			msg := fmt.Sprintf("%s run %s as %s: denied\n", u.Username, args[0], as)
			stderr = []byte(msg)
			logger.Notice(msg)
		}

		b, err := json.Marshal(proj.Result{Stdout: stdout, Stderr: stderr})
		if err != nil {
			logger.Notice(err.Error())
		}

		c.Write(b)

		c.Close()
	}

	done <- true
}

func consume() {
	logger, err := syslog.New(syslog.LOG_NOTICE, os.Args[0])
	if err != nil {
		/*
		 * System might be booting, and syslog might not have started.
		 * Try again after a few seconds.
		 */
		time.Sleep(10 * time.Second)

		logger, err = syslog.New(syslog.LOG_NOTICE, os.Args[0])
		if err != nil {
			panic(err)
		}
	}

	for {
		j := <-jobs
		_, _, err := j.run()
		if err != nil {
			logger.Notice(err.Error())
		}

		if j.notify {
			url := "http://aquinas-www." + conf.Domain() + "/api/notify?uuid=" + j.id.String()
			resp, err := http.Get(url)
			if err != nil {
				logger.Err("notification failed: " + url + ": " + err.Error())
				continue
			}
			if resp.StatusCode != http.StatusOK {
				logger.Err("notification failed: " + url + ": " + resp.Status)
				continue
			}

			logger.Notice(j.id.String() + " notified httpd")
		}

		logger.Notice(j.id.String() + " complete: " + strings.Join(j.args, " "))
	}
}
