package main

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
)

func main() {
	var f *os.File
	var err error

	if len(os.Args) > 2 {
		fmt.Fprintf(os.Stderr, "usage: %s [FILE]\n", os.Args[0])
		os.Exit(1)
	}

	if len(os.Args) == 1 {
		f = os.Stdin
	} else {
		f, err = os.Open(os.Args[1])
		if err != nil {
			fmt.Fprintf(os.Stderr, "could not open: %s\n", err)
			os.Exit(1)
		}

		defer f.Close()
	}

	input := bufio.NewScanner(f)
	for input.Scan() {
		line := input.Text()

		re := regexp.MustCompile(`^const char \*SECRET = .*`)
		line = re.ReplaceAllString(line, `const char *SECRET = "[Sanitized]";`)

		re = regexp.MustCompile(`^#define SECRET.*`)
		line = re.ReplaceAllString(line, `#define SECRET "[Sanitized]"`)

		re = regexp.MustCompile(`^const SECRET = .*`)
		line = re.ReplaceAllString(line, `const SECRET = "[Sanitized]"`)

		fmt.Println(line)
	}
}
