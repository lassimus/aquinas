package main

func (s *server) routes() {
	s.handler.HandleFunc("/",                     s.mirror(s.handleGetRestUnauth(), s.handleGetRestAuth())).Methods("GET")
	s.handler.HandleFunc("/login",                s.mirror(s.handleGetLoginUnauth(), s.handleGetLoginAuth())).Methods("GET")
	s.handler.HandleFunc("/projects",             s.mirror(s.handleGetWasmUnauth(), s.handleGetWasmAuth())).Methods("GET")
	s.handler.HandleFunc("/references",           s.mirror(s.handleGetReferenceUnauth(), s.handleGetReferenceAuth())).Methods("GET")

	s.handler.HandleFunc("/project/{id:[a-zA-Z0-9-]+}", s.mirror(s.handleGetWasmUnauth(), s.handleGetWasmAuth())).Methods("GET")
	s.handler.HandleFunc("/project/{id:[a-zA-Z0-9-]+}/img/{rest}", s.handleGetStatic()).Methods("GET")
	s.handler.HandleFunc("/project/{id:[a-zA-Z0-9-]+}/files/{rest}", s.handleGetStatic()).Methods("GET")

	s.handler.HandleFunc("/landing",              s.handleGetLanding()).Methods("GET")
	s.handler.HandleFunc("/register",             s.handleGetRegister()).Methods("GET")
	s.handler.HandleFunc("/register2",            s.handlePostWasmUnauth()).Methods("POST")
	s.handler.HandleFunc("/register3",            s.handleGetWasmUnauth()).Methods("GET")
	s.handler.HandleFunc("/reset",                s.handleGetReset()).Methods("GET")
	s.handler.HandleFunc("/reset2",               s.handlePostWasmUnauth()).Methods("POST")
	s.handler.HandleFunc("/reset3",               s.handleGetWasmUnauth()).Methods("GET")
	s.handler.HandleFunc("/bin/{rest}",           s.handleGetStatic()).Methods("GET")
	s.handler.HandleFunc("/files/{rest}",         s.handleGetStatic()).Methods("GET")
	s.handler.HandleFunc("/css/{rest}",           s.handleGetStatic()).Methods("GET")
	s.handler.HandleFunc("/img/{rest}",           s.handleGetStatic()).Methods("GET")
	s.handler.HandleFunc("/fonts/{rest}",         s.handleGetStatic()).Methods("GET")
	s.handler.HandleFunc("/js/{rest}",            s.handleGetStatic()).Methods("GET")
	s.handler.HandleFunc("/.well-known/{rest}",   s.handleGetStatic()).Methods("GET")
	s.handler.HandleFunc("/wasm/{rest}",          s.handleGetStatic()).Methods("GET")

	s.handler.HandleFunc("/alias",                s.protect(s.handlePostWasmAuth())).Methods("GET")
	s.handler.HandleFunc("/alias2",               s.protect(s.handlePostWasmAuth())).Methods("POST")
	s.handler.HandleFunc("/git-provider",         s.protect(s.handlePostWasmAuth())).Methods("GET")
	s.handler.HandleFunc("/git-provider2",        s.protect(s.handlePostWasmAuth())).Methods("POST")
	s.handler.HandleFunc("/book",                 s.protect(s.handleGetBook())).Methods("GET")
	s.handler.HandleFunc("/course",               s.protect(s.handleGetWasmAuth())).Methods("GET")
	s.handler.HandleFunc("/password",             s.protect(s.handleGetPassword())).Methods("GET")
	s.handler.HandleFunc("/password2",            s.protect(s.handlePostWasmAuth())).Methods("POST")
	s.handler.HandleFunc("/keys",                 s.protect(s.handleGetWasmAuth())).Methods("GET")
	s.handler.HandleFunc("/keys2",                s.protect(s.handlePostWasmAuth())).Methods("POST")
	s.handler.HandleFunc("/rankings",             s.protect(s.handleGetWasmAuth())).Methods("GET")
	s.handler.HandleFunc("/remove",               s.protect(s.handleGetRemove())).Methods("GET")
	s.handler.HandleFunc("/remove2",              s.protect(s.handlePostWasmAuth())).Methods("POST")
	s.handler.HandleFunc("/logout",               s.protect(s.handleGetLogout())).Methods("GET")

	s.handler.HandleFunc("/api/students/{email}", s.protect(s.handleAPIGetStudents())).Methods("GET")
	s.handler.HandleFunc("/api/students/{email}", s.protect(s.handleAPIPostStudents())).Methods("POST")
	s.handler.HandleFunc("/api/students/{email}", s.protect(s.handleAPIDeleteStudents())).Methods("DELETE")
	s.handler.HandleFunc("/api/grades",           s.protect(s.handleAPIGetGrades())).Methods("GET")
	s.handler.HandleFunc("/api/grades-now",       s.protect(s.handleAPIGetGradesNow())).Methods("GET")

	s.handler.HandleFunc("/api/projects",         s.handleAPIGetProjects()).Methods("GET")

	/* Results differ, depending on whether authenticated or not. */
	s.handler.HandleFunc("/api/projects/{id:[a-zA-Z0-9-]+}", s.mirror(s.handleAPIGetProjectsNamedUnauth(), s.handleAPIGetProjectsNamedAuth())).Methods("GET")

	/*
	 * Does NOT require authentication. UUIDs are random, so an attacker
	 * cannot submit a valid one.
	 */
	s.handler.HandleFunc("/api/notify",           s.handleAPIGetNotify()).Methods("GET")
	s.handler.HandleFunc("/api/wait",             s.handleAPIGetWait()).Methods("GET")

	s.handler.HandleFunc("/api/register/{email}", s.handleAPIGetRegister()).Methods("GET")
	s.handler.HandleFunc("/api/register/{email}", s.handleAPIPostRegister()).Methods("POST")
	s.handler.HandleFunc("/api/reset-password/{email}", s.handleAPIGetResetPassword()).Methods("GET")
	s.handler.HandleFunc("/api/reset-password/{email}", s.handleAPIPostResetPassword()).Methods("POST")

	/* Everything else. */
	s.handler.HandleFunc("/{rest}",               s.mirror(s.handleGetRestUnauth(), s.handleGetRestAuth())).Methods("GET")
}
