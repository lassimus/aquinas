package main

/*
 * These handlers process input from the network, and thus must take
 * care to sanitize such input. Input arrives as query data and URL path
 * components. Two safeties help ensure safe input processing:
 *
 * (1) Handlers sanitize input that comes from a URL path component
 *     using varFromPath().
 *
 * (2) Handlers sanitize input that comes from forms or a URL query
 *     component using readForm().
 *
 * These safeties rely on the correct selection of types to hold parsed
 * data. For example, safe.String is not useful to capture file names,
 * because safe.String permits the '/' character, and thus things like
 * "../../y." The safe.Filename type prohibits '/'.
 *
 * Note that other mechanisms might also filter ill-formatted data.
 * This includes the regular expressions that define handler routes.
 * For clarity, the handlers herein do not rely on such measures.
 */

import (
	"bytes"
	"../common"
	"../conf"
	"../db"
	"encoding"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"github.com/gorilla/schema"
	"html/template"
	"net/http"
	"net/smtp"
	"os"
	"path"
	"../proj"
	"../run"
	"../safe"
	"strconv"
	"strings"
	textTemplate "text/template"
	"time"
)

var mimeMap = map[string]string{
	".css":   "text/css",
	".ico":   "image/x-icon",
	".jpg":   "image/jpeg",
	".js":    "text/javascript",
	".png":   "image/png",
	".svg":   "image/svg+xml",
	".wasm":  "application/wasm",
	".woff2": "font/woff2",
	".woff":  "font/woff",
}

var jobs = newJobs()

var funcMap = template.FuncMap{
	"normalizeUsername": common.NormalizeUsername,
}

var templates *template.Template

func loadTemplates() {
	templates = template.Must(template.New("").Funcs(funcMap).ParseGlob(path.Join(*root, "*.tmpl")))
	templates.ParseGlob(path.Join(*root, "project", "*", "*.tmpl"))
}

var decoder = schema.NewDecoder()

func readForm(values interface{}, r *http.Request) (err error) {
	err = r.ParseForm()
	if err != nil {
		return
	}

	switch r.Method {
	case http.MethodGet:
		err = decoder.Decode(values, r.Form)
		if err != nil {
			return
		}
	case http.MethodPost:
		err = decoder.Decode(values, r.PostForm)
		if err != nil {
			return
		}
	default:
		err = errors.New("unsupported method: " + r.Method)
		return
	}

	return
}

func varFromPath(u encoding.TextUnmarshaler, r *http.Request, k string) error {
	return u.UnmarshalText([]byte(mux.Vars(r)[k]))
}

func (s *server) _protect(w http.ResponseWriter, r *http.Request, h handlerFuncAuth, user, pass, file string) {
	if err := s.db.Authenticate(user, pass); err != nil {
	logger.Notice(err.Error())

		serveLogin(w)
		return
	}

	if err := s.db.Permitted(file, user); err != nil {
		dualErr := dualError{err: err, status: http.StatusForbidden}
		s.handleError(w, &dualErr, "project is restricted")
		return
	}

	switch file {
	case "keys", "keys2", "alias", "alias2", path.Join("api", "students", user):
		/* Setting up things on first login. */
		h(w, r, user)
	default:
		/* Ask for and install SSH key on first login. */
		sshAuthKeys, err := s.db.SSHAuthKeys(user)
		if err != nil {
			dualErr := dualError{err: err, status: http.StatusInternalServerError}
			s.handleError(w, &dualErr, "")
			return
		}

		if sshAuthKeys == "" {
			http.Redirect(w, r, "/keys", http.StatusSeeOther)
			logger.Notice("forwarding from " + file + " to ssh")
			return
		}

		/* Ask for alias on first login. */
		alias, err := s.db.Alias(user)
		if err != nil {
			dualErr := dualError{err: err, status: http.StatusInternalServerError}
			s.handleError(w, &dualErr, "")
			return
		}

		if alias == "" {
			http.Redirect(w, r, "/alias", http.StatusSeeOther)
			logger.Notice("forwarding from " + file + " to alias")
			return
		}

		h(w, r, user)
	}
}

/* Serves differently depending on if logged in or not. */
func (s *server) mirror(hUnauth http.HandlerFunc, hAuth handlerFuncAuth) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		file := r.URL.Path[1:]

		user, pass, ok := r.BasicAuth()
		if !ok {
			if err := s.db.Permitted(file, ""); err != nil {
				dualErr := dualError{err: err, status: http.StatusForbidden}
				s.handleError(w, &dualErr, "project is restricted")
				return
			}

			hUnauth(w, r)
		} else {
			s._protect(w, r, hAuth, user, pass, file)
		}
	}
}

/* Serves if logged in; otherwise prompts for credentials. */
func (s *server) protect(h handlerFuncAuth) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		user, pass, ok := r.BasicAuth()
		if !ok {
			serveLogin(w)
			return
		}

		s._protect(w, r, h, user, pass, r.URL.Path[1:])
	}
}

func (s *server) handleError(w http.ResponseWriter, err *dualError, msg string) {
	var page bytes.Buffer

	if msg != "" {
		logger.Err(err.Error() + ": " + msg)
	} else {
		logger.Err(err.Error())
	}

	status := strconv.Itoa(err.Status())
	err2 := templates.ExecuteTemplate(&page,
	                                   status + ".tmpl",
	                                   map[string]interface{}{"message": err.ErrorPub(msg)})
	if err2 != nil {
		http.Error(w,
		          "failed to process " + status + " template",
		           http.StatusInternalServerError)
		return
	}

	w.WriteHeader(err.Status())
	w.Header().Set("Content-Type", "text/html")
	page.WriteTo(w)
}

func (s *server) handleAPIError(w http.ResponseWriter, err *dualError, msg string) {
	if msg != "" {
		logger.Err(err.Error() + ": " + msg)
	} else {
		logger.Err(err.Error())
	}

	http.Error(w, err.ErrorPub(msg), err.Status())
}

func (s *server) handleGetRestUnauth() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path == "/" {
			http.Redirect(w, r, "/landing", http.StatusSeeOther)
			logger.Notice("forwarding from / to landing")
			return
		}

		file := r.URL.Path[1:]

		tmpl := file + ".tmpl"
		if templates.Lookup(tmpl) == nil {
			err := errors.New("unknown template: " + tmpl)
			s.handleError(w, &dualError{err: err, status: http.StatusNotFound}, tmpl)
			return
		}

		values := map[string]interface{}{
			"gitHost": common.GitHost(r.Host),
			"root": conf.Root(),
		}

		if err := serve(w, r, file, values); err != nil {
			s.handleError(w, err, "")
		}
	}
}

func (s *server) handleGetLoginUnauth() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		serveLogin(w)
	}
}

func (s *server) handleGetLanding() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if err := serve(w, r, "landing", nil); err != nil {
			s.handleError(w, err, "")
		}
	}
}

func (s *server) handleGetRegister() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if err := serve(w, r, "register", nil); err != nil {
			s.handleError(w, err, "")
		}
	}
}

func (s *server) handleGetReset() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if err := serve(w, r, "reset", nil); err != nil {
			s.handleError(w, err, "")
		}
	}
}

func (s *server) handleGetStatic() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		mimeType, ok := mimeMap[path.Ext(r.URL.Path)]
		if ok {
			w.Header().Set("Content-Type", mimeType)
		} else {
			w.Header().Set("Content-Type", "application/octet-stream")
		}

		p := path.Join(*root, r.URL.Path[1:])

		if _, err := os.Stat(p + ".gz"); err == nil {
			w.Header().Set("Content-Encoding", "gzip")
			http.ServeFile(w, r, p + ".gz")
		} else {
			http.ServeFile(w, r, p)
		}
	}
}

func (s *server) handleGetRestAuth() handlerFuncAuth {
	return func(w http.ResponseWriter, r *http.Request, user string) {
		if r.URL.Path == "/" {
			http.Redirect(w, r, "/projects", http.StatusSeeOther)
			logger.Notice("forwarding from / to projects")
			return
		}

		file := r.URL.Path[1:]

		tmpl := file + ".tmpl"
		if templates.Lookup(tmpl) == nil {
			err := errors.New("unknown template: " + tmpl)
			s.handleError(w, &dualError{err: err, status: http.StatusNotFound}, tmpl)
			return
		}

		last, err := s.db.Last(*root, user, file)
		if err != nil {
			dualErr := dualError{err: err, status: http.StatusInternalServerError}
			s.handleError(w, &dualErr, "")
			return
		}

		values := map[string]interface{}{
			"auth": true,
			"teacher": s.db.IsTeacher(user),
			"gitHost": common.GitHost(r.Host),
			"student": user,
			"project": file,
			"last": last,
			"root": conf.Root(),
		}

		if err := serve(w, r, file, values); err != nil {
			s.handleError(w, err, "")
		}
	}
}

func (s *server) handleGetLoginAuth() handlerFuncAuth {
	return func(w http.ResponseWriter, r *http.Request, user string) {
		/* Post-authentication request following first load of login. */
		http.Redirect(w, r, "/", http.StatusSeeOther)
		logger.Notice("forwarding from login to projects")
	}
}

func (s *server) handleGetWasmUnauth() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		file := path.Base(r.URL.Path)

		values := map[string]interface{}{
			"auth": false,
			"teacher": false,
			"wasm": file,
			"student": "",
			"arg": r.URL.RawQuery,
		}

		if err := serve(w, r, "wasm", values); err != nil {
			s.handleError(w, err, "")
		}
	}
}

func (s *server) handleGetWasmAuth() handlerFuncAuth {
	return func(w http.ResponseWriter, r *http.Request, user string) {
		file := path.Base(r.URL.Path)

		values := map[string]interface{}{
			"auth": true,
			"teacher": s.db.IsTeacher(user),
			"wasm": file,
			"student": user,
			"arg": r.URL.RawQuery,
		}

		if err := serve(w, r, "wasm", values); err != nil {
			s.handleError(w, err, "")
		}
	}
}

func (s *server) handlePostWasmUnauth() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var query string

		file := path.Base(r.URL.Path)

		if err := r.ParseForm(); err != nil {
			s.handleError(w, &dualError{err: err, status: http.StatusBadRequest}, "")
			return
		}

		for k, v := range r.PostForm {
			for _, v2 := range v {
				if query != "" {
					query += "&"
				}
				query += k + "=" + v2
			}
		}

		values := map[string]interface{}{
			"auth": false,
			"teacher": false,
			"wasm": file,
			"student": "",
			"arg": query,
		}

		if err := serve(w, r, "wasm", values); err != nil {
			s.handleError(w, err, "")
		}
	}
}

func (s *server) handlePostWasmAuth() handlerFuncAuth {
	return func(w http.ResponseWriter, r *http.Request, user string) {
		var query string

		file := path.Base(r.URL.Path)

		if err := r.ParseForm(); err != nil {
			s.handleError(w, &dualError{err: err, status: http.StatusBadRequest}, "")
			return
		}

		for k, v := range r.PostForm {
			for _, v2 := range v {
				if query != "" {
					query += "&"
				}
				query += k + "=" + v2
			}
		}

		values := map[string]interface{}{
			"auth": true,
			"teacher": s.db.IsTeacher(user),
			"wasm": file,
			"student": user,
			"arg": query,
		}

		if err := serve(w, r, "wasm", values); err != nil {
			s.handleError(w, err, "")
		}
	}
}

func (s *server) handleGetRemove() handlerFuncAuth {
	return func(w http.ResponseWriter, r *http.Request, user string) {
		values := map[string]interface{}{
			"auth": true,
			"teacher": s.db.IsTeacher(user),
			"student": user,
		}

		if err := serve(w, r, "remove", values); err != nil {
			s.handleError(w, err, "")
		}
	}
}

func (s *server) handleGetPassword() handlerFuncAuth {
	return func(w http.ResponseWriter, r *http.Request, user string) {
		values := map[string]interface{}{
			"auth": true,
			"teacher": s.db.IsTeacher(user),
			"student": user,
		}

		if err := serve(w, r, "password", values); err != nil {
			s.handleError(w, err, "")
		}
	}
}

func (s *server) handleGetReferenceUnauth() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if err := serve(w, r, "references", nil); err != nil {
			s.handleError(w, err, "")
		}
	}
}

func (s *server) handleGetReferenceAuth() handlerFuncAuth {
	return func(w http.ResponseWriter, r *http.Request, user string) {
		values := map[string]interface{}{
			"auth": true,
			"teacher": s.db.IsTeacher(user),
			"student": user,
		}

		if err := serve(w, r, "references", values); err != nil {
			s.handleError(w, err, "")
		}
	}
}

func (s *server) handleGetBook() handlerFuncAuth {
	return func(w http.ResponseWriter, r *http.Request, user string) {
		if !s.db.IsTeacher(user) {
			err := errors.New("non-teacher loaded book")
			s.handleError(w, &dualError{err: err, status: http.StatusForbidden}, "")
			return
		}

		s.handleGetWasmAuth()(w, r, user)
	}
}

func (s *server) handleAPIGetStudents() handlerFuncAuth {
	return func(w http.ResponseWriter, r *http.Request, user string) {
		w.Header().Set("Content-Type", "application/json")

		var student safe.Email

		err := varFromPath(&student, r, "email")
		if err != nil {
			err := fmt.Errorf("ill-formed email: %s", err)
			s.handleAPIError(w, &dualError{err: err, status: http.StatusBadRequest}, "")
			return
		}

		/* Check student is self. */
		if student.String() != user {
			err := errors.New("cannot " + r.Method + " students other than self")
			s.handleAPIError(w, &dualError{err: err, status: http.StatusBadRequest}, "")
			return
		}

		alias, err := s.db.Alias(user)
		if err != nil {
			dualErr := dualError{err: err, status: http.StatusInternalServerError}
			s.handleAPIError(w, &dualErr, "")
			return
		}

		keys, err := s.db.SSHAuthKeys(user)
		if err != nil {
			dualErr := dualError{err: err, status: http.StatusInternalServerError}
			s.handleAPIError(w, &dualErr, "")
			return
		}

		gitProvider, err := s.db.KV(user, "git-provider")
		if err != nil {
			dualErr := dualError{err: err, status: http.StatusInternalServerError}
			s.handleAPIError(w, &dualErr, "")
			return
		}

		gitPath, err := s.db.KV(user, "git-path")
		if err != nil {
			dualErr := dualError{err: err, status: http.StatusInternalServerError}
			s.handleAPIError(w, &dualErr, "")
			return
		}

		gitUsername, err := s.db.KV(user, "git-username")
		if err != nil {
			dualErr := dualError{err: err, status: http.StatusInternalServerError}
			s.handleAPIError(w, &dualErr, "")
			return
		}

		gitToken, err := s.db.KV(user, "git-token")
		if err != nil {
			dualErr := dualError{err: err, status: http.StatusInternalServerError}
			s.handleAPIError(w, &dualErr, "")
			return
		}

		enc := json.NewEncoder(w)

		err = enc.Encode(map[string]string{
			"alias":        alias,
			"keys":         keys,
			"git-provider": gitProvider,
			"git-path":     gitPath,
			"git-username": gitUsername,
			"git-token":    gitToken,
		})
		if err != nil {
			s.handleAPIError(w, &dualError{err: err, status: http.StatusInternalServerError}, "")
			return
		}
	}
}

func (s *server) handleAPIPostStudents() handlerFuncAuth {
	return func(w http.ResponseWriter, r *http.Request, user string) {
		w.Header().Set("Content-Type", "application/json")

		var student safe.Email

		err := varFromPath(&student, r, "email")
		if err != nil {
			err := fmt.Errorf("ill-formed email: %s", err)
			s.handleAPIError(w, &dualError{err: err, status: http.StatusBadRequest}, "")
			return
		}

		/* Check student is self. */
		if student.String() != user {
			err := errors.New("cannot " + r.Method + " students other than self")
			s.handleAPIError(w, &dualError{err: err, status: http.StatusBadRequest}, "")
			return
		}

		values := new(struct {
			Password    *safe.String `schema:"password"`
			Alias       *safe.String `schema:"alias"`
			Key         *safe.Key    `schema:"keys"`
			GitProvider *safe.String `schema:"git-provider"`
			GitPath     *safe.String `schema:"git-path"`
			GitUsername *safe.String `schema:"git-username"`
			GitToken    *safe.String `schema:"git-token"`
		})

		err = readForm(values, r)
		if err != nil {
			s.handleAPIError(w, &dualError{err: err, status: http.StatusBadRequest}, "")
			return
		}

		if values.Alias != nil {
			logger.Notice("received new alias")

			alias := values.Alias.String()

			if err := s.db.SetAlias(user, alias); err != nil {
				if err == db.ErrAliasExists {
					s.handleAPIError(w, &dualError{err: err, status: http.StatusBadRequest}, err.Error())
				} else {
					s.handleAPIError(w, &dualError{err: err, status: http.StatusInternalServerError}, "")
				}
				return
			}
		}

		if values.Key != nil {
			logger.Notice("received new public SSH key")

			keys := values.Key.String()

			jobs.Lock()
			id, err := s.db.SetSSHAuthKeys(user, keys)
			if err != nil {
				jobs.Unlock()
				s.handleAPIError(w, &dualError{err: err, status: http.StatusInternalServerError}, "")
				return
			}
			jobs.UnlockAndRegister(id)
			jobs.Wait(id)
		}

		if values.Password != nil {
			logger.Notice("received new password")

			password := values.Password.String()

			if err := s.db.SetPassword(user, password); err != nil {
				s.handleAPIError(w, &dualError{err: err, status: http.StatusInternalServerError}, "")
				return
			}
		}

		if values.GitProvider != nil {
			logger.Notice("received new Git provider")

			gitProvider := values.GitProvider.String()

			if err := s.db.SetKV(user, "git-provider", gitProvider); err != nil {
				s.handleAPIError(w, &dualError{err: err, status: http.StatusInternalServerError}, "")
				return
			}
		}

		if values.GitPath != nil {
			logger.Notice("received new Git path")

			gitPath := values.GitPath.String()

			if err := s.db.SetKV(user, "git-path", gitPath); err != nil {
				s.handleAPIError(w, &dualError{err: err, status: http.StatusInternalServerError}, "")
				return
			}
		}

		if values.GitUsername != nil {
			logger.Notice("received new Git username")

			gitUsername := values.GitUsername.String()

			if err := s.db.SetKV(user, "git-username", gitUsername); err != nil {
				s.handleAPIError(w, &dualError{err: err, status: http.StatusInternalServerError}, "")
				return
			}
		}

		if values.GitToken != nil {
			logger.Notice("received new Git token")

			gitToken := values.GitToken.String()

			if err := s.db.SetKV(user, "git-token", gitToken); err != nil {
				s.handleAPIError(w, &dualError{err: err, status: http.StatusInternalServerError}, "")
				return
			}
		}

		enc := json.NewEncoder(w)

		err = enc.Encode(true)
		if err != nil {
			s.handleAPIError(w, &dualError{err: err, status: http.StatusInternalServerError}, "")
			return
		}

		return
	}
}

func (s *server) handleAPIDeleteStudents() handlerFuncAuth {
	return func(w http.ResponseWriter, r *http.Request, user string) {
		w.Header().Set("Content-Type", "application/json")

		var student safe.Email

		err := varFromPath(&student, r, "email")
		if err != nil {
			err := fmt.Errorf("ill-formed email: %s", err)
			s.handleAPIError(w, &dualError{err: err, status: http.StatusBadRequest}, "")
			return
		}

		/* Check student is self. */
		if student.String() != user {
			err := errors.New("cannot " + r.Method + " students other than self")
			s.handleAPIError(w, &dualError{err: err, status: http.StatusBadRequest}, "")
			return
		}

		logger.Notice("received request to remove " + user)

		jobs.Lock()
		id, err := s.db.RemoveStudent(user)
		if err != nil {
			jobs.Unlock()
			s.handleAPIError(w, &dualError{err: err, status: http.StatusInternalServerError}, "")
			return
		}
		jobs.UnlockAndRegister(id)
		jobs.Wait(id)

		enc := json.NewEncoder(w)

		err = enc.Encode(true)
		if err != nil {
			s.handleAPIError(w, &dualError{err: err, status: http.StatusInternalServerError}, "")
			return
		}

		return
	}
}

func (s *server) handleAPIGetGradesNow() handlerFuncAuth {
	return func(w http.ResponseWriter, r *http.Request, user string) {
		values := new(struct {
			Project *safe.Filename `schema:"project"`
		})

		err := readForm(values, r)
		if err != nil {
			s.handleAPIError(w, &dualError{err: err, status: http.StatusBadRequest}, "")
			return
		}

		if common.HasNil(values) {
			err := fmt.Errorf("missing parameter: %v", values)
			s.handleAPIError(w, &dualError{err: err, status: http.StatusBadRequest}, "")
			return
		}

		repo, provider := db.BuildRepo(s.db, common.NormalizeUsername(user), common.GitHost(r.Host), conf.Root(), user, values.Project.String())
		if provider == "aquinas" {
			tok := strings.Split(repo, ":")
			if len(tok) != 2 {
				err := errors.New("bad aquinas repository: " + repo)
				s.handleAPIError(w, &dualError{err: err, status: http.StatusInternalServerError}, "")
				return
			}
			repo = tok[1]
		}
		stdin := strings.NewReader("check " + user + " " + values.Project.String() + " " + repo + "\n")
		_, stderr, err := run.SSH(stdin, nil, "aquinas-git." + conf.Domain())
		if err != nil {
			s.handleAPIError(w, &dualError{err: err, status: http.StatusInternalServerError}, "")
			return
		}
		if len(stderr) > 0 {
			err = errors.New("failed interaction with Git provider")
			s.handleAPIError(w, &dualError{err: err, status: http.StatusServiceUnavailable}, string(stderr))
			return
		}

		/* TODO: Is resetting things in this way is safe? */
		r.URL.RawQuery += "&student=" + user
		r.Form = nil
		r.PostForm = nil
		s.handleAPIGetGrades()(w, r, user)
	}
}

func (s *server) handleAPIGetGrades() handlerFuncAuth {
	return func(w http.ResponseWriter, r *http.Request, user string) {
		w.Header().Set("Content-Type", "application/json")

		values := new(struct {
			Student *safe.Email         `schema:"student"`
			Project *safe.Filename      `schema:"project"`
			Brief   *safe.BooleanString `schema:"brief"`
		})

		err := readForm(values, r)
		if err != nil {
			s.handleAPIError(w, &dualError{err: err, status: http.StatusBadRequest}, "")
			return
		}

		if values.Student == nil {
			values.Student = new(safe.Email)
			if err := values.Student.UnmarshalText([]byte("")); err != nil {
				s.handleAPIError(w, &dualError{err: err, status: http.StatusBadRequest}, "")
				return
			}
		}
		if values.Project == nil {
			values.Project = new(safe.Filename)
			if err := values.Project.UnmarshalText([]byte("")); err != nil {
				s.handleAPIError(w, &dualError{err: err, status: http.StatusBadRequest}, "")
				return
			}
		}
		if values.Brief == nil {
			values.Brief = new(safe.BooleanString)
			if err := values.Brief.UnmarshalText([]byte("false")); err != nil {
				s.handleAPIError(w, &dualError{err: err, status: http.StatusBadRequest}, "")
				return
			}
		}

		grades, err := s.db.Grades(
			user,
			values.Student.String(),
			values.Project.String(),
			values.Brief.String() == "true")
		if err != nil {
			s.handleAPIError(w, &dualError{err: err, status: http.StatusInternalServerError}, "")
			return
		}

		enc := json.NewEncoder(w)

		err = enc.Encode(grades)
		if err != nil {
			s.handleAPIError(w, &dualError{err: err, status: http.StatusInternalServerError}, "")
			return
		}
	}
}

func (s *server) handleAPIGetProjects() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		projects := make(map[string]proj.Project)

		w.Header().Set("Content-Type", "application/json")

		values := new(struct {
			Tag *safe.String `schema:"tag"`
		})

		err := readForm(values, r)
		if err != nil {
			s.handleAPIError(w, &dualError{err: err, status: http.StatusBadRequest}, "")
			return
		}

		if values.Tag != nil {
			for k, v := range projectList {
				if _, ok := v.TagsF[values.Tag.String()]; ok {
					projects[k] = v
				}
			}
		} else {
			projects = projectList
		}

		enc := json.NewEncoder(w)

		if err := enc.Encode(projects); err != nil {
			s.handleAPIError(w, &dualError{err: err, status: http.StatusInternalServerError}, "")
			return
		}
	}
}

func (s *server) handleAPIGetProjectsNamed(w http.ResponseWriter, r *http.Request, name string, values map[string]interface{}) {
	var inst proj.Instructions

	w.Header().Set("Content-Type", "application/json")

	tmpl := name + ".tmpl"
	if templates.Lookup(tmpl) == nil {
		err := errors.New("unknown template: " + tmpl)
		s.handleAPIError(w, &dualError{err: err, status: http.StatusNotFound}, tmpl)
		return
	}

	var buf bytes.Buffer

	if v, ok := projectList[name]; ok {
		inst.Title = "The " + v.NameF + " project"
		inst.Summary = v.SummaryF
	}

	if inst.Title == "" || inst.Summary == "" {
		err := errors.New("unknown project: " + name)
		s.handleAPIError(w, &dualError{err: err, status: http.StatusInternalServerError}, "")
		return
	}

	if err := templates.ExecuteTemplate(&buf, tmpl, values); err != nil {
		s.handleAPIError(w, &dualError{err: err, status: http.StatusInternalServerError}, "")
		return
	}
	inst.Instructions = buf.String()

	enc := json.NewEncoder(w)

	if err := enc.Encode(inst); err != nil {
		s.handleAPIError(w, &dualError{err: err, status: http.StatusInternalServerError}, "")
		return
	}
}

func (s *server) handleAPIGetProjectsNamedUnauth() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var name safe.Filename

		err := varFromPath(&name, r, "id")
		if err != nil {
			err := fmt.Errorf("ill-formed name: %s", err)
			s.handleAPIError(w, &dualError{err: err, status: http.StatusBadRequest}, "")
			return
		}

		values := map[string]interface{}{
			"gitHost": common.GitHost(r.Host),
			"root": conf.Root(),
		}

		s.handleAPIGetProjectsNamed(w, r, name.String(), values)
	}
}

func (s *server) handleAPIGetProjectsNamedAuth() handlerFuncAuth {
	return func(w http.ResponseWriter, r *http.Request, user string) {
		var name safe.Filename

		err := varFromPath(&name, r, "id")
		if err != nil {
			err := fmt.Errorf("ill-formed name: %s", err)
			s.handleAPIError(w, &dualError{err: err, status: http.StatusBadRequest}, "")
			return
		}

		last, err := s.db.Last(*root, user, name.String())
		if err != nil {
			dualErr := dualError{err: err, status: http.StatusInternalServerError}
			s.handleAPIError(w, &dualErr, "")
			return
		}

		repo, _ := db.BuildRepo(s.db, common.NormalizeUsername(user), common.GitHost(r.Host), conf.Root(), user, name.String())

		values := map[string]interface{}{
			"auth": true,
			"teacher": s.db.IsTeacher(user),
			"gitHost": common.GitHost(r.Host),
			"student": user,
			"project": name.String(),
			"last": last,
			"root": conf.Root(),
			"repo": repo,
		}

		s.handleAPIGetProjectsNamed(w, r, name.String(), values)
	}
}

/*
 * Does NOT require authentication. UUIDs are random, so an attacker
 * cannot submit a valid one.
 */
func (s *server) handleAPIGetNotify() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")

		values := new(struct {
			UUID *uuid.UUID `schema:"uuid"`
		})

		err := readForm(values, r)
		if err != nil {
			s.handleAPIError(w, &dualError{err: err, status: http.StatusBadRequest}, "")
			return
		}

		if common.HasNil(values) {
			err := fmt.Errorf("missing parameter: %v", values)
			s.handleAPIError(w, &dualError{err: err, status: http.StatusBadRequest}, "")
			return
		}

		ok := jobs.Notify(values.UUID.String())
		if !ok {
			fmt.Fprintf(w, "false")
			return
		}

		fmt.Fprintf(w, "true")
	}
}

/*
 * Does NOT require authentication. UUIDs are random, so an attacker
 * cannot submit a valid one.
 */
func (s *server) handleAPIGetWait() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")

		values := new(struct {
			UUID *uuid.UUID `schema:"id"`
		})

		err := readForm(values, r)
		if err != nil {
			s.handleAPIError(w, &dualError{err: err, status: http.StatusBadRequest}, "")
			return
		}

		if common.HasNil(values) {
			err := fmt.Errorf("missing parameter: %v", values)
			s.handleAPIError(w, &dualError{err: err, status: http.StatusBadRequest}, "")
			return
		}

		jobs.Wait(values.UUID.String())
		fmt.Fprintf(w, "true")
	}
}

func (s *server) handleAPIGetResetPassword() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")

		var student safe.Email

		err := varFromPath(&student, r, "email")
		if err != nil {
			err := fmt.Errorf("ill-formed email: %s", err)
			s.handleAPIError(w, &dualError{err: err, status: http.StatusBadRequest}, "")
			return
		}

		if !common.AllowedEmailDomain(student.String()) {
			msg := "Aquinas presently limits its users to select email domains"
			err := errors.New("disallowed email domain")
			s.handleAPIError(w, &dualError{err: err, status: http.StatusBadRequest}, msg)
			return
		}

		/* Note check in handleAPIRegister and handleAPIResetPassword differ. */
		if !s.db.StudentExists(student.String()) {
			err = errors.New("student does not exist")
			s.handleAPIError(w, &dualError{err: err, status: http.StatusBadRequest}, err.Error())
			return
		}

		p := path.Join(*root, "email-reset")
		tmpl, err := textTemplate.New(path.Base(p)).ParseFiles(p)
		if err != nil {
			s.handleAPIError(w, &dualError{err: err, status: http.StatusInternalServerError}, "")
			return
		}

		id := uuid.New()

		pass, err := common.Password()
		if err != nil {
			s.handleAPIError(w, &dualError{err: err, status: http.StatusInternalServerError}, "")
			return
		}

		nonce := common.Nonce()
		token := nonce + student.String() + secret.String()

		emailBodyFilled := bytes.Buffer{}
		err = tmpl.Execute(&emailBodyFilled, map[string]interface{}{
			"date": time.Now().Format(time.RFC1123Z),
			"from": conf.EmailSender(),
			"fromName": conf.EmailSenderName(),
			"host": r.Host,
			"nonce": nonce,
			"token": common.HashCalc(token),
			"student": student.String(),
			"password": pass,
			"uuid": id})
		if err != nil {
			s.handleAPIError(w, &dualError{err: err, status: http.StatusInternalServerError}, "")
			return
		}

		logger.Notice("preparing reset email" +
		              " student: "  + student.String() +
		              " nonce: "    + nonce +
		              " token: "    + common.HashCalc(token) +
		              " password: " + pass)

		if strings.HasSuffix(student.String(), "example.com") {
			msg := "assuming use of invalid @example.com is a test, and thus not sending email"
			err := errors.New("disallowed email domain (example.com)")
			s.handleAPIError(w, &dualError{err: err, status: http.StatusBadRequest}, msg)
			return
		}

		err = smtp.SendMail(conf.EmailRelay(), nil, conf.EmailSender(),
		                    []string{student.String()},
		                    []byte(emailBodyFilled.Bytes()))
		if err != nil {
			s.handleAPIError(w, &dualError{err: err, status: http.StatusInternalServerError}, "")
			return
		}

		enc := json.NewEncoder(w)

		err = enc.Encode(true)
		if err != nil {
			s.handleAPIError(w, &dualError{err: err, status: http.StatusInternalServerError}, "")
			return
		}
	}
}

func (s *server) handleAPIPostResetPassword() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")

		var student safe.Email

		err := varFromPath(&student, r, "email")
		if err != nil {
			err := fmt.Errorf("ill-formed email: %s", err)
			s.handleAPIError(w, &dualError{err: err, status: http.StatusBadRequest}, "")
			return
		}

		values := new(struct {
			Nonce       *safe.String `schema:"nonce"`
			HashedToken *safe.String `schema:"token"`
			Password    *safe.String `schema:"password"`
		})

		err = readForm(values, r)
		if err != nil {
			s.handleAPIError(w, &dualError{err: err, status: http.StatusBadRequest}, "")
			return
		}

		if common.HasNil(values) {
			err := fmt.Errorf("missing parameter: %v", values)
			s.handleAPIError(w, &dualError{err: err, status: http.StatusBadRequest}, "")
			return
		}

		token := values.Nonce.String() + student.String() + secret.String()
		if !common.HashSame(values.HashedToken.String(), common.HashCalc(token)) {
			msg := "faulty authentication; token likely expired."
			err = errors.New(msg)
			s.handleAPIError(w, &dualError{err: err, status: http.StatusBadRequest}, msg)
			return
		}

		logger.Notice("received valid authentication to password-reset3")

		if err = s.db.SetPassword(student.String(), values.Password.String()); err != nil {
			s.handleAPIError(w, &dualError{err: err, status: http.StatusInternalServerError}, "")
			return
		}

		enc := json.NewEncoder(w)

		err = enc.Encode(true)
		if err != nil {
			s.handleAPIError(w, &dualError{err: err, status: http.StatusInternalServerError}, "")
			return
		}
	}
}

func (s *server) handleAPIGetRegister() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")

		var student safe.Email

		err := varFromPath(&student, r, "email")
		if err != nil {
			err := fmt.Errorf("ill-formed email: %s", err)
			s.handleAPIError(w, &dualError{err: err, status: http.StatusBadRequest}, "")
			return
		}

		if !common.AllowedEmailDomain(student.String()) {
			msg := "Aquinas presently limits its users to select email domains"
			err := errors.New("disallowed email domain")
			s.handleAPIError(w, &dualError{err: err, status: http.StatusBadRequest}, msg)
			return
		}

		/* Note check in handleAPIRegister and handleAPIResetPassword differ. */
		if s.db.StudentExists(student.String()) {
			err = errors.New("student exists")
			s.handleAPIError(w, &dualError{err: err, status: http.StatusBadRequest}, err.Error())
			return
		}

		p := path.Join(*root, "email-register")
		tmpl, err := textTemplate.New(path.Base(p)).ParseFiles(p)
		if err != nil {
			s.handleAPIError(w, &dualError{err: err, status: http.StatusInternalServerError}, "")
			return
		}

		id := uuid.New()

		pass, err := common.Password()
		if err != nil {
			s.handleAPIError(w, &dualError{err: err, status: http.StatusInternalServerError}, "")
			return
		}

		nonce := common.Nonce()
		token := nonce + student.String() + secret.String()

		emailBodyFilled := bytes.Buffer{}
		err = tmpl.Execute(&emailBodyFilled, map[string]interface{}{
			"date": time.Now().Format(time.RFC1123Z),
			"from": conf.EmailSender(),
			"fromName": conf.EmailSenderName(),
			"host": r.Host,
			"nonce": nonce,
			"token": common.HashCalc(token),
			"student": student.String(),
			"password": pass,
			"uuid": id})
		if err != nil {
			s.handleAPIError(w, &dualError{err: err, status: http.StatusInternalServerError}, "")
			return
		}

		logger.Notice("preparing registration email" +
		              " student: "  + student.String() +
		              " nonce: "    + nonce +
		              " token: "    + common.HashCalc(token) +
		              " password: " + pass)

		if strings.HasSuffix(student.String(), "example.com") {
			msg := "assuming use of invalid @example.com is a test, and thus not sending email"
			err := errors.New("disallowed email domain (example.com)")
			s.handleAPIError(w, &dualError{err: err, status: http.StatusBadRequest}, msg)
			return
		}

		err = smtp.SendMail(conf.EmailRelay(), nil, conf.EmailSender(),
		                    []string{student.String()},
		                    []byte(emailBodyFilled.Bytes()))
		if err != nil {
			s.handleAPIError(w, &dualError{err: err, status: http.StatusInternalServerError}, "")
			return
		}

		enc := json.NewEncoder(w)

		err = enc.Encode(true)
		if err != nil {
			s.handleAPIError(w, &dualError{err: err, status: http.StatusInternalServerError}, "")
			return
		}
	}
}

func (s *server) handleAPIPostRegister() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")

		var student safe.Email

		err := varFromPath(&student, r, "email")
		if err != nil {
			err := fmt.Errorf("ill-formed email: %s", err)
			s.handleAPIError(w, &dualError{err: err, status: http.StatusBadRequest}, "")
			return
		}

		values := new(struct {
			Nonce       *safe.String `schema:"nonce"`
			HashedToken *safe.String `schema:"token"`
			Password    *safe.String `schema:"password"`
		})

		err = readForm(values, r)
		if err != nil {
			s.handleAPIError(w, &dualError{err: err, status: http.StatusBadRequest}, "")
			return
		}

		if common.HasNil(values) {
			err := fmt.Errorf("missing parameter: %v", values)
			s.handleAPIError(w, &dualError{err: err, status: http.StatusBadRequest}, "")
			return
		}

		token := values.Nonce.String() + student.String() + secret.String()
		if !common.HashSame(values.HashedToken.String(), common.HashCalc(token)) {
			msg := "faulty authentication; token likely expired."
			err = errors.New(msg)
			s.handleAPIError(w, &dualError{err: err, status: http.StatusBadRequest}, msg)
			return
		}

		logger.Notice("received valid authentication to register")

		jobs.Lock()
		id, err := s.db.AddStudent(student.String(), values.Password.String())
		if err != nil {
			jobs.Unlock()
			s.handleAPIError(w, &dualError{err: err, status: http.StatusInternalServerError}, "")
			return
		}

		jobs.UnlockAndRegister(id)

		enc := json.NewEncoder(w)

		err = enc.Encode(id)
		if err != nil {
			s.handleAPIError(w, &dualError{err: err, status: http.StatusInternalServerError}, "")
			return
		}
	}
}

func (s *server) handleGetLogout() handlerFuncAuth {
	return func(w http.ResponseWriter, r *http.Request, user string) {
		w.WriteHeader(http.StatusUnauthorized)
		serve(w, r, "logout", nil)
	}
}

func serveLogin(w http.ResponseWriter) {
	w.Header().Set("WWW-Authenticate", `Basic realm="Restricted"`)
	http.Error(w, "unauthorized", http.StatusUnauthorized)
	logger.Notice("not authenticated: requesting credentials")
}

func serve(w http.ResponseWriter, req *http.Request, file string, values map[string]interface{}) (dualErr *dualError) {
	w.Header().Set("Content-Type", "text/html")

	err := templates.ExecuteTemplate(w, file + ".tmpl", values)
	if err != nil {
		return &dualError{err: err, status: http.StatusInternalServerError}
	}

	return nil
}
