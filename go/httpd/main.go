package main

/* Run as user http on aquinas-www. */

/*
 * Thank you to Mat Ryer for his tips at:
 * https://medium.com/statuscode/how-i-write-go-http-services-after-seven-years-37c208122831
 */

import (
	"flag"
	"fmt"
	"log/syslog"
	"net/http"
	"os"
	"os/user"
	"strconv"
	"time"

	"../common"
	"../db"

	"github.com/google/uuid"
	"github.com/gorilla/mux"
)

var addr = flag.String("addr", ":http", "http.ListenAndServe compatible addr")
var root = flag.String("root", "/www", "root directory for served documents")
var dummy = flag.Bool("dummy", false, "execute using dummy database")
var secret uuid.UUID
var logger *syslog.Writer

var errorMap = map[int]string{
	http.StatusInternalServerError: "Unexpected error; contact admin",
	http.StatusNotFound:            "Not found",
	http.StatusBadRequest:          "Bad request",
	http.StatusForbidden:           "Not permitted",
}

/* One error message for log, another for httpd client. */
type dualError struct {
	err    error
	status int
}

func (e *dualError) Error() string {
	return e.err.Error()
}

func (e *dualError) ErrorPub(msg string) string {
	if msg != "" {
		return errorMap[e.Status()] + ": " + msg + "."
	}

	return errorMap[e.Status()] + "."
}

func (e *dualError) Status() int {
	return e.status
}

type server struct {
	db      db.DB
	handler *mux.Router
}

type handlerFuncAuth func(w http.ResponseWriter, r *http.Request, user string)

func main() {
	var err error
	var s server

	flag.Parse()

	logger, err = syslog.New(syslog.LOG_NOTICE, os.Args[0])
	if err != nil {
		/*
		 * System might be booting, and syslog might not have started.
		 * Try again after a few seconds.
		 */
		time.Sleep(10 * time.Second)

		logger, err = syslog.New(syslog.LOG_NOTICE, os.Args[0])
		if err != nil {
			panic(err)
		}
	}

	uid := os.Geteuid()
	user, err := user.LookupId(strconv.Itoa(uid))
	if err != nil {
		common.Fail(logger.Err, fmt.Errorf("could not lookup uid %d: %s", uid, err))
	}

	/* Required so SSH can find its keys. */
	if err := os.Setenv("HOME", user.HomeDir); err != nil {
		common.Fail(logger.Err, fmt.Errorf("could not setenv HOME: %s", err))
	}

	secret = uuid.New()

	if *dummy {
		s.db = &db.Dummy{}
	} else {
		s.db = make(db.Filesystem)
	}

	s.handler = mux.NewRouter()
	s.routes()
	loadTemplates()

	/*
	 * TODO: Support TLS; I am presently using lighttpd's mod-proxy to provide TLS.
	 * err := http.ListenAndServeTLS(":https", "server.cert", "server.key", nil)
	 */
	if err = http.ListenAndServe(*addr, s.handler); err != nil {
		logger.Err("error starting: " + err.Error())
	}

	os.Exit(1)
}
