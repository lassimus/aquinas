FROM fedora:32

RUN dnf -y install \
    bind-utils \
    gcc \
    gcc-c++ \
    git \
    glibc-static \
    golang \
    golang-x-lint \
    hugo \
    jq \
    LaTeXML \
    make \
    openssh-clients \
    perl \
    wget \
    which

WORKDIR /proj
