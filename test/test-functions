. ../aquinas-functions

msg() {
	printf "%s" "${1}: "
	shift
	printf "%s" "${1}: "
	shift
	echo "${*}"
}

failmsg() {
	msg "[ ] FAIL" "$@"
	exit 1
}

passmsg() {
	msg "[+] PASS" "$@"
	exit 0
}

passmsg2() {
	msg "[+] PASS" "$@"
}

failbad() {
	exitcode="$1"
	shift
	name="$1"
	shift
	string="$*"
	if [ "$exitcode" != 0 ]; then
		failmsg "$name" "$exitcode: $string"
	fi
}

failgood() {
	exitcode="$1"
	shift
	if [ "$exitcode" = 0 ]; then
		exitcode=1
	else
		exitcode=0
	fi
	failbad $exitcode "$@"
}

testcommit() {
	local testname="$1"
	local proj="$2"
	local lang="$3"
	local src="$4"
	local prog="$5"
	local rest="$6"
	local pass_value="$7"
	local last_value="$8"

	local student="$testCredit"
	local norm=$(calcnorm "$testCredit")

	local dir=$(mktemp -d)
	local rand=$RANDOM

	trap "/bin/rm -rf $dir" SIGTERM SIGINT EXIT

	error=$(git clone -q "$norm@aquinas-git.$domain:$root/$student/$proj$lang" "$dir" 2>&1)
	failbad $? "$testname" "clone failed: ${error:-no stderr}"

	pushd "$dir" >/dev/null

	if [ -n "$rest" ]; then
		cp -a $rest .
		git add ./*
	fi

	# Dump a random value to cause a committable change.
	if [ -z "$prog" ]; then
		# Source tree; just place the random value in rand.
		echo $rand >rand
		git add rand
	elif [ -z "$src" ]; then
		echo "$prog" | sed "s/\$rand/$rand/g" >"$proj"
		# Python or another shebang language.
		chmod +x "$proj"
		git add "$proj"
	else
		echo "$prog" | sed "s/\$rand/$rand/g" >"$src"
		git add "$src"
	fi

	git commit -q -s -m "Another revision for testing: $rand" >/dev/null

	output=$(git push -q 2>&1)
	failbad $? "$testname" "git push failed: ${output:-no stderr}"

	uuid=$(echo "$output" | cut -d " " -f 2)
	failbad $? "$testname" "gathering UUID failed: ${uuid:-no stderr}"

	error=$(waitlog 600 "root@$logServer" "$uuid" /mnt/xvdb/var/log/messages 2>&1)
	failbad $? "$testname" "waitlog failed: ${error:-no stderr}"

	hash=$(git log -1 --pretty=%H)

	popd >/dev/null

	record=$root/teacher/workdir/records/$proj/$student$lang.record
	error=$(ssh "root@aquinas-git.$domain" grep -q "$hash.*\"$pass_value\"" "$record" 2>&1)
	failbad $? "$testname" "pass value wrong: ${error:-no stderr}"

	last=$root/teacher/workdir/records/$proj/$student$lang.last
	error=$(ssh "root@aquinas-git.$domain" grep -q "\"$last_value\"" "$last" 2>&1)
	failbad $? "$testname" "last value failed: $last_value not in $last: ${error:-no stderr}"
}

studentcreated() {
	local student="$1"
	local norm=$(calcnorm "$student")

	# Account exists on aquinas-www.
	error=$(ssh -T "root@aquinas-www.$domain" [ -f "/etc/httpd/accounts/$student/password" ] 2>&1)
	failbad $? "$0" "/etc/httpd/accounts/$student/password missing"

	# Account exists on aquinas-git.
	error=$(ssh -T "root@aquinas-git.$domain" grep -q "^$norm:" /etc/passwd 2>&1)
	failbad $? "$0" "$norm not in aquinas-git's /etc/passwd"

	# Account exists on aquinas-user.
	error=$(ssh -T "root@aquinas-user.$domain" grep -q "^$norm:" /etc/passwd 2>&1)
	failbad $? "$0" "$norm not in aquinas-user's /etc/passwd"

	# Home directory exists on aquinas-git and is populated with project repositories.
	error=$(ssh -T "root@aquinas-git.$domain" [ -d "$root/$student/git" ] 2>&1)
	failbad $? "$0" "no $root/$student/git on aquinas-git"

	# No project repository if no check.
	error=$(ssh -T "root@aquinas-git.$domain" [ ! -d "$root/$student/unix" ] 2>&1)
	failbad $? "$0" "$root/$student/unix already exists on aquinas-git"

	# Home directory exists on aquinas-user and is populated with project repositories.
	error=$(ssh -T "root@aquinas-user.$domain" [ -d "/home/$student" ] 2>&1)
	failbad $? "$0" "no /home/$student on aquinas-user"
}

studentremoved() {
	local student="$1"
	local norm=$(calcnorm "$student")

	# Account removed from aquinas-www.
	error=$(ssh -T "root@aquinas-www.$domain" [ -d "/etc/httpd/accounts/$student/" ] 2>&1)
	failgood $? "$0" "$student still exists in /etc/httpd/accounts"

	# Records removed from aquinas-www.
	error=$(ssh -T "root@aquinas-www.$domain" [ -d "/www/$student/" ] 2>&1)
	failgood $? "$0" "$student records remain in /www/$student"

	# Account removed from aquinas-git.
	error=$(ssh -T "root@aquinas-git.$domain" grep -q "^$norm:" /etc/passwd 2>&1)
	failgood $? "$0" "$norm still in aquinas-git's /etc/passwd"

	# Home directory removed from aquinas-git.
	error=$(ssh -T "root@aquinas-git.$domain" [ -d "$root/$student" ] 2>&1)
	failgood $? "$0" "$root/$student remains on aquinas-git"

	# Teacher workdir removed from aquinas-git.
	error=$(ssh -T "root@aquinas-git.$domain" [ -d "$root/teacher/workdir/students/$student" ] 2>&1)
	failgood $? "$0" "$root/teacher/workdir/students/$student remains on aquinas-git"

	# Records removed from aquinas-git.
	count=$(ssh -T "root@aquinas-git.$domain" find "$root/teacher/workdir/records" -name "${student}*" | wc -l 2>&1)
	if [ "x$count" != x0 ]; then
		failmsg "$0" "record remains on aquinas-git at $root/teacher/workdir/records"
	fi

	# Account removed from aquinas-user.
	error=$(ssh -T "root@aquinas-user.$domain" grep -q "^$norm:" /etc/passwd 2>&1)
	failgood $? "$0" "$norm still in aquinas-user's /etc/passwd"

	# Home directory removed from aquinas-user.
	error=$(ssh -T "root@aquinas-user.$domain" [ -d "/home/$student" ] 2>&1)
	failgood $? "$0" "/home/$student remains on aquinas-user"
}

mkwebdir() {
	local student=$1
	ssh root@aquinas-www.$domain mkdir -p /www/test/
	ssh root@aquinas-www.$domain chmod 2774 /www/test/
	ssh root@aquinas-www.$domain chown teacher:www-data /www/test/
}

injectresult() {
	local student=$1
	local project=$2
	local outcome=$3

	ssh root@aquinas-www.$domain touch "/www/$student/$project-records"
	ssh root@aquinas-www.$domain chown teacher:www-data "/www/$student/$project-records"
	ssh root@aquinas-www.$domain chmod 640 "/www/test/$project-records"
	ssh root@aquinas-www.$domain echo "[{\\\"project\\\": \\\"$project\\\", \\\"student\\\": \\\"$student\\\", \\\"commit\\\": \\\"ffffffffffffffffffffffffffffffffffffffff\\\", \\\"timestamp\\\": \\\"1970/01/01 00:00:00\\\", \\\"outcome\\\": \\\"$outcome\\\"}]" \>"/www/test/$project-records"
}

urlencode() {
	local length="${#1}"
	for (( i = 0; i < length; i++ )); do
		local c="${1:i:1}"
		case $c in
		[a-zA-Z0-9.~_-]) printf "%s" "$c" ;;
		*) printf '%s' "$c" | xxd -p -c1 |
		   while read c; do printf '%%%s' "$c"; done ;;
		esac
	done
}

waitlog() {
	local timeout="$1"
	local userserver="$2"
	local regexp="$3"
	local log="$4"

	timeout "$timeout" ssh "$userserver" tail -n 1000 -f "\"$log\"" \| grep -ql "\"$regexp\""
}
