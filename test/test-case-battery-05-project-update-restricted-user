#!/bin/sh

# NOTE: We run this test twice to test the case of a fresh clone and a
# pull. Aquinas-user.$domain's buildrunsh executes a clone the first time
# we run this. It runs a pull the second time. Hence the symbolic link to
# this file which results in two test executions.

. ./test-functions

dir=$(mktemp -d)
rand=$RANDOM
password=$(randpassword)

clean() {
	ssh root@aquinas-www.$domain rm -rf /etc/httpd/restrictions/project/${rand}Python

	if [ -n "$undo" ]; then
		if [ $(pwd) != "$dir" ]; then
			failmsg "$0" left proper directory\; in $(pwd)
		fi

		ssh root@aquinas-git.$domain git -C $root/teacher/workdir/projects reset -q --hard HEAD~1
		git reset -q --hard HEAD~1
		git push -q --force 2>/dev/null
		ssh root@aquinas-git.$domain rm -rf $root/*/${rand}C
		ssh root@aquinas-git.$domain rm -rf $root/*/${rand}Python
	fi

	popd >/dev/null
	rm -rf $dir
}

trap "clean" SIGTERM SIGINT EXIT

error=$(ssh root@aquinas-www.$domain sudo -u http aquinas-passwd-student test "$password" 2>&1)
failbad $? "$0" "error resetting test password: ${error:-no stderr}"

error=$(git clone -q teacher@aquinas-git.$domain:$root/teacher/projects $dir 2>&1)
failbad $? "$0" ${error:-could not clone}

pushd $dir >/dev/null
failbad $? "$0" "failed to pushd $dir"

error=$(git merge master 2>&1)
failbad $? "$0" ${error:-could not merge}

mkdir $rand

cat >$rand/description.json <<EOF
{ 
        "name": "$rand",
	"summary": "$0",
        "languages": [ "C", "Python" ],
	"checks": [{ "command": "./foo",
	             "stdin": null,
	             "stdout": null,
	             "stderr": null,
	             "exitCode": 0
	           }]
}
EOF

git add $rand/description.json
git commit -q -s -m "Another revision for testing: $rand ($0)" >/dev/null

undo=true

output=$(git push -q 2>&1)
failbad $? "$0" ${output:-failed to push test project}

uuid=$(echo "$output" | cut -d " " -f 2)
failbad $? "$0" ${uuid:-failed to capture uuid}

error=$(ssh root@aquinas-www.$domain mkdir -p /etc/httpd/restrictions/project/${rand}Python)
failbad $? "$0" ${error:-failed to restrict test project}

error=$(waitlog 600 root@$logServer "$uuid" /mnt/xvdb/var/log/messages 2>&1)
failbad $? "$0" ${error:-$uuid not found in log}

error=$(ssh root@aquinas-git.$domain [ -d $root/test/${rand}C ])
failbad $? "$0" ${error:-$root/test/${rand}C not found}

error=$(ssh root@aquinas-git.$domain [ -d $root/test/${rand}Python ])
failbad $? "$0" ${error:-$root/test/${rand}Python not found}

code=$(wget --server-response --quiet -O /dev/null --http-user=test --http-password=$password --auth-no-challenge http://aquinas-www.$domain/project/${rand}Python 2>&1 | awk '/^  HTTP/{ print $2 }')
if [ x"$code" != x403 ]; then
	failmsg "$0" "server did not provide 403 forbidden status: $code"
fi

passmsg "$0" okay
