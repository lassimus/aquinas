#!/bin/sh

. ./test-functions

log_host=root@$logServer
log_path=$root/var/log/messages
student=test@example.com
dir=$(mktemp -d)
tmp=$(mktemp)
rand=$RANDOM

trap "/bin/rm -rf $dir $tmp" SIGTERM SIGINT EXIT
trap "ssh root@aquinas-www.$domain sudo -u http aquinas-remove-student $student >/dev/null 2>&1" SIGTERM SIGINT EXIT

# Do not capture error; just cleaning up/should not already exist.
ssh root@aquinas-www.$domain sudo -u http aquinas-remove-student $student >/dev/null 2>&1

# This should fail due to example.com domain. See httpd.go which
# is written to log what we need, but not proceed further in the
# process.
error=$(wget --quiet -O /dev/null "http://aquinas-www.$domain/api/register/$student")
if [ $? != 8 ]; then
	failmsg "$0" "failed interaction with GET api/register/$student: ${error:-no stderr}"
fi

# NOTE: will fail if more than 1000 log messages between above and below.

log_msg=$(ssh $log_host tail -n 1000 $log_path | grep -a "preparing registration email student: $student" | tail -1)
failbad $? "$0" "could not find \"preparing registration email student: $student\" in log"

nonce=$(echo $log_msg | awk '{ print $12 }')
token=$(echo $log_msg | awk '{ print $14 }')
password=$(echo $log_msg | awk '{ print $16 }')

error=$(wget --quiet -O $tmp --post-data "nonce=$nonce&token=$token&password=$password" http://aquinas-www.$domain/api/register/$student)
failbad $? "$0" "failed interaction with POST api/register/$student: ${error:-no stderr}"

uuid=$(cat "$tmp" | sed 's/"//g')
error=$(wget --quiet -O /dev/null "http://aquinas-www.$domain/api/wait?id=$uuid")
failbad $? "$0" "failed first interaction with api/wait: ${error:-no stderr}"

key="ssh-rsa bar test@example.com"
alias="Ephemeral Test Account test@example.com"
error=$(wget --quiet -O $tmp --http-user=test@example.com --http-password=$password --auth-no-challenge --post-data "keys=$key&alias=$alias" http://aquinas-www.$domain/api/students/$student)
failgood $? "$0" system accepted bad SSH key

passmsg "$0" okay
